package by.epam.newsclient.i18n;

import java.util.ListResourceBundle;

/**
 * This class is used for internationalization. Contains method that returns
 * array with the key, value pairs. Where keys is the values for the tag
 * arguments in the JSP page and the values is the messages that will be printed
 * in place of specified tag. Messages are for the English localization.
 * 
 * @author Aliaksandr_Artsemyeu
 * @version 1.0
 */
public class NewsClientLocale extends ListResourceBundle {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.ListResourceBundle#getContents()
	 */
	@Override
	protected Object[][] getContents() {
		return new Object[][] { 
			{ "head.title", "News Portal" },
			{ "header.title", "NEWS PORTAL" }, 
			{ "header.engloc", "English" },
			{ "header.rusloc", "Russian" }, 
			{ "filter.author", "AUTHOR" }, 
			{ "filter.local.reset", "reset" }, 
			{ "filter.tags", "TAGS" }, 
			{ "filter.filter", "Filter" }, 
			{ "filter.reset", "Reset" }, 
			{ "filter.empty.error", "Sorry, you must choose at least one option to perform filtering." }, 
			{ "filter.reset.error", "Sorry, you must perform filtering, before trying to reset it." }, 
			{ "news.list.comments", "Comments" }, 
			{ "news.list.view", "View" }, 
			{ "show.news.previous", "Previous" },
			{ "show.news.back", "Back" },
			{ "show.news.next", "Next" },
			{ "news.comment.error", "Comment text should not be empty or longer then 100 symbols." },
			{ "show.news.post", "Post Comment" },
			{ "error.message.404", "Sorry, there is no such page." },
			{ "error.message.home", "Home" },
			{ "error.message.500", "Sorry, some error occurred during execution of the application." },
			{ "emtry.search.result", "Sorry, no news found using this filter options." },
			{ "footer.copyright", "Copyrigth @ EPAM 2015. All Rights Reserved." } };
	}

}
