package by.epam.newsclient.i18n;

import java.util.ListResourceBundle;

/**
 * This class is used for internationalization. Contains method that returns
 * array with the key, value pairs. Where keys is the values for the tag
 * arguments in the JSP page and the values is the messages that will be printed
 * in place of specified tag. Messages are for the Russian localization.
 * 
 * @author Aliaksandr_Artsemyeu
 * @version 1.0
 */
public class NewsClientLocale_ru extends ListResourceBundle {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.ListResourceBundle#getContents()
	 */
	@Override
	protected Object[][] getContents() {
		return new Object[][] { 
			{ "head.title", "Новостной портал" }, 
			{ "header.title", "НОВОСТНОЙ ПОРТАЛ" },
			{ "header.engloc", "Английский" }, 
			{ "header.rusloc", "Русский" }, 
			{ "filter.author", "АВТОР" },
			{ "filter.local.reset", "сбросить" }, 
			{ "filter.tags", "ТЭГИ" }, 
			{ "filter.filter", "Фильтровать" },
			{ "filter.reset", "Сбросить" }, 
			{ "filter.empty.error", "Извините, для выполнения фильтрации необходимо выбрать хотя бы один критерий." },
			{ "filter.reset.error", "Извините, перед тем как сбрасывать настройки фильтра, необходимо произвести фильтрацию." }, 
			{ "news.list.comments", "Комментарии" }, 
			{ "news.list.view", "Просмотреть" },
			{ "show.news.previous", "Предыдущая" }, 
			{ "show.news.back", "Назад" }, 
			{ "show.news.next", "Следующая" },
			{ "error.message.404", "Извините, такой страницы не существует." },
			{ "error.message.home", "На Главную" },
			{ "error.message.500", "Извините, во время работы программы произошла неизвестная ошибка." },
			{ "news.comment.error", "Текст комментария не должен быть пустым и длинна не должна превышать 100 символов" },
			{ "show.news.post", "Оставить Комментарий" }, 
			{ "emtry.search.result", "Извините, не найдено ни одной новости." },
			{ "footer.copyright", "Copyrigth @ ЕПАМ 2015. Все права защищены." } };
	}

}
