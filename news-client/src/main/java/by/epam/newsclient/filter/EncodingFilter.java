package by.epam.newsclient.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * This class is a servlet filter. Used to setup default encoding.
 * 
 * @author Aliaksandr_Artsemyeu
 * @version 1.0
 */
public class EncodingFilter implements Filter {

	/**
	 * This filed defines default encoding.
	 */
	private String encoding;

	@Override
	public void destroy() {
	}

	/**
	 * This method is used to set web page encoding.
	 * 
	 * @param request
	 *            ServletRequest instance.
	 * @param response
	 *            ServletResponse instance.
	 * @param chain
	 *            FilterChain instance.
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (request.getCharacterEncoding() == null) {
			request.setCharacterEncoding(encoding);
		}
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		chain.doFilter(request, response);
	}

	/**
	 * This method is used to read parameter from web.xml or if there is no such
	 * parameter, to set encoding field to default value witch is UTF-8 .
	 * 
	 * @param config
	 *            FilterConfig.
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {
		encoding = config.getInitParameter("requestEncoding");
		if (encoding == null) {
			encoding = "UTF-8";
		}
	}

}
