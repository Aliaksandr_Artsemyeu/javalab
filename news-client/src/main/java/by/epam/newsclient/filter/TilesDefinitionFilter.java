package by.epam.newsclient.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tiles.TilesContainer;
import org.apache.tiles.access.TilesAccess;
import org.apache.tiles.request.ApplicationContext;
import org.apache.tiles.request.Request;
import org.apache.tiles.request.servlet.ServletUtil;

/**
 * This class is a servlet filter. This filter is used only for post-processing.
 * It uses TilesContainer to render specified view to the user.
 * 
 * @author Aliaksandr_Artsemyeu
 * @version 1.0
 */
public class TilesDefinitionFilter implements Filter {

	/**
	 * This method is used to get TilesContainer instance which corresponds to
	 * the current servlet context. After obtaining instance of TilesContainer
	 * this method gets a String that contains a logical name of the view and
	 * pass this string to the container. Container will use this string to find
	 * appropriate tiles definition and render the output using this definition.
	 * 
	 * Also if the string with the logical name of the view, will contain
	 * "redirect:" at the beginning, filter will perform redirect to the address
	 * specified after "redirect:".
	 * 
	 * @throws IOException
	 * 
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		try {
			chain.doFilter(request, response);
			ApplicationContext applicationContext = ServletUtil.getApplicationContext(request.getServletContext());
			TilesContainer tilesContainer = TilesAccess.getContainer(applicationContext);
			String definition = (String) request.getAttribute("definition");
			if (definition.startsWith("redirect:")) {
				String parsedURL = definition.substring("redirect:".length());
				httpResponse.sendRedirect("/news-client/news-portal" + parsedURL);
			} else {
				Request tilesRequest = new org.apache.tiles.request.servlet.ServletRequest(applicationContext,
						httpRequest, httpResponse);
				tilesContainer.render(definition, tilesRequest);
			}
		} catch (ServletException e) {
			httpResponse.sendRedirect("/news-client/news-portal/error");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
	}

}
