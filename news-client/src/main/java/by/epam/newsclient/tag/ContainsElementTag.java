package by.epam.newsclient.tag;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * This class is a custom JSP tag that is used to check if the given List
 * contains specified Tag id value or not.
 * 
 * @author Aliaksandr_Artsemyeu
 * @version 1.0
 */
public class ContainsElementTag extends SimpleTagSupport {

	/**
	 * List that contains values of type Long, which corresponds to the TAG_ID
	 * column values in the TAG table.
	 */
	private List<Long> tagList;

	/**
	 * Specified Tag id, also corresponds to the TAG_ID column value in the TAG
	 * table. This value will be used to check if tagList contains it or not.
	 */
	private Long tag;

	public void setTagList(List<Long> tagList) {
		this.tagList = tagList;
	}

	public void setTag(Long tag) {
		this.tag = tag;
	}

	/**
	 * Checks if List contains specified element, if so HTML inside tag will be
	 * printed or executed.
	 */
	public void doTag() throws JspException, IOException {
		if (tagList != null && tagList.contains(tag)) {
			getJspBody().invoke(null);
		}
	}
}
