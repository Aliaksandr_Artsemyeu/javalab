package by.epam.newsclient.controller.command.impl;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.javalab.newscommon.entity.Comment;
import by.epam.javalab.newscommon.service.CommentsService;
import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;
import by.epam.javalab.newscommon.service.util.NewsListWrapper;
import by.epam.newsclient.controller.command.Command;

/**
 * This is implementation of Command interface. It is used to add a new Comment
 * record to the COMMENTS table for the specified News.
 * 
 * @author Aliaksandr_Artsemyeu
 * @version 1.0
 */
public class PostCommentCommand implements Command {
	
	@Inject
	private CommentsService commentsService;
	
	@Inject
	private NewsListWrapper newsListWrapper;

	/**
	 * Maximum length of the Comment text field.
	 */
	public static final int COMMENT_TEXT_MAX_LENGTH = 100;

	/**
	 * Used to add a new record to the COMMENTS table based on Comment instance,
	 * that is built and validated before that.
	 * 
	 * @param request
	 *            HttpServletRequest instance.
	 * @param response
	 *            HttpServletResponse instance.
	 * @return logical view name.
	 * @throws InvalidParameterException
	 *             if validation was unsuccessful.
	 * @throws ServiceException
	 *             if internal error occurred.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		try {
			Comment comment = buildComment(request);
			if (validateComment(comment, newsListWrapper.getIdList())) {
				commentsService.insert(comment);
			} else {
				return "redirect:/show-news?id=" + comment.getNewsId() + "&invalid=true";
			}
			return "redirect:/show-news?id=" + comment.getNewsId();
		} catch (InvalidParameterException e) {
			return "redirect:/error?status=404";
		} catch (ServiceException e) {
			return "redirect:/error?status=500";
		}
	}

	/**
	 * Used to create Comment instance and initialize it with the values
	 * obtained from the request parameters.
	 * 
	 * @param context
	 *            Context instance.
	 * @return Comment instance.
	 */
	private Comment buildComment(HttpServletRequest request) {
		String newsIdParam = request.getParameter("newsId");
		String commentText = request.getParameter("commentText");
		Long newsId = (newsIdParam != null ? Long.parseLong(newsIdParam) : null);
		return new Comment(newsId, commentText);
	}

	/**
	 * This method is used to validate Comment instance. If newId is null or
	 * current idList do not contain it InvalidParameterException will be
	 * thrown. If Comments text value is empty or its length is greater then
	 * specified value false will be returned.
	 * 
	 * @param comment
	 *            Comment instance.
	 * @return true, false depending on validation process.
	 * @throws InvalidParameterException
	 *             thrown when newsId value is null or idList do not contains
	 *             it.
	 */
	private boolean validateComment(Comment comment, List<Long> idList) throws InvalidParameterException {
		Long newsId = comment.getNewsId();
		String text = comment.getText();
		if (newsId == null || !idList.contains(newsId)) {
			throw new InvalidParameterException();
		}
		if (text == null || text.trim().length() == 0 || text.length() > COMMENT_TEXT_MAX_LENGTH) {
			return false;
		}
		return true;
	}
}
