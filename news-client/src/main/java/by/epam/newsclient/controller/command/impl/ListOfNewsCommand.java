package by.epam.newsclient.controller.command.impl;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.javalab.newscommon.service.AuthorService;
import by.epam.javalab.newscommon.service.NewsManagmentService;
import by.epam.javalab.newscommon.service.NewsService;
import by.epam.javalab.newscommon.service.TagService;
import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;
import by.epam.javalab.newscommon.service.util.NewsListWrapper;
import by.epam.javalab.newscommon.service.util.SearchCriteria;
import by.epam.newsclient.controller.command.Command;

/**
 * Implementation of Command Interface. Used to provide List of News records
 * that contains all records by default or only records selected with the use of
 * SearchCriteria instance.
 * 
 * @author Aliaksandr_Artsemyeu
 * @version 1.0
 */
public class ListOfNewsCommand implements Command {
	
	@Inject
	private NewsManagmentService newsManagmentService;
	
	@Inject
	private NewsService newsService;
	
	@Inject
	private AuthorService authorService;
	
	@Inject
	private TagService tagService;
	
	@Inject
	private NewsListWrapper newsListWrapper;
	
	@Inject
	private SearchCriteria searchCriteria;

	/**
	 * This method is used to check if the SearchCriteria instance already
	 * exists as session attribute, if so it will be used by NewsService to get
	 * List with all News records Id values, that matches to the SearchCriteria
	 * constrains. Then NewsListWrapper is obtained and idList is set to it. And
	 * then NewsManagmentService is used to get List of DTONews instances which
	 * represent part of the chosen News records.
	 * 
	 * @param request
	 *            HttpServletRequest instance.
	 * @param response
	 *            HttpServletResponse instance.
	 * @return logical view name.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<Long> idList = null;
			String pageParam = request.getParameter("page");
			int page = (pageParam == null ? -1 : Integer.parseInt(pageParam));
			if (page == -1) {
				newsListWrapper.setList(newsService.selectIdList(searchCriteria));
				idList = newsListWrapper.getIdSubList();
			} else {
				idList = newsListWrapper.getIdSubList(page);
			}
			request.setAttribute("newsList", newsManagmentService.selectList(idList));
			request.setAttribute("searchCriteria", searchCriteria);
			request.setAttribute("newsListWrapper", newsListWrapper);
			request.setAttribute("authorList", authorService.selectEntries());
			request.setAttribute("tagList", tagService.selectEntries());
			return "list-of-news";
		} catch (InvalidParameterException | NumberFormatException e) {
			return "redirect:/error?status=404";
		} catch (ServiceException e) {
			return "redirect:/error?status=500";
		}
	}

}
