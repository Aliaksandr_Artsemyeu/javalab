package by.epam.newsclient.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import by.epam.newsclient.controller.command.Command;

/**
 * This class is a controller of this application. It is used to get user
 * requests and to provide appropriate views for this requests.
 * 
 * @author Aliaksandr_Artsemyeu
 * @version 1.0
 */
@SuppressWarnings("serial")
public class DispatcherServlet extends HttpServlet {

	private final static Logger logger = Logger.getLogger(DispatcherServlet.class);
	
	/**
	 * Spring application context. Used to obtain beans.
	 */
	private ApplicationContext applicationContext;

	/**
	 * Used to manage user requests. Obtains appropriate Command for the
	 * specified user request and executes this Command. Execution of Command
	 * results in the String that contains logical view name. Passes this name
	 * to the Tiles container that renders appropriate view.
	 * 
	 * @param request
	 *            HttpServletRequest instance.
	 * @param response
	 *            HttpServletResponse instance.
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		try {
			Command command = (Command) applicationContext.getBean(request.getMethod() + request.getPathInfo());
			request.setAttribute("definition", command.execute(request, response));
		} catch (BeansException e) {
			logger.error(e.getMessage(), e);
			request.setAttribute("definition", "not-found");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			request.setAttribute("definition", "internal-error");
		}
	}

	/**
	 * Redirects execution to the doGet method.
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		doGet(request, response);
	}

	@Override
	public void init() throws ServletException {
		applicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletConfig().getServletContext());
	}
	
}
