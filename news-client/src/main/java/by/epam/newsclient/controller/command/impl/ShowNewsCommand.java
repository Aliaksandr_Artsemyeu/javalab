package by.epam.newsclient.controller.command.impl;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.javalab.newscommon.service.NewsManagmentService;
import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;
import by.epam.javalab.newscommon.service.util.NewsListWrapper;
import by.epam.newsclient.controller.command.Command;

/**
 * This is implementation of Command interface. Used to provide News instance
 * according to requested Id.
 * 
 * @author Aliaksandr_Artsemyeu
 * @version 1.0
 */
public class ShowNewsCommand implements Command {
	
	@Inject
	private NewsManagmentService newsManagmentService;
	
	@Inject
	private NewsListWrapper newsListWrapper;

	/**
	 * Used to get newId from request parameters and to get appropriate DTONews
	 * instance for this Id value. NewsManagmentService is used for this
	 * purpose.
	 * 
	 * @param request
	 *            HttpServletRequest instance.
	 * @param response
	 *            HttpServletResponse instance.
	 * @return logical view name.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		try {
			String idParam = request.getParameter("id");
			Long newsId = (idParam == null ? null : Long.parseLong(idParam));
			newsListWrapper.setIndex(newsId);
			request.setAttribute("newsDTO", newsManagmentService.select(newsId));
			request.setAttribute("newsListWrapper", newsListWrapper);
			return "show-news";
		} catch (InvalidParameterException | NumberFormatException e) {
			return "redirect:/error?status=404";
		} catch (ServiceException e) {
			return "redirect:/error?status=500";
		}
	}

}
