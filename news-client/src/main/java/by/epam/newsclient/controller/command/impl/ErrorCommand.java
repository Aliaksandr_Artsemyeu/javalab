package by.epam.newsclient.controller.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.newsclient.controller.command.Command;

/**
 * implementation of Command interface. Used to provide custom error pages to
 * the user.
 * 
 * @author Aliaksandr_Artsemyeu
 * @version 1.0
 */
public class ErrorCommand implements Command {

	/**
	 * Used to provide appropriate logical view names for the error pages
	 * according to the value of error status.
	 * 
	 * @param request
	 *            HttpServletRequest instance.
	 * @param response
	 *            HttpServletResponse instance.
	 * @return String with URL address, user will be redirected to.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String status = request.getParameter("status");
		String view = null;
		switch (status) {
		case "404":
			view = "not-found"; break;
		case "500":
			view = "internal-error"; break;
		default:
			view = "internal-error"; 
		}
		return view;
	}

}
