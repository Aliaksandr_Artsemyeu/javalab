package by.epam.newsclient.controller.command.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.javalab.newscommon.service.util.SearchCriteria;
import by.epam.newsclient.controller.command.Command;

/**
 * Implementation of Command Interface. Used to perform filtering action on News
 * records. Define which records will be shown to the user, according to the
 * SearchCriteria instance member values.
 * 
 * @author Aliaksandr_Artsemyeu
 * @version 1.0
 */
public class FilterCommand implements Command {

	@Inject
	private SearchCriteria searchCriteria;

	/**
	 * This method gets SearchCriteria instance that contains filter preferences
	 * as values of its members. And sets it as a session attribute.
	 * 
	 * @param request
	 *            HttpServletRequest instance.
	 * @param response
	 *            HttpServletResponse instance.
	 * @return String with URL address, user will be redirected to.
	 */
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String option = request.getParameter("filter-button");
		switch (option) {
		case "filter":
			buildSearchCriteria(request.getParameterMap());
			break;
		case "reset":
			searchCriteria.resetAuthorAndTags();
			break;
		default:
			return "redirect:/error?status=404";
		}
		return "redirect:/list-of-news";
	}

	/**
	 * This method is used to build SearchCriteria instance based on request
	 * parameters.
	 * 
	 * @param parameters
	 *            Map that contain request parameters.
	 */
	private void buildSearchCriteria(Map<String, String[]> parameters) {
		String[] authorIdParams = parameters.get("authorId");
		String[] tagIdListParam = parameters.get("tagIdList");
		Long authorId = null;
		if (authorIdParams != null) {
			String authorIdString = authorIdParams[0];
			if (authorIdString != null) {
				authorId = Long.parseLong(authorIdString);
			}
		}
		List<Long> tagIdList = new ArrayList<>();
		if (tagIdListParam != null) {
			for (String tag : tagIdListParam) {
				if (tag != null) {
					tagIdList.add(Long.parseLong(tag));
				}
			}
		}
		searchCriteria.setAuthorAndTags(authorId, tagIdList);
		searchCriteria.setSearchPerformed(true);
	}

}
