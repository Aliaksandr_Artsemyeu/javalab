package by.epam.newsclient.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This interface defines Command that is obtained from the CommandFactory in
 * response to user request.
 * 
 * @author Aliaksandr_Artsemyeu
 * @version 1.0
 */
public interface Command {

	/**
	 * Contains sequence of commands that performs some action according to
	 * users request.
	 * 
	 * @param request
	 *            HttpServletRequest instance.
	 * @param response
	 *            HttpServletResponse instance.
	 * @return logical view name.
	 */
	public String execute(HttpServletRequest request, HttpServletResponse response);

}
