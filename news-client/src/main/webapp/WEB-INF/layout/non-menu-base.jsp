<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="by.epam.newsclient.i18n.NewsClientLocale" scope="session"/>

<html>
	<head>
		<title><fmt:message key="head.title"/></title>
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/normalize.css"/>">
		<script src="<c:url value="/resources/js/utility-scripts.js"/>"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<div class="main-content">
			
			<div class="row">
				<header class="col-12">
					<t:insertAttribute name="header" />
				</header>
			</div>
			
			<div class="row">
				<article class="col-12">
					<section class="col-12">
						<t:insertAttribute name="body" />
					</section>
				</article>
			</div>
			
			<div class="row">
				<footer class="col-12">
					<t:insertAttribute name="footer" />
				</footer>
			</div>
			
		</div>
	</body>
</html>
