<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h1><a href="<c:url value="/news-portal/list-of-news"/>"><fmt:message key="header.title"/></a></h1>
<div class="language-switch">
	<a href='${pageContext.request.pathInfo}?
		<c:choose>
			<c:when test='${fn:contains(pageContext.request.queryString, "language")}'>
				${fn:substring(pageContext.request.queryString, 0, fn:indexOf(pageContext.request.queryString, "language"))}
				language=en
			</c:when>
			<c:otherwise>
				${pageContext.request.queryString}
				<c:if test='${not empty pageContext.request.queryString}'>&</c:if>
				language=en
			</c:otherwise>
		</c:choose>
		'><fmt:message key="header.engloc"/></a>
	<span>|</span>
	<a href='${pageContext.request.pathInfo}?
		<c:choose>
			<c:when test='${fn:contains(pageContext.request.queryString, "language")}'>
				${fn:substring(pageContext.request.queryString, 0, fn:indexOf(pageContext.request.queryString, "language"))}
				language=ru
			</c:when>
			<c:otherwise>
				${pageContext.request.queryString}
				<c:if test='${not empty pageContext.request.queryString}'>&</c:if>
				language=ru
			</c:otherwise>
		</c:choose>
		'><fmt:message key="header.rusloc"/></a>
</div>