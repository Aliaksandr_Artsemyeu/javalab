<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="news" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>

<div class="row col-12 search-filter">
	<div class="col-12 row search-filter-error-messages">
		<span id="empty-filter-error"><fmt:message key="filter.empty.error"/></span>
		<span id="reset-filter-error"><fmt:message key="filter.reset.error"/></span>
	</div>
	<div class="col-12 row">
		<form id="search-filter-form">
			<div class="col-3 author-filter">
				<span><fmt:message key="filter.author"/></span>
				<ul>
					<c:forEach items="${authorList}" var="author">
						<li>
							<input type="checkbox" name="authorId" value="${author.id}" <c:if test="${author.id == searchCriteria.authorId}">checked</c:if>>
							<span><c:out escapeXml="true" value="${author.name}"/></span>
						</li>
					</c:forEach>
					<li class="local-filter-reset"><input type="button" value="<fmt:message key="filter.local.reset"/>" onclick="resetAuthor()"></li>
				</ul>
			</div>
			<div class="col-3 tag-filter">
				<span><fmt:message key="filter.tags"/></span>
				<ul>
					<c:forEach items="${tagList}" var="tag">
						<li>
							<input type="checkbox" name="tagIdList" value="${tag.id}" <news:ifContains tagList="${searchCriteria.tagIdList}" tag="${tag.id}">checked</news:ifContains>>
							<span><c:out escapeXml="true" value="${tag.name}"/></span>
						</li>
					</c:forEach>
					<li class="local-filter-reset"><input type="button" value="<fmt:message key="filter.local.reset"/>" onclick="resetTags()"></li>
				</ul>
			</div>
			<div class="col-2"></div>
			<input id="search-performed-element" type="hidden" value="${searchCriteria.searchPerformed}"/>
			<button class="col-2 filter-button filter" type="submit" name="filter-button" value="filter" onclick="return validateFilter()" formaction="<c:url value="/news-portal/list-of-news/filter"/>"><fmt:message key="filter.filter"/></button>
			<button class="col-2 filter-button reset" type="submit" name="filter-button" value="reset" onclick="return validateReset()" formaction="<c:url value="/news-portal/list-of-news/filter"/>"><fmt:message key="filter.reset"/></button>
		</form>
	</div>
</div>

<div class="row col-12 news-list">
	<c:choose>
		<c:when test="${fn:length(newsList) != 0}">
			<form method="POST">
				<c:forEach items="${newsList}" var="newsItem">
					<div class="news-item">
						<div class="row">
							<div class="news-item-header" >
								<span class="col-6"><c:out escapeXml="true" value="${newsItem.title}"/></span>
								<span class="col-4 news-author">(by <c:out escapeXml="true" value="${newsItem.author.name}"/>)</span>
								<span class="col-2 news-creation-date"><joda:format value="${newsItem.creationDate}" pattern="MM-dd-yyyy"/></span>
							</div>
						</div>
						<div class="row">
							<div class="col-12 news-item-content">
								<span><c:out escapeXml="true" value="${newsItem.shortText}"/></span>
							</div>
						</div>
						<div class="row">
							<div class="col-12 news-item-footer">
								<a href="<c:url value="/news-portal/show-news?id=${newsItem.id}"/>"><fmt:message key="news.list.view"/></a>
								<span><fmt:message key="news.list.comments"/>(${fn:length(newsItem.comments)})</span>
								<div class="news-item-tagslist">
									<c:forEach items="${newsItem.tags}" var="tag" varStatus="status">
										<span><c:out escapeXml="true" value="${tag.name}"/></span>
										<c:if test="${not status.last}"><span>,</span></c:if>
									</c:forEach>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</form>
		</c:when>
		<c:otherwise>
			<div class="empty-search-result-message"><span><fmt:message key="emtry.search.result"/></span></div>
		</c:otherwise>			
	</c:choose>
</div>

<div class="row col-12 news-pagination">
	<c:forEach begin="1" end="${fn:length(newsListWrapper.idList)}" step="${newsListWrapper.numberOfNewsOnPage}" varStatus="loopCounter">
		<c:set var="count" value="${loopCounter.count}" />
		<a class="news-pagination-button" href="<c:url value="/news-portal/list-of-news?page=${count - 1}"/>">${count}</a>
	</c:forEach>
</div>

<script>
$("input:checkbox[name='authorId']").change(function() {
	var group = ":checkbox[name='authorId']";
	if ($(this).is(':checked')) {
		$(group).not($(this)).attr("checked", false);
	}
});
</script>