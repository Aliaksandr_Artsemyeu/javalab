<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row col-12 error-message">
	<h1>404</h1>
	<span><fmt:message key="error.message.404"/></span>
	<a href="<c:url value="/news-portal/list-of-news"/>"><img src="<c:url value="/resources/img/home.png"/>" alt="<fmt:message key="error.message.home"/>"></a>
</div>
