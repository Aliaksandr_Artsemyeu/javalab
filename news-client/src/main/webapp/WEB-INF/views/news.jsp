<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>

<div class="row col-12 show-news">
	<div class="row col-12 show-news-navigation-buttons">
		<c:set var="next" value="${newsListWrapper.next}"/>
		<c:set var="prev" value="${newsListWrapper.prev}"/>
		<div class="col-4 show-news-navigation-buttons-prev">
			<c:if test="${not empty prev}">
				<a href="<c:url value="/news-portal/show-news?id=${prev}"/>"><fmt:message key="show.news.previous"/></a> 
			</c:if>
		</div>
		<div class="col-4 show-news-navigation-buttons-back">
			<a href="<c:url value="/news-portal/list-of-news"/>"><fmt:message key="show.news.back"/></a>
		</div>
		<div class="col-4 show-news-navigation-buttons-next">
			<c:if test="${not empty next}">
				<a href="<c:url value="/news-portal/show-news?id=${next}"/>"><fmt:message key="show.news.next"/></a>
			</c:if>
		</div>
	</div>
	
	<div class="row col-12 show-news-body">
		<div class="show-news-title">
			<span><c:out escapeXml="true" value="${newsDTO.title}"/></span>
		</div>
		<div class="show-news-author-date">
			<span>(by <c:out escapeXml="true" value="${newsDTO.author.name}"/>)</span>
			<span><joda:format value="${newsDTO.modificationDate}" pattern="MM-dd-yyyy"/></span>
		</div>
		<div class="show-news-content">
			<span><c:out escapeXml="true" value="${newsDTO.fullText}"/></span>
		</div>
	</div>
	
	<div class="row col-12 show-news-comments">
		<c:forEach items="${newsDTO.comments}" var="comment">
			<form class="col-12" method="POST">
				<div class="show-news-comments-header">
					<span><joda:format value="${comment.creationDate}" pattern="MM-dd-yyyy"/></span>
				</div>
				<span class="show-news-comments-text"><c:out escapeXml="true" value="${comment.text}"/></span>
			</form>
		</c:forEach>
	</div>
	
	<div class="row col-12 show-news-comments-add-comment">
		<form class="col-12" method="POST" onsubmit="return validateComment()">
			<div class="show-news-comments-add-comment-error">
				<span id="add-comment-error-message" <c:if test="${param.invalid}">style="display: initial;"</c:if>><fmt:message key="news.comment.error"/></span>
			</div>
			<input type="hidden" name="newsId" value="${newsDTO.id}"/>
			<textarea id="show-news-add-comment-input" rows="3" name="commentText" maxlength="100" <c:if test="${invalid}">style="border-color: #f02200;"</c:if>></textarea> 
			<input type="submit" value="<fmt:message key="show.news.post"/>" formaction="<c:url value="/news-portal/show-news/post-comment"/>">
		</form>
	</div>
</div>