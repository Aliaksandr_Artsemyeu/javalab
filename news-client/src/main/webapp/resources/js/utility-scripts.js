function resetAuthor() {
	var x = document.getElementsByName("authorId");
	var i;
	for (i = 0; i < x.length; i++) {
		if (x[i].type == "checkbox") {
			x[i].checked = false;
		}
	}
}

function resetTags() {
	var x = document.getElementsByName("tagIdList");
	var i;
	for (i = 0; i < x.length; i++) {
		if (x[i].type == "checkbox") {
			x[i].checked = false;
		}
	}
}

function validateFilter() {
	var author = document.getElementsByName("authorId");
	var tags = document.getElementsByName("tagIdList");
	var valid = false;

	for (i = 0; i < author.length; i++) {
		if (author[i].checked) {
			valid = true;
		}
	}

	for (i = 0; i < tags.length; i++) {
		if (tags[i].checked) {
			valid = true;
		}
	}

	if (!valid) {
		document.getElementById("reset-filter-error").style.display = "none";
		document.getElementById("empty-filter-error").style.display = "flex";
	}

	return valid;
}

function validateReset() {
	var searchPerformed = document.getElementById("search-performed-element").value;

	if (searchPerformed == "false") {
		document.getElementById("empty-filter-error").style.display = "none";
		document.getElementById("reset-filter-error").style.display = "flex";
		return false;
	}

	return true;
}

function validateComment() {
	var comment = document.getElementById("show-news-add-comment-input");
	var commentLength = comment.value;
	if (commentLength == 0 || commentLength > 100) {
		document.getElementById("add-comment-error-message").style.display = "initial";
		return false;
	}
	return true;
}