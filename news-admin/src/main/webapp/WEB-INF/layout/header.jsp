<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<h1><a href="<s:url value="/list-of-news"/>"><s:message code="header.title"/></a></h1>
<div class="language-switch">
	<a href="?lang=en"><s:message code="header.engloc"/></a>
	<span>|</span>
	<a href="?lang=ru"><s:message code="header.rusloc"/></a>
</div>