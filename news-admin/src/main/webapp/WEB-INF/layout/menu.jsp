<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<div class="menu-title">
	<span><s:message code="menu.title"/></span>
</div>

<div class="menu-item-button-wrap" >
	<a href="<s:url value="/list-of-news"/>"><span><s:message code="menu.newslist"/></span></a>
</div>
<div class="menu-item-button-wrap" >
	<a href="<s:url value="/add-news"/>"><span><s:message code="menu.addnews"/></span></a>
</div>
<div class="menu-item-button-wrap" >
	<a href="<s:url value="/edit-authors"/>"><span><s:message code="menu.addauthor"/></span></a>
</div>
<div class="menu-item-button-wrap" >
	<a href="<s:url value="/edit-tags"/>"><span><s:message code="menu.addtag"/></span></a>
</div>

<div class="logout-user-wrap">
	<span><security:authentication property="principal.username" /></span>
</div>
<div class="logout-link">
	<form id="logout-form" method="POST" action="<s:url value="/logout"/>">
		<span onclick="document.getElementById('logout-form').submit();"><s:message code="logout.logout"/></span>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
</div>	
