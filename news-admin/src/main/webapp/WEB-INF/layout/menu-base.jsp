<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<!DOCTYPE html>
<html>
	<head>
		<title><s:message code="head.title"/></title>
		<link rel="stylesheet" type="text/css" href="<s:url value="/resources/css/style.css"/>">
		<link rel="stylesheet" type="text/css" href="<s:url value="/resources/css/normalize.css"/>">
		<script src="<s:url value="/resources/js/utility-scripts.js"/>"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<script type="text/javascript">
			$(function () {
				setNavigation();
			});

			function setNavigation() {
				var path = window.location.pathname;
				path = path.replace(/localhost[^?]*/, "$1");
				path = decodeURIComponent(path);

				$(".menu-item-button-wrap a").each(function () {
					var href = $(this).attr('href');
					if (path.substring(0, href.length) === href) {
						$(this).parent().addClass('active');
					}
				});
			}
    	</script>
	</head>
	<body>
		<div class="main-content">
			
			<div class="row">
				<header class="col-12">
					<t:insertAttribute name="header" />
				</header>
			</div>
			
			<div class="row">
				<article class="col-12">
					<nav class="col-2">
						<t:insertAttribute name="menu" />
					</nav>
					<section class="col-10">
						<t:insertAttribute name="body" />
					</section>
				</article>
			</div>
			
			<div class="row">
				<footer class="col-12">
					<t:insertAttribute name="footer" />
				</footer>
			</div>
			
		</div>
	</body>
</html>
