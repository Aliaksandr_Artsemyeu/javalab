<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<!DOCTYPE html>
<html>
	<head>
		<title><s:message code="head.title"/></title>
		<link rel="stylesheet" type="text/css" href="<s:url value="/resources/css/style.css"/>">
		<link rel="stylesheet" type="text/css" href="<s:url value="/resources/css/normalize.css"/>">
		<script src="<s:url value="/resources/js/utility-scripts.js"/>"></script>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<div class="main-content">
			
			<div class="row">
				<header class="col-12">
					<t:insertAttribute name="header" />
				</header>
			</div>
			
			<div class="row">
				<article class="col-12">
					<section class="col-12">
						<t:insertAttribute name="body" />
					</section>
				</article>
			</div>
			
			<div class="row">
				<footer class="col-12">
					<t:insertAttribute name="footer" />
				</footer>
			</div>
			
		</div>
	</body>
</html>
