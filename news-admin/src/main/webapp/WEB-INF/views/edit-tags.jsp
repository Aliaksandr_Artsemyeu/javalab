<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row col-12 edit-tags">
	<div class="row col-12 edit-tags-error-message">
		<sf:form class="col-12" commandName="tag">
			<sf:errors path="name" />
			<span id="edit-tags-error-message-client-side"><s:message code="tag.save.error"/></span>
		</sf:form>
	</div>
	<span class="row edit-tags-title"><s:message code="edit.tags.add"/></span>
	<sf:form class="row col-12 edit-tags-add-form" method="POST" commandName="tag">
		<sf:input htmlEscape="true" class="edit-tags-tag-name" type="text" maxlength="30" path="name" />
		<input type="submit" onclick="return validateTag()" value="<s:message code="edit.tags.save"/>" formaction="<s:url value="/edit-tags/add-tag"/>">
	</sf:form>
	<span class="row edit-tags-title"><s:message code="edit.tags.tags"/></span>
	<c:forEach items="${tagList}" var="tag">
		<div class="row col-12 edit-tags-tag">
			<sf:form class="col-12" method="POST" commandName="tag">
				<sf:input type="hidden" path="id" value="${tag.id}"/>
				<sf:input htmlEscape="true" class="edit-tags-tag-name" maxlength="30" disabled="${(tag.id != tagId) ? 'true' : 'false'}" type="text" path="name" value="${tag.name}" />
				<c:choose>
					<c:when test="${tag.id == tagId}">
						<div class="row col-12 edit-tags-tag-edit-buttons">
							<input type="submit" onclick="return validateTag()" value="<s:message code="edit.tags.update"/>" formaction="<s:url value="/edit-tags/${tag.id}"/>"/>
							<input type="submit" value="<s:message code="edit.tags.delete"/>" formaction="<s:url value="/edit-tags/delete-tag"/>"/>
							<a href="<s:url value="/edit-tags"/>"><s:message code="edit.tags.cancel"/></a>
						</div>
					</c:when>
					<c:otherwise>
						<a href="<s:url value="/edit-tags/${tag.id}"/>"><s:message code="edit.tags.edit"/></a>
					</c:otherwise>			
				</c:choose>
			</sf:form>
		</div>
	</c:forEach>
</div>