<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<div class="row col-12 show-news">
	<div class="row col-12 show-news-navigation-buttons">
		<c:set var="next" value="${newsListWrapper.next}"/>
		<c:set var="prev" value="${newsListWrapper.prev}"/>
		<div class="col-4 show-news-navigation-buttons-prev">
			<c:if test="${not empty prev}">
				<a href="<s:url value="/show-news/${prev}"/>"><s:message code="show.news.previous"/></a>
			</c:if>
		</div>
		<div class="col-4 show-news-navigation-buttons-back">
			<a href="<s:url value="/list-of-news"/>"><s:message code="show.news.back"/></a>
		</div>
		<div class="col-4 show-news-navigation-buttons-next">
			<c:if test="${not empty next}">
				<a href="<s:url value="/show-news/${next}"/>"><s:message code="show.news.next"/></a>
			</c:if>
		</div>
	</div>
	
	<div class="row col-12 show-news-body">
		<div class="show-news-title">
			<span><c:out escapeXml="true" value="${newsDTO.title}"/></span>
		</div>
		<div class="show-news-author-date">
			<span>(by <c:out escapeXml="true" value="${newsDTO.author.name}"/>)</span>
			<span><joda:format value="${newsDTO.modificationDate}" pattern="MM-dd-yyyy"/></span>
		</div>
		<div class="show-news-content">
			<span><c:out escapeXml="true" value="${newsDTO.fullText}"/></span>
		</div>
	</div>
	
	<div class="row col-12 show-news-comments">
		<c:forEach items="${newsDTO.comments}" var="comment">
			<form class="col-12" method="POST" action="<s:url value="/show-news/delete-comment"/>">
				<div class="show-news-comments-header">
					<span><joda:format value="${comment.creationDate}" pattern="MM-dd-yyyy"/></span>
					<input type="submit" value="<s:message code="show.news.delete"/>"/>
				</div>
				<span class="show-news-comments-text"><c:out escapeXml="true" value="${comment.text}"/></span>
				<input type="hidden" name="newsId" value="${newsDTO.id}"/>
				<input type="hidden" name="commentId" value="${comment.commentId}"/>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			</form>
		</c:forEach>
	</div>
	
	<div class="row col-12 show-news-comments-add-comment">
		<sf:form class="col-12" method="POST" commandName="comment">
			<div class="show-news-comments-add-comment-error">
				<sf:errors path="text"/>
				<span id="add-comment-error-message"><s:message code="comment.text.error"/></span>
			</div>
			<sf:input type="hidden" path="newsId" value="${newsDTO.id}"/>
			<sf:textarea id="show-news-add-comment-input" htmlEscape="true" rows="3" name="text" maxlength="100" path="text" cssErrorClass="add-comment-error-style"/> 
			<input type="submit" onclick="return validateComment()" value="<s:message code="show.news.post"/>" formaction="<s:url value="/show-news/post-comment"/>">
		</sf:form>
	</div>
</div>