<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>


<div class="row col-12 modified-news">
	<div class="row col-12 modified-news-navigation-buttons">
		<a href="<s:url value="/list-of-news"/>"><s:message code="modified.news.back"/></a>
	</div>
	
	<div class="row col-12 modified-news-body">
		<div class="modified-news-title">
			<span><c:out escapeXml="true" value="${newsDTO.title}"/></span>
		</div>
		<div class="modified-news-author-date">
			<span>(by <c:out escapeXml="true" value="${newsDTO.author.name}"/>)</span>
			<span><joda:format value="${newsDTO.modificationDate}" pattern="MM-dd-yyyy"/></span>
		</div>
		<div class="modified-news-content">
			<span><c:out escapeXml="true" value="${newsDTO.fullText}"/></span>
		</div>
	</div>
	
	<div class="row col-12 modified-news-footer">
		<c:forEach items="${newsDTO.tags}" var="tag" varStatus="status">
			<span><c:out escapeXml="true" value="${tag.name}"/></span>
			<c:if test="${not status.last}"><span>,</span></c:if>
		</c:forEach>
	</div>

</div>