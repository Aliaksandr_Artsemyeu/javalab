<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda"%>

<div class="row col-12 search-filter">
	<div class="col-12 row search-filter-error-messages">
		<span id="empty-filter-error"><s:message code="filter.empty.error" /></span> 
		<span id="reset-filter-error"><s:message code="filter.reset.error" /></span> 
		<span id="delete-news-error"><s:message code="delete.news.error" /></span>
	</div>
	<div class="col-12 row">
		<sf:form id="search-filter-form" commandName="searchCriteria">
			<div class="col-3 author-filter">
				<span><s:message code="filter.author" /></span>
				<ul>
					<sf:checkboxes htmlEscape="true" path="authorId" items="${authorList}" itemLabel="name" itemValue="id" element="li" />
					<li class="local-filter-reset"><input type="button" value="<s:message code="filter.local.reset"/>" onclick="resetAuthor()"></li>
				</ul>
			</div>
			<div class="col-3 tag-filter">
				<span><s:message code="filter.tags" /></span>
				<ul>
					<sf:checkboxes htmlEscape="true" path="tagIdList" items="${tagList}" itemLabel="name" itemValue="id" element="li" />
					<li class="local-filter-reset"><input type="button" value="<s:message code="filter.local.reset"/>" onclick="resetTags()"></li>
				</ul>
			</div>
			<input id="search-performed-element" type="hidden" value="${searchCriteria.searchPerformed}" />
			<input class="col-2 filter-button filter" type="submit" value="<s:message code="filter.filter"/>" onclick="return validateFilter()" formaction="<s:url value="/list-of-news/filter-news"/>" />
			<input class="col-2 filter-button reset" type="submit" value="<s:message code="filter.reset"/>" onclick="return validateReset()" formaction="<s:url value="/list-of-news/reset-filter"/>" />
			<input class="col-2 filter-button delete" type="submit" value="<s:message code="filter.delete"/>" onclick="return validateDelete()" formaction="<s:url value="/list-of-news/delete-news"/>" formmethod="POST" />
		</sf:form>
	</div>
</div>

<div class="row col-12 news-list">
	<c:choose>
		<c:when test="${fn:length(newsList) != 0}">
			<form method="POST">
				<c:forEach items="${newsList}" var="newsItem">
					<div class="news-item">
						<div class="row">
							<div class="news-item-header">
								<a class="col-6" href="<s:url value="/show-news/${newsItem.id}"/>"><c:out escapeXml="true" value="${newsItem.title}"/></a>
								<span class="col-4 news-author">(by <c:out escapeXml="true" value="${newsItem.author.name}"/>)</span> 
								<span class="col-2 news-creation-date"><joda:format value="${newsItem.creationDate}" pattern="MM-dd-yyyy" /></span>
							</div>
						</div>
						<div class="row">
							<div class="col-12 news-item-content">
								<span><c:out escapeXml="true" value="${newsItem.shortText}"/></span>
							</div>
						</div>
						<div class="row">
							<div class="col-12 news-item-footer">
								<input class="news-item-delete-check" type="checkbox" name="newsIdArr" value="${newsItem.id}" form="search-filter-form"> 
								<a href="<s:url value="/update-news/${newsItem.id}"/>"><s:message code="news.list.edit" /></a> 
								<span><s:message code="news.list.comments" />(${fn:length(newsItem.comments)})</span>
								<div class="news-item-tagslist">
									<c:forEach items="${newsItem.tags}" var="tag" varStatus="status">
										<span><c:out escapeXml="true" value="${tag.name}"/></span>
										<c:if test="${not status.last}">
											<span>,</span>
										</c:if>
									</c:forEach>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</form>
		</c:when>
		<c:otherwise>
			<div class="empty-search-result-message">
				<span><s:message code="emtry.search.result" /></span>
			</div>
		</c:otherwise>
	</c:choose>
</div>

<div class="row col-12 news-pagination">
	<c:forEach begin="1" end="${fn:length(newsListWrapper.idList)}" step="${newsListWrapper.numberOfNewsOnPage}" varStatus="loopCounter">
		<c:set var="count" value="${loopCounter.count}" />
		<a class="news-pagination-button" href="<s:url value="/list-of-news?page=${count - 1}"/>">${count}</a>
	</c:forEach>
</div>

<script>
$("input:checkbox[name='authorId']").change(function() {
	var group = ":checkbox[name='authorId']";
	if ($(this).is(':checked')) {
		$(group).not($(this)).attr("checked", false);
	}
});
</script>