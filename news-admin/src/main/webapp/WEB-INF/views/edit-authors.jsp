<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>
<jsp:useBean id="now" class="java.util.Date" />
<div class="row col-12 edit-authors">
	<div class="row col-12 edit-authors-error-message">
		<sf:form class="col-12" commandName="author">
			<sf:errors path="name" />
			<span id="edit-authors-error-message-client-side"><s:message code="author.save.error"/></span>
		</sf:form>
	</div>
	<span class="row edit-authors-title"><s:message code="edit.authors.add"/></span>
	<sf:form class="row col-12 edit-authors-add-form" method="POST" commandName="author">
		<sf:input htmlEscape="true" class="edit-authors-author-name" type="text" maxlength="30" path="name" />
		<input type="submit" onclick="return validateAuthor()" value="<s:message code="edit.authors.save"/>" formaction="<s:url value="/edit-authors/add-author"/>">
	</sf:form>
	<span class="row edit-authors-title"><s:message code="edit.authors.authors"/></span>
	<c:forEach items="${authorList}" var="author">
		<div class="row col-12 edit-authors-author">
			<sf:form class="col-12" method="POST" commandName="author">
				<joda:format value="${author.expired}" pattern="MM-dd-yyyy" var="formatExpired"/>
				<sf:input type="hidden" path="id" value="${author.id}"/>
				<sf:input type="hidden" path="expired" value="${formatExpired}"/>
				<sf:input htmlEscape="true" class="edit-authors-author-name" maxlength="30" disabled="${(author.id != authorId) ? 'true' : 'false'}" type="text" path="name" value="${author.name}" />
				<c:choose>
					<c:when test="${author.id == authorId}">
						<div class="row col-12 edit-authors-author-edit-buttons">
							<input type="submit" onclick="return validateAuthor()" value="<s:message code="edit.authors.update"/>" formaction="<s:url value="/edit-authors/${author.id}"/>"/>
							<input type="submit" value="<s:message code="edit.authors.expire"/>" formaction="<s:url value="/edit-authors/expire-author"/>"/>
							<a href="<s:url value="/edit-authors"/>"><s:message code="edit.authors.cancel"/></a>
						</div>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${author.expired.millis lt now.time}">
								<span><s:message code="edit.authors.expired"/></span>
							</c:when>
							<c:otherwise>
								<a href="<s:url value="/edit-authors/${author.id}"/>"><s:message code="edit.authors.edit"/></a>
							</c:otherwise>	
						</c:choose>
					</c:otherwise>			
				</c:choose>
			</sf:form>
		</div>
	</c:forEach>
</div>