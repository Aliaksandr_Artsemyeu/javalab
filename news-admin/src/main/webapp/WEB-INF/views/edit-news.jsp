<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<sf:form class="col-12 edit-news-form" method="POST"
	commandName="DTONews" onsubmit="return validateEditNews()">
	<sf:hidden path="id" />

	<div class="row col-12 edit-news-error-message">
		<sf:errors path="title" />
		<span id="edit-news-error-message-client-side"><s:message code="edit.news.error.client" /></span>
	</div>
	<div class="row col-12 edit-news-input">
		<span class="col-2"><s:message code="edit.news.title" />:</span>
		<sf:input class="col-10 edit-news-user-input" htmlEscape="true" type="text" maxlength="30" path="title" cssErrorClass="edit-news-error-class" />
	</div>

	<div class="row col-12 edit-news-error-message">
		<c:choose>
			<c:when test="${not empty DTONews.id}">
				<sf:errors path="modificationDate" />
			</c:when>
			<c:otherwise>
				<sf:errors path="creationDate" />
			</c:otherwise>
		</c:choose>
		<span id="edit-news-error-date-client-side"><s:message code="edit.news.date.error" /></span>
	</div>
	<div class="row edit-news-input">
		<span class="col-2"><s:message code="edit.news.date" />:</span>
		<c:choose>
			<c:when test="${not empty DTONews.id}">
				<sf:input class="col-10 edit-news-user-input" htmlEscape="true" id="edit-news-input-date" type="text" path="modificationDate" cssErrorClass="edit-news-error-class" />
			</c:when>
			<c:otherwise>
				<sf:input class="col-10 edit-news-user-input" htmlEscape="true" id="edit-news-input-date" type="text" path="creationDate" cssErrorClass="edit-news-error-class" />
			</c:otherwise>
		</c:choose>
	</div>

	<div class="row col-12 edit-news-error-message">
		<sf:errors path="shortText" />
	</div>
	<div class="row edit-news-input">
		<span class="col-2"><s:message code="edit.news.brief" />:</span>
		<sf:textarea class="col-10 edit-news-user-input" htmlEscape="true" rows="3" name="text" maxlength="100" path="shortText" cssErrorClass="edit-news-error-class" />
	</div>

	<div class="row col-12 edit-news-error-message">
		<sf:errors path="fullText" />
	</div>
	<div class="row edit-news-input">
		<span class="col-2"><s:message code="edit.news.content" />:</span>
		<sf:textarea class="col-10 edit-news-user-input" htmlEscape="true" rows="10" name="text" maxlength="2000" path="fullText" cssErrorClass="edit-news-error-class" />
	</div>

	<div class="row col-12 edit-news-author-error-message">
		<sf:errors path="author" />
	</div>
	<div class="row col-12 author-tags-filter">
		<div class="author-filter edit-news-author">
			<span><s:message code="filter.author" /></span>
			<ul>
				<sf:checkboxes path="author" items="${validAuthorList}" itemLabel="name" itemValue="id" element="li" />
				<li class="local-filter-reset"><input type="button" value="<s:message code="filter.local.reset"/>" onclick="resetEditAuthor()"></li>
			</ul>
		</div>
		<div class="tag-filter edit-news-tags">
			<span><s:message code="filter.tags" /></span>
			<ul>
				<sf:checkboxes path="tags" items="${tagList}" itemLabel="name" itemValue="id" element="li" />
				<li class="local-filter-reset"><input type="button" value="<s:message code="filter.local.reset"/>" onclick="resetEditTags()"></li>
			</ul>
		</div>
	</div>
	<div class="row col-12 edit-news-submit-button">
		<c:choose>
			<c:when test="${not empty DTONews.id}">
				<a href="<s:url value="/update-news/${DTONews.id}"/>"><s:message code="edit.news.cancel" /></a>
				<input type="submit" value="<s:message code="edit.news.update"/>" formaction="<s:url value="/update-news/${DTONews.id}"/>">
			</c:when>
			<c:otherwise>
				<input type="submit" value="<s:message code="edit.news.save"/>">
			</c:otherwise>
		</c:choose>
	</div>
</sf:form>

<script>
	$("input:checkbox[name='author']").change(function() {
		var group = ":checkbox[name='author']";
		if ($(this).is(':checked')) {
			$(group).not($(this)).attr("checked", false);
		}
	});
</script>