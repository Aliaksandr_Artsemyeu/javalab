<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row col-12 error-message">
	<h1>404</h1>
	<span><s:message code="error.message.404"/></span>
	<a href="<s:url value="/list-of-news"/>"><img src="<s:url value="/resources/img/home.png"/>" alt="<s:message code="error.message.home"/>"></a>
</div>
