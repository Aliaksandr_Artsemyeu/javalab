<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row col-12 login-page">
	<div class="col-12 login-page-form">
		<c:choose>
			<c:when test="${not empty param.error}">
				<span><s:message code="login.error.invalid"/></span>
			</c:when>	
			<c:when test="${not empty param.logout}">
				<span><s:message code="login.error.logout"/></span>
			</c:when>			
		</c:choose>
		<span id="login-page-form-client-side-error"><s:message code="login.client.error"/></span>
		<form class="col-12" method="POST" onsubmit="return validateLogin()">
			<span><s:message code="login.error.login"/>:</span>
			<input id="login-page-form-login" type="text" name="username"/>
			<span><s:message code="login.error.password"/>:</span>
			<input id="login-page-form-password" type="password" name="password"/>
			<input type="submit" value="<s:message code="login.error.enter"/>"/>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
	</div>
</div>