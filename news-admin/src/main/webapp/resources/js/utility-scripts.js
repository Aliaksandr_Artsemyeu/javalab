function resetAuthor() {
	var x = document.getElementsByName("authorId");
	var i;
	for (i = 0; i < x.length; i++) {
		if (x[i].type == "checkbox") {
			x[i].checked = false;
		}
	}
}

function resetTags() {
	var x = document.getElementsByName("tagIdList");
	var i;
	for (i = 0; i < x.length; i++) {
		if (x[i].type == "checkbox") {
			x[i].checked = false;
		}
	}
}

function resetEditAuthor() {
	var x = document.getElementsByName("author");
	var i;
	for (i = 0; i < x.length; i++) {
		if (x[i].type == "checkbox") {
			x[i].checked = false;
		}
	}
}

function resetEditTags() {
	var x = document.getElementsByName("tags");
	var i;
	for (i = 0; i < x.length; i++) {
		if (x[i].type == "checkbox") {
			x[i].checked = false;
		}
	}
}

function validateFilter() {
	var author = document.getElementsByName("authorId");
	var tags = document.getElementsByName("tagIdList");
	var valid = false;

	for (i = 0; i < author.length; i++) {
		if (author[i].checked) {
			valid = true;
		}
	}

	for (i = 0; i < tags.length; i++) {
		if (tags[i].checked) {
			valid = true;
		}
	}

	if (!valid) {
		document.getElementById("delete-news-error").style.display = "none";
		document.getElementById("reset-filter-error").style.display = "none";
		document.getElementById("empty-filter-error").style.display = "flex";
	}

	return valid;
}

function validateReset() {
	var searchPerformed = document.getElementById("search-performed-element").value;

	if (searchPerformed == "false") {
		document.getElementById("delete-news-error").style.display = "none";
		document.getElementById("empty-filter-error").style.display = "none";
		document.getElementById("reset-filter-error").style.display = "flex";
		return false;
	}

	return true;
}

function validateDelete() {
	var deleteNews = document.getElementsByName("newsIdArr");
	var valid = false;

	for (i = 0; i < deleteNews.length; i++) {
		if (deleteNews[i].checked) {
			valid = true;
		}
	}

	if (!valid) {
		document.getElementById("empty-filter-error").style.display = "none";
		document.getElementById("reset-filter-error").style.display = "none";
		document.getElementById("delete-news-error").style.display = "flex";
	}

	return valid;
}

function validateComment() {
	var comment = document.getElementById("show-news-add-comment-input");
	var commentLength = comment.value;
	if (commentLength == 0 || commentLength > 100) {
		document.getElementById("add-comment-error-message").style.display = "initial";
		return false;
	}
	return true;
}

function validateTag() {
	var tags = document.getElementsByClassName("edit-tags-tag-name");
	var valid = true;

	for (i = 0; i < tags.length; i++) {
		var tag = tags[i].value;
		if (tag.length == 0 || tag.length > 30) {
			valid = false;
		}
	}

	if (!valid) {
		document.getElementById("edit-tags-error-message-client-side").style.display = "initial";
	}

	return valid;
}

function validateAuthor() {
	var authors = document.getElementsByClassName("edit-authors-author-name");
	var valid = true;

	for (i = 0; i < authors.length; i++) {
		var author = authors[i].value;
		if (author.length == 0 || author.length > 30) {
			valid = false;
		}
	}

	if (!valid) {
		document.getElementById("edit-authors-error-message-client-side").style.display = "initial";
	}

	return valid;
}

function validateLogin() {
	var login = document.getElementById("login-page-form-login").value;
	var password = document.getElementById("login-page-form-password").value;

	if (login.length == 0 || password.length == 0) {
		document.getElementById("login-page-form-client-side-error").style.display = "initial";
		return false;
	}

	return true;
}

function validateEditNews() {
	var userInputs = document.getElementsByClassName("edit-news-user-input");

	var date = document.getElementById("edit-news-input-date").value;
	var datePattern = /\d{2}-\d{2}-\d{4}/;
	var result = datePattern.test(date);

	if (!result) {
		document.getElementById("edit-news-error-date-client-side").style.display = "initial";
	}

	for (i = 0; i < userInputs.length; i++) {
		var userInput = userInputs[i].value;
		if (userInput.length == 0) {
			document.getElementById("edit-news-error-message-client-side").style.display = "initial";
			return false;
		}
	}

	return true;
}