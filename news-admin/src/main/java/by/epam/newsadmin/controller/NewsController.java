package by.epam.newsadmin.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import by.epam.javalab.newscommon.entity.Author;
import by.epam.javalab.newscommon.entity.Comment;
import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.service.AuthorService;
import by.epam.javalab.newscommon.service.NewsManagmentService;
import by.epam.javalab.newscommon.service.NewsService;
import by.epam.javalab.newscommon.service.TagService;
import by.epam.javalab.newscommon.service.exception.ServiceException;
import by.epam.javalab.newscommon.service.util.NewsListWrapper;
import by.epam.javalab.newscommon.service.util.SearchCriteria;

/**
 * This class is a Spring Controller. It is used to map URL requests to specific
 * methods. This methods perform some actions, typically populate Spring Model
 * with attributes and return logical name of the view to be resolved. This
 * Controller bear with the News entity handling.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
@Controller
public class NewsController {

	@Inject
	private NewsManagmentService newsManagmentService;

	@Inject
	private NewsService newsService;

	@Inject
	private AuthorService authorService;

	@Inject
	private TagService tagService;

	@Inject
	private NewsListWrapper newsListWrapper;

	@Inject
	private SearchCriteria searchCriteria;

	/**
	 * This method is mapped to the
	 * http://localhost{:port}/news-admin/list-of-news. It is used to populate
	 * model with the List that contains all the news by default or only those
	 * news that was filtered, with the use of searchCriteria. Also it obtains
	 * separate List that contains the Id values of all filtered news(or all the
	 * existing news if searchCriteria is empty) and pass it to the
	 * newsListWrapper, to care it.
	 * 
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @param page
	 *            number of page, used to obtain sublist of news list.
	 * 
	 * @return logical view name.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 * 
	 */
	@RequestMapping(value = { "/list-of-news" })
	public String listOfNews(Model model, @RequestParam(required = false) Integer page) throws ServiceException {
		List<Long> idSubList = null;
		if (page == null) {
			List<Long> idList = newsService.selectIdList(searchCriteria);
			newsListWrapper.setList(idList);
			idSubList = newsListWrapper.getIdSubList();
		} else {
			idSubList = newsListWrapper.getIdSubList(page);
		}
		model.addAttribute("newsList", newsManagmentService.selectList(idSubList));
		model.addAttribute("newsListWrapper", newsListWrapper);
		model.addAttribute("searchCriteria", searchCriteria);
		model.addAttribute("authorList", authorService.selectEntries());
		model.addAttribute("tagList", tagService.selectEntries());
		return "list-of-news";
	}

	/**
	 * This method is used to perform filtering. SearchCriteria instance is
	 * obtained first, and then newsService use this instance to get list of
	 * news id values, which suite filter criteria.
	 * 
	 * @param submitedSearchCriteria
	 *            SearchCriteria instance, submitted from page.
	 * @return redirect URL.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = { "/list-of-news/filter-news" })
	public String filterNews(SearchCriteria submitedSearchCriteria) throws ServiceException {
		searchCriteria.setAuthorAndTags(submitedSearchCriteria);
		List<Long> idList = newsService.selectIdList(searchCriteria);
		newsListWrapper.setList(idList);
		searchCriteria.setSearchPerformed(true);
		return "redirect:/list-of-news";
	}

	/**
	 * This method is used to reset filter options. In fact it just populates
	 * the Model with the new empty SearchCriteria instance.
	 * 
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return String with the url address to redirect to.
	 */
	@RequestMapping(value = { "/list-of-news/reset-filter" })
	public String resetFilter() {
		searchCriteria.resetAuthorAndTags();
		return "redirect:/list-of-news";
	}

	/**
	 * This method is used to delete selected news. It obtains an array with the
	 * news id values to be deleted, as a request parameter and passes this
	 * array to the service method.
	 * 
	 * @param newsIdArr
	 *            array of type Long. Contains Id values of news records to be
	 *            deleted.
	 * @return String with the url address to redirect to.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = { "/list-of-news/delete-news" }, method = RequestMethod.POST)
	public String deleteSelectedNews(@RequestParam(required = false) Long[] newsIdArr) throws ServiceException {
		newsManagmentService.delete(newsIdArr);
		return "redirect:/list-of-news";
	}

	/**
	 * This method is used to get newsDTO instance that corresponds to the news
	 * record and its dependent records, with the id value equal to the newsId
	 * request parameter. This method populates Model with this newDTO instance
	 * and also sets newsListWrapper curIndex field value with the newsId value.
	 * 
	 * @param newsId
	 *            Id value of the News record.
	 * @param newsListWrapper
	 *            NewsListWrapper instance, used to store list of selected news
	 *            id values.
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return logical view name.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = { "/show-news/{newsId}" })
	public String showNews(@PathVariable Long newsId, Model model) throws ServiceException {
		newsListWrapper.setIndex(newsId);
		model.addAttribute("newsDTO", newsManagmentService.select(newsId));
		model.addAttribute("newsListWrapper", newsListWrapper);
		model.addAttribute("comment", new Comment());
		return "news";
	}

	/**
	 * This method produce the same result as showNews() method, but it used
	 * only when save or update News operations was performed. Simply it is used
	 * to provide page where newly created or updated news is displayed. Also
	 * newsListWrapper is not used here.
	 * 
	 * @param newsId
	 *            Id value of the News record.
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return logical view name.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = { "/modified-news/{newsId}" })
	public String modifiedNews(@PathVariable Long newsId, Model model) throws ServiceException {
		model.addAttribute("newsDTO", newsManagmentService.select(newsId));
		return "modified-news";
	}

	/**
	 * This method is used to provide page with the existing news data,
	 * available for the modification. It gets newsId request parameter, which
	 * corresponds to the value of the news record Id. It invokes
	 * newsManagmentService to get instance of newDTO class, that represents
	 * news with given Id. Populates Model with this instance.
	 * 
	 * @param newsId
	 *            Id value of the News record.
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return logical view name.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = { "/update-news/{newsId}" }, method = RequestMethod.GET)
	public String editNews(@PathVariable Long newsId, Model model) throws ServiceException {
		populateModelwithAuthorTag(model, newsManagmentService.select(newsId));
		return "edit-news";
	}

	/**
	 * This method is used to invoke newsManagmentService update method, to
	 * perform update of News data.
	 * 
	 * @param dtoNews
	 *            DTONews instance, represents News record and its dependent
	 *            records.
	 * @return String with the URL address to redirect to.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = { "/update-news/{newsId}" }, method = RequestMethod.POST)
	public String updateNews(@Valid News dtoNews, Errors errors, Model model) throws ServiceException {

		if (errors.hasErrors()) {
			populateModelwithAuthorTag(model, dtoNews);
			return "edit-news";
		}

		newsManagmentService.update(dtoNews, dtoNews.getAuthor(), dtoNews.getTags());
		return "redirect:/modified-news/" + dtoNews.getId();
	}

	/**
	 * This method provides logical view name of the web page, which is used for
	 * creating new News records.
	 * 
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return logical view name
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = { "/add-news" }, method = RequestMethod.GET)
	public String addNews(Model model) throws ServiceException {
		populateModelwithAuthorTag(model, new News());
		return "edit-news";
	}

	/**
	 * This method is used to perform insertion of a new News record to the
	 * database, by invoking newsManagmentService insert method. DTONews
	 * instance is validated before been passed to the method. If its not valid
	 * the same web page will be returned with the invalid fields signed in red.
	 * 
	 * @param dtoNews
	 *            DTONews instance, represents News record and its dependent
	 *            records.
	 * @param errors
	 *            contain information about invalid fields.
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return logical view name if there is a validation error, or redirect
	 *         string with the URL address.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = { "/add-news" }, method = RequestMethod.POST)
	public String saveNews(@Valid News dtoNews, Errors errors, Model model) throws ServiceException {

		if (errors.hasErrors()) {
			populateModelwithAuthorTag(model, dtoNews);
			return "edit-news";
		}

		Long newsId = newsManagmentService.insert(dtoNews, dtoNews.getAuthor(), dtoNews.getTags());
		return "redirect:/modified-news/" + newsId;
	}

	/**
	 * This method is created to minimize code duplication. It is used to
	 * populate Model with the List of Authors and with the List of Tags. Also
	 * it checks if the News author is expired already it adds this author to
	 * Author List.
	 * 
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @param news
	 *            DTONews instance, represents News record and its dependent
	 *            records.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	private void populateModelwithAuthorTag(Model model, News news) throws ServiceException {
		List<Author> authorList = authorService.selectUnexpiredEntries();
		Author author = news.getAuthor();
		if (author != null && author.getExpired().isBeforeNow()) {
			authorList.add(author);
		}
		model.addAttribute("DTONews", news);
		model.addAttribute("validAuthorList", authorList);
		model.addAttribute("tagList", tagService.selectEntries());
	}

}
