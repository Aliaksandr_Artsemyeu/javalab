package by.epam.newsadmin.controller.utils;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import by.epam.javalab.newscommon.entity.Author;
import by.epam.javalab.newscommon.service.AuthorService;
import by.epam.javalab.newscommon.service.exception.ServiceException;

public class AuthorConverter implements Converter<String, Author> {

	private static final Logger logger = Logger.getLogger(AuthorConverter.class);

	private AuthorService authorService;

	public AuthorConverter() {}

	public AuthorConverter(AuthorService authorService) {
		this.authorService = authorService;
	}

	@Override
	public Author convert(String source) {
		try {
			return authorService.select(Long.parseLong(source));
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			return new Author(Long.parseLong(source));
		}
	}

}
