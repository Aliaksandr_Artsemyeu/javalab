package by.epam.newsadmin.controller;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;

/**
 * This class is a Spring ControllerAdvice. It is used to map URL requests to
 * specific methods. This methods perform some actions, typically populate
 * Spring Model with attributes and return logical name of the view to be
 * resolved. This Controller bear with the HTTP Errors and Exceptions that
 * occurred during execution of the application.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
@ControllerAdvice
@RequestMapping(path = "/error")
public class ErrorController {
	
	private static final Logger logger = Logger.getLogger(ErrorController.class);

	/**
	 * If there is an HTTP error with status 404 occurred, this method handles
	 * this error simply by providing custom page with the custom message to the
	 * user.
	 * 
	 * @return logical view name.
	 */
	@RequestMapping(path = "/404")
	public String handle_404(Exception e) {
		logger.error(e);
		return "not-found";
	}

	/**
	 * If there is an HTTP error with status 500 occurred, this method handles
	 * this error simply by providing custom page with the custom message to the
	 * user.
	 * 
	 * @return logical view name.
	 */
	@RequestMapping(path = "/500")
	public String handle_500(Exception e) {
		logger.error(e);
		return "internal-error";
	}

	/**
	 * If there is an HTTP error with any status occurred, this method handles
	 * this error simply by providing custom page with the custom message to the
	 * user.
	 * 
	 * @return logical view name.
	 */
	@RequestMapping
	public String handle_error(Exception e) {
		logger.error(e.getMessage(), e);
		return "internal-error";
	}

	/**
	 * If there is an InvalidParameterException occurred during execution of the
	 * application, this method will handle this exception simply by providing
	 * custom page with the custom message to the user. And also it sets the
	 * response HTTP status to 404.
	 * 
	 * @return logical view name.
	 */
	@ExceptionHandler(InvalidParameterException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String invalidParameterHandler(InvalidParameterException e) {
		logger.error(e);
		return "not-found";
	}

	/**
	 * If there is an ServiceException occurred during execution of the
	 * application, this method will handle this exception simply by providing
	 * custom page with the custom message to the user.
	 * 
	 * @return logical view name.
	 */
	@ExceptionHandler(ServiceException.class)
	public String seviceExceptionHandler(ServiceException e) {
		logger.error(e);
		return "internal-error";
	}

	/**
	 * If there is an Exception occurred during execution of the application,
	 * this method will handle this exception simply by providing custom page
	 * with the custom message to the user.
	 * 
	 * @return logical view name.
	 */
	@ExceptionHandler(Exception.class)
	public String exceptionHandler(Exception e) {
		logger.error(e.getMessage(), e);
		return "internal-error";
	}
}
