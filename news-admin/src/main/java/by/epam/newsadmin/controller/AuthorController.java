package by.epam.newsadmin.controller;

import javax.inject.Inject;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import by.epam.javalab.newscommon.entity.Author;
import by.epam.javalab.newscommon.service.AuthorService;
import by.epam.javalab.newscommon.service.exception.ServiceException;

/**
 * This class is a Spring Controller. It is used to map URL requests to specific
 * methods. This methods perform some actions, typically populate Spring Model
 * with attributes and return logical name of the view to be resolved. This
 * Controller bear with the Author entity handling.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
@Controller
@RequestMapping(value = { "/edit-authors" })
public class AuthorController {

	@Inject
	private AuthorService authorService;

	/**
	 * This method provides logical view name of the web page, which is used for
	 * editing and creating Author records.
	 * 
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return logical view name
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping
	public String editAuthors(Model model) throws ServiceException {
		populateModelwithAuthorList(model, new Author());
		return "edit-authors";
	}

	/**
	 * This method is used to get value of the pressed Edit button on the
	 * edit-author web page and to populate the model with this value, so when
	 * the web page will be provided to the user specified input field now will
	 * be available for the modification.
	 * 
	 * @param authorId
	 *            value of the pressed Edit button that is equals to the author
	 *            Id.
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return logical view name
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = { "/{authorId}" })
	public String editAuthors(@PathVariable int authorId, Model model) throws ServiceException {
		populateModelwithAuthorList(model, new Author());
		model.addAttribute("authorId", authorId);
		return "edit-authors";
	}

	/**
	 * This method is used to update Author record in database. It gets Author
	 * instance as a request parameter then invokes authorService update method
	 * and pass this instance to that method.
	 * 
	 * @param author
	 *            instance of Author.
	 * @param errors
	 *            Spring entity used to store information about errors during
	 *            validation.
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return String with the URL address for the redirection.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = "/{authorId}", method = RequestMethod.POST)
	public String updateAuthor(@Valid Author author, Errors errors, Model model) throws ServiceException {

		if (errors.hasErrors()) {
			populateModelwithAuthorList(model, author);
			return "edit-authors";
		}

		authorService.update(author);
		return "redirect:/edit-authors";
	}

	/**
	 * This method is used to update EXIRED column value of the AUTHOR table in
	 * database. It gets Author instance as a request parameter then sets its
	 * expired field to the current date and invokes authorService update method
	 * with this instance as method attribute.
	 * 
	 * @param author
	 *            instance of Author.
	 * @return String with the URL address for the redirection.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = "/expire-author", method = RequestMethod.POST)
	public String expireAuthor(Author author) throws ServiceException {
		author.setExpired(new DateTime());
		authorService.update(author);
		return "redirect:/edit-authors";
	}

	/**
	 * This method is used to insert new record to the AUTHOR table in database.
	 * It gets Author instance as a request parameter and then invokes
	 * authorService insert method with this instance as method attribute.
	 * 
	 * @param author
	 *            instance of Author.
	 * @param errors
	 *            Spring entity used to store information about errors during
	 *            validation.
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return String with the URL address for the redirection.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = "/add-author", method = RequestMethod.POST)
	public String addAuthor(@Valid Author author, Errors errors, Model model) throws ServiceException {

		if (errors.hasErrors()) {
			populateModelwithAuthorList(model, author);
			return "edit-authors";
		}

		authorService.insert(author);
		return "redirect:/edit-authors";
	}

	/**
	 * This method is created to minimize code duplication. It is used to
	 * populate Model with List of Authors
	 * 
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @param author
	 *            instance of Author.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	private void populateModelwithAuthorList(Model model, Author author) throws ServiceException {
		model.addAttribute("authorList", authorService.selectEntries());
		model.addAttribute("author", author);
	}

}
