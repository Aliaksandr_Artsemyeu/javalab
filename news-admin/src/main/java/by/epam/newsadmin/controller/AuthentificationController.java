package by.epam.newsadmin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This class is a Spring Controller. It is used to map URL requests to specific
 * methods. This methods perform some actions, typically populate Spring Model
 * with attributes and return logical name of the view to be resolved.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
@Controller
public class AuthentificationController {

	/**
	 * This method is used only to produce logical name of view, which is custom
	 * login page.
	 * 
	 * @return logical name of the view
	 */
	@RequestMapping(value = "/login")
	public String login() {
		return "login";
	}

}
