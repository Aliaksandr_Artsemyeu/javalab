package by.epam.newsadmin.controller.utils;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import by.epam.javalab.newscommon.entity.Tag;
import by.epam.javalab.newscommon.service.TagService;
import by.epam.javalab.newscommon.service.exception.ServiceException;

public class TagConverter implements Converter<String, Tag> {

	private static final Logger logger = Logger.getLogger(TagConverter.class);

	private TagService tagService;

	public TagConverter() {}

	public TagConverter(TagService tagService) {
		this.tagService = tagService;
	}

	@Override
	public Tag convert(String source) {
		try {
			return tagService.select(Long.parseLong(source));
		} catch (ServiceException e) {
			logger.error(e.getMessage(), e);
			return new Tag(Long.parseLong(source));
		}
	}

}
