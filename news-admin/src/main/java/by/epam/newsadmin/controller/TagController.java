package by.epam.newsadmin.controller;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import by.epam.javalab.newscommon.entity.Tag;
import by.epam.javalab.newscommon.service.TagService;
import by.epam.javalab.newscommon.service.exception.ServiceException;

/**
 * This class is a Spring Controller. It is used to map URL requests to specific
 * methods. This methods perform some actions, typically populate Spring Model
 * with attributes and return logical name of the view to be resolved. This
 * Controller bear with the Tag entity handling.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
@Controller
@RequestMapping({ "/edit-tags" })
public class TagController {

	@Inject
	private TagService tagService;

	/**
	 * This method provides logical view name of the web page, which is used for
	 * editing and creating Tag records.
	 * 
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return logical view name
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping
	public String editTags(Model model) throws ServiceException {
		populateModelwithTagList(model, new Tag());
		return "edit-tags";
	}

	/**
	 * This method is used to get value of the pressed Edit button on the
	 * edit-tags web page and to populate the model with this value, so when the
	 * web page will be provided to the user specified input field now will be
	 * available for the modification.
	 * 
	 * @param tagId
	 *            value of the pressed Edit button equals to tag Id.
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return logical view name
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = { "/{tagId}" })
	public String editTag(@PathVariable int tagId, Model model) throws ServiceException {
		populateModelwithTagList(model, new Tag());
		model.addAttribute("tagId", tagId);
		return "edit-tags";
	}

	/**
	 * This method is used to update Tag record in database. It gets Tag
	 * instance as a request parameter then invokes tagService update method and
	 * pass this instance to that method.
	 * 
	 * @param tag
	 *            instance of Tag.
	 * @param errors
	 *            Spring entity used to store information about errors during
	 *            validation.
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return String with the URL address for the redirection.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = "/{tagId}", method = RequestMethod.POST)
	public String updateTag(@Valid Tag tag, Errors errors, Model model) throws ServiceException {

		if (errors.hasErrors()) {
			populateModelwithTagList(model, tag);
			return "edit-tags";
		}

		tagService.update(tag);
		return "redirect:/edit-tags";
	}

	/**
	 * This method is used to delete Tag record from the database. It gets Tag
	 * instance as a request parameter and invokes tagService delete method with
	 * this instance as methods attribute.
	 * 
	 * @param tag
	 *            instance of Tag.
	 * @return String with the URL address for the redirection.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = "/delete-tag", method = RequestMethod.POST)
	public String deleteTag(Tag tag) throws ServiceException {
		tagService.delete(tag.getId());
		return "redirect:/edit-tags";
	}

	/**
	 * This method is used to insert new record to the TAG table in database. It
	 * gets Tag instance as a request parameter and then invokes tagService
	 * insert method with this instance as method attribute.
	 * 
	 * @param tag
	 *            instance of Tag.
	 * @param errors
	 *            Spring entity used to store information about errors during
	 *            validation.
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @return String with the URL address for the redirection.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = "/add-tag", method = RequestMethod.POST)
	public String addTag(@Valid Tag tag, Errors errors, Model model) throws ServiceException {

		if (errors.hasErrors()) {
			populateModelwithTagList(model, tag);
			return "edit-tags";
		}

		tagService.insert(tag);
		return "redirect:/edit-tags";
	}

	/**
	 * This method is created to minimize code duplication. It is used to
	 * populate Model with List of Tags
	 * 
	 * @param model
	 *            Spring Model, used to care request attributes.
	 * @param tag
	 *            instance of Tag.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	private void populateModelwithTagList(Model model, Tag tag) throws ServiceException {
		model.addAttribute("tagList", tagService.selectEntries());
		model.addAttribute("tag", tag);
	}

}
