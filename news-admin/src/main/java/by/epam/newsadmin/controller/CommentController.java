package by.epam.newsadmin.controller;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import by.epam.javalab.newscommon.entity.Comment;
import by.epam.javalab.newscommon.service.CommentsService;
import by.epam.javalab.newscommon.service.NewsManagmentService;
import by.epam.javalab.newscommon.service.exception.ServiceException;
import by.epam.javalab.newscommon.service.util.NewsListWrapper;

/**
 * This class is a Spring Controller. It is used to map URL requests to specific
 * methods. This methods perform some actions, typically populate Spring Model
 * with attributes and return logical name of the view to be resolved. This
 * Controller bear with the Comment entity handling.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
@Controller
@RequestMapping({ "/show-news" })
public class CommentController {

	@Inject
	private CommentsService commentsService;
	
	@Inject
	private NewsManagmentService newsManagmentService;
	
	@Inject
	private NewsListWrapper newsListWrapper;

	/**
	 * This method is used to delete news comments. It gets newsId and commentId
	 * as request attributes and then invokes commentsService delete method with
	 * the commentId passed as method attribute.
	 * 
	 * @param newsId
	 *            Id of the News record.
	 * @param commentId
	 *            Id of the Comment record.
	 * @return String with the URL address for the redirection.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = "/delete-comment", method = RequestMethod.POST)
	public String deleteComment(@RequestParam Long newsId, @RequestParam Long commentId) throws ServiceException {
		commentsService.delete(commentId);
		return "redirect:/show-news/" + newsId;
	}

	/**
	 * This method is used to add new comments. It gets Comment entity as Model
	 * attribute and passes it to the commentsService insert method as a method
	 * attribute. Also it checks the error instance, if it contains errors user
	 * will be returned to the same page with the invalid field signed in red.
	 * 
	 * @param comment
	 *            instance of Comment, represents new Comment entry.
	 * @param errors
	 *            contains information about invalid fields.
	 * @return logical view name if there is a validation error, or redirect
	 *         string with the URL address.
	 * @throws ServiceException
	 *             if ServiceException occurred during service methods
	 *             execution.
	 */
	@RequestMapping(value = "/post-comment", method = RequestMethod.POST)
	public String addComment(@Valid Comment comment, Errors errors, Model model) throws ServiceException {

		if (errors.hasErrors()) {
			model.addAttribute("newsDTO", newsManagmentService.select(comment.getNewsId()));
			model.addAttribute("newsListWrapper", newsListWrapper);
			return "news";
		}

		commentsService.insert(comment);
		return "redirect:/show-news/" + comment.getNewsId();
	}
}
