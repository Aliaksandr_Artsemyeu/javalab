package by.epam.newsadmin.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * This class is used, to set up default configurations during, servlet
 * initializing.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class NewsAdminWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	/**
	 * Specify @Configuration and/or @Component classes to be provided to the
	 * root application context.
	 * 
	 * @return the configuration classes for the root application context, or
	 *         null if creation and registration of a root context is not
	 *         desired
	 */
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] { RootConfig.class, SecurityConfig.class };
	}

	/**
	 * Specify @Configuration and/or @Component classes to be provided to the
	 * dispatcher servlet application context.
	 * 
	 * @return the configuration classes for the dispatcher servlet application
	 *         context (may not be empty or null).
	 */
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { WebConfig.class };
	}

	/**
	 * Specify the servlet mapping(s) for the DispatcherServlet, e.g. '/',
	 * '/app', etc.
	 */
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		servletContext.setInitParameter("spring.profiles.active", "development,hibernate");
		servletContext.setInitParameter("defaultHtmlEscape", "true");
		super.onStartup(servletContext);
	}
	
}
