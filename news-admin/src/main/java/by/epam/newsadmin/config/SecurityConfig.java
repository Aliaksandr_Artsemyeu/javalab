package by.epam.newsadmin.config;

import javax.inject.Inject;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

import by.epam.javalab.newscommon.service.UserService;

/**
 * This class provides Java-based configuration for Spring Security.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Inject
	private UserService userService;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
		encodingFilter.setEncoding("UTF-8");
		encodingFilter.setForceEncoding(true);
		
		http
		.authorizeRequests()
			.antMatchers("/resources/**").permitAll()
			.antMatchers("/login*").permitAll()
			.anyRequest().hasAuthority("ROLE_ADMIN")
		.and()
		.formLogin()
			.loginPage("/login").permitAll()
			.defaultSuccessUrl("/list-of-news", true)
			.failureUrl("/login?error=true")
		.and()
		.logout()
			.permitAll()
			.logoutUrl("/logout").permitAll()
			.logoutSuccessUrl("/login?logout=true").permitAll()
		.and().addFilterBefore(encodingFilter, CsrfFilter.class);
		}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
		.userDetailsService(userService)
		.passwordEncoder(new Md5PasswordEncoder());
	}
	
}
