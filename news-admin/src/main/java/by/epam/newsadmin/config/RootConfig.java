package by.epam.newsadmin.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import by.epam.javalab.newscommon.service.AuthorService;

import by.epam.javalab.newscommon.service.TagService;
import by.epam.newsadmin.controller.utils.AuthorConverter;
import by.epam.newsadmin.controller.utils.TagConverter;

/**
 * This class provides Java-based configuration of root application context. It
 * contains Beans that will be used in this application.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
@Configuration
@ImportResource("classpath*:/commonApplicationContext.xml")
public class RootConfig {

	@Bean
	public TagConverter tagConverter(TagService tagService) {
		return new TagConverter(tagService);
	}

	@Bean
	AuthorConverter authorConverter(AuthorService authorService) {
		return new AuthorConverter(authorService);
	}

}
