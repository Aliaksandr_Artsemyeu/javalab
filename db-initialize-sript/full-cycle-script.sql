DROP TABLE News_Author;
DROP TABLE News_Tag;
DROP TABLE Roles;
DROP TABLE TUser;
DROP TABLE Comments;
DROP TABLE News;
DROP TABLE Author;
DROP TABLE Tag;
DROP SEQUENCE NEWS_ID_seq;
DROP SEQUENCE AUTHOR_ID_seq;
DROP SEQUENCE TAG_ID_seq;
DROP SEQUENCE COMMENT_ID_seq;
DROP SEQUENCE USER_ID_seq;
/
    
CREATE TABLE NEWS (
    NEWS_ID            NUMBER(20) NOT NULL,
    TITLE              NVARCHAR2(30) NOT NULL,
    SHORT_TEXT         NVARCHAR2(100) NOT NULL,
    FULL_TEXT          NVARCHAR2(2000) NOT NULL,
    CREATION_DATE      TIMESTAMP NOT NULL,
    MODIFICATION_DATE  DATE NOT NULL,
    constraint News_PK primary key (NEWS_ID)
);
/

CREATE SEQUENCE NEWS_ID_seq;
/

CREATE TABLE AUTHOR (
    AUTHOR_ID   NUMBER(20) NOT NULL,
    AUTHOR_NAME NVARCHAR2(30) NOT NULL,
    EXPIRED     TIMESTAMP,
    constraint  Author_PK primary key (AUTHOR_ID)
);
/

CREATE SEQUENCE AUTHOR_ID_seq;
/

CREATE TABLE NEWS_AUTHOR (
    NEWS_ID    NUMBER(20) NOT NULL,
    AUTHOR_ID  NUMBER(20) NOT NULL
);
/

ALTER TABLE NEWS_AUTHOR ADD CONSTRAINT NEWS_AUTHOR_FK 
FOREIGN KEY (NEWS_ID)
REFERENCES NEWS (NEWS_ID);

ALTER TABLE NEWS_AUTHOR ADD CONSTRAINT NEWS_AUTHOR_FK2 
FOREIGN KEY (AUTHOR_ID)
REFERENCES AUTHOR (AUTHOR_ID);

CREATE TABLE TAG (
    TAG_ID     NUMBER(20) NOT NULL,
    TAG_NAME   NVARCHAR2(30) NOT NULL,
    constraint Tag_PK primary key (TAG_ID)
);
/

CREATE SEQUENCE TAG_ID_seq;
/

CREATE TABLE NEWS_TAG (
    NEWS_ID    NUMBER(20) NOT NULL,
    TAG_ID     NUMBER(20) NOT NULL
);
/

ALTER TABLE NEWS_TAG ADD CONSTRAINT NEWS_TAG_FK 
FOREIGN KEY (NEWS_ID)
REFERENCES NEWS (NEWS_ID);

ALTER TABLE NEWS_TAG ADD CONSTRAINT NEWS_TAG_FK2 
FOREIGN KEY (TAG_ID)
REFERENCES TAG (TAG_ID);

CREATE TABLE COMMENTS (
    COMMENT_ID    NUMBER(20) NOT NULL,
    NEWS_ID       NUMBER(20) NOT NULL,
    COMMENT_TEXT  NVARCHAR2(100) NOT NULL,
    CREATION_DATE TIMESTAMP NOT NULL,
    constraint Comments_PK primary key (COMMENT_ID)
);
/

CREATE SEQUENCE COMMENT_ID_seq;
/

ALTER TABLE COMMENTS ADD CONSTRAINT COMMENTS_FK 
FOREIGN KEY (NEWS_ID)
REFERENCES NEWS (NEWS_ID);

CREATE TABLE TUSER (
    USER_ID    NUMBER(20) NOT NULL,
    USER_NAME  NVARCHAR2(50) NOT NULL,
    LOGIN      VARCHAR2(30) NOT NULL,
    PASSWORD   VARCHAR2(35) NOT NULL,
    constraint User_PK primary key (USER_ID)
);
/

CREATE SEQUENCE USER_ID_seq;
/

CREATE TABLE ROLES (
    USER_ID    NUMBER(20) NOT NULL,
    ROLE_NAME  VARCHAR2(50) NOT NULL
);

ALTER TABLE Roles ADD CONSTRAINT ROLES_FK 
FOREIGN KEY (USER_ID)
REFERENCES TUSER (USER_ID);
/

    DECLARE x NUMBER := 1;
    BEGIN    
    FOR i IN 1..20 LOOP
        INSERT INTO News (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE)
        VALUES (
        NEWS_ID_seq.nextval,
        'Neque porro quisquam est '||to_char(x),
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer blandit tellus a ante consectetur.',
        'Nulla pellentesque risus in quam imperdiet, in commodo ante tristique. Mauris dignissim ex arcu, in ultrices lorem volutpat at.
Morbi fermentum pulvinar malesuada. Proin leo mauris, consectetur ut bibendum vitae, elementum eu risus.
Sed vulputate augue vitae rhoncus pellentesque. Quisque non nibh nibh. Quisque at placerat nisi. Morbi aliquam laoreet felis in tempus.
Phasellus laoreet rutrum volutpat.

Nam sit amet orci finibus lacus eleifend viverra vitae nec dui. Nam quis fermentum odio.
Proin a nunc dictum, dignissim ex at, lacinia odio. Donec eleifend viverra lacinia.
Aenean in ornare justo. Donec quis orci ante. Nullam malesuada vulputate risus, ut auctor arcu mollis in.
Pellentesque laoreet lobortis orci, in viverra lacus condimentum sit amet.

Aenean nisl neque, iaculis a tincidunt eget, iaculis id ipsum. Nulla fringilla mauris eget libero facilisis, in tempor erat imperdiet.
Nullam tempor commodo porttitor. Phasellus varius feugiat justo tempus tempor. Nam dapibus metus vitae metus sagittis sodales.
Aenean quis volutpat mauris. Maecenas convallis cursus augue nec placerat. Suspendisse potenti. Pellentesque eget luctus tellus.
Suspendisse id dapibus tellus. Vestibulum orci quam, volutpat commodo sollicitudin ac, aliquet in libero.
Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. 
Morbi at tortor in tellus rutrum egestas iaculis nec orci.

Cras ac tempor lacus, nec placerat odio. Curabitur porta sapien velit, pellentesque laoreet augue posuere et. 
Nam quis mauris elit. Donec id tristique elit. Vivamus non eleifend lectus. Nullam lorem neque, tincidunt eget ultrices in, pretium id erat. 
Etiam in ligula nec enim tempor suscipit quis non dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
In hac habitasse platea dictumst. Vivamus sagittis, velit eget scelerisque dapibus, quam augue vestibulum risus, eget 
imperdiet augue nulla sit amet libero.',
        CURRENT_TIMESTAMP,
        CURRENT_DATE);
    x := x + 1;
    END LOOP;
    COMMIT;
    END;
/
    
   INSERT INTO Author (AUTHOR_ID, AUTHOR_NAME, EXPIRED)
   VALUES (AUTHOR_ID_SEQ.nextval, 'Jhon Lock', '16-SEP-17 08.53.09.968000 PM');
   INSERT INTO Author (AUTHOR_ID, AUTHOR_NAME, EXPIRED)
   VALUES (AUTHOR_ID_SEQ.nextval, 'Jack The Sparrow', '16-SEP-17 08.53.09.968000 PM');
   INSERT INTO Author (AUTHOR_ID, AUTHOR_NAME, EXPIRED)
   VALUES (AUTHOR_ID_SEQ.nextval, 'Davy Jones', '16-SEP-17 08.53.09.968000 PM');
   INSERT INTO Author (AUTHOR_ID, AUTHOR_NAME, EXPIRED)
   VALUES (AUTHOR_ID_SEQ.nextval, 'Billy Bones', '16-SEP-17 08.53.09.968000 PM');
   INSERT INTO Author (AUTHOR_ID, AUTHOR_NAME, EXPIRED)
   VALUES (AUTHOR_ID_SEQ.nextval, 'Luke SkyWalker', '16-SEP-17 08.53.09.968000 PM');
   INSERT INTO Author (AUTHOR_ID, AUTHOR_NAME, EXPIRED)
   VALUES (AUTHOR_ID_SEQ.nextval, 'Bilbo Baggins', '16-SEP-17 08.53.09.968000 PM');
   
   INSERT INTO Tag (TAG_ID, TAG_NAME)
   VALUES (TAG_ID_SEQ.nextval, 'Career');
   INSERT INTO Tag (TAG_ID, TAG_NAME)
   VALUES (TAG_ID_SEQ.nextval, 'Education');
   INSERT INTO Tag (TAG_ID, TAG_NAME)
   VALUES (TAG_ID_SEQ.nextval, 'Economics');
   INSERT INTO Tag (TAG_ID, TAG_NAME)
   VALUES (TAG_ID_SEQ.nextval, 'Politics');
   INSERT INTO Tag (TAG_ID, TAG_NAME)
   VALUES (TAG_ID_SEQ.nextval, 'Travel');
   INSERT INTO Tag (TAG_ID, TAG_NAME)
   VALUES (TAG_ID_SEQ.nextval, 'Business');
   INSERT INTO Tag (TAG_ID, TAG_NAME)
   VALUES (TAG_ID_SEQ.nextval, 'Science');
   INSERT INTO Tag (TAG_ID, TAG_NAME)
   VALUES (TAG_ID_SEQ.nextval, 'Places');
   
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'Administrator', 'root', '81dc9bdb52d04dc20036dbd8313ed055');
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'Jhon Lock', 'lock', '81dc9bdb52d04dc20036dbd8313ed055');
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'Jack The Sparrow', 'jts', '81dc9bdb52d04dc20036dbd8313ed055');
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'Davy Jones', 'flying_dutchman', '81dc9bdb52d04dc20036dbd8313ed055');
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'Billy Bones', 'pirate', '81dc9bdb52d04dc20036dbd8313ed055');
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'Luke SkyWalker', 'force', '81dc9bdb52d04dc20036dbd8313ed055');
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'Bilbo Baggins', 'hobbit', '81dc9bdb52d04dc20036dbd8313ed055');
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'Nick Diaz', 'ufc', '81dc9bdb52d04dc20036dbd8313ed055');
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'Cheech Marin', 'upinsmoke', '81dc9bdb52d04dc20036dbd8313ed055');
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'Howlin Wolf', 'bluesman', '81dc9bdb52d04dc20036dbd8313ed055');
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'Willie Brown', 'crossroads', '81dc9bdb52d04dc20036dbd8313ed055');
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'R.L.Burnside', 'blackmattie', '81dc9bdb52d04dc20036dbd8313ed055');
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'Johnny Woods', 'donny', '81dc9bdb52d04dc20036dbd8313ed055');
   INSERT INTO TUser (USER_ID, USER_NAME, LOGIN, PASSWORD)
   VALUES (USER_ID_SEQ.nextval, 'Ragnar Lothbrok', 'viking', '81dc9bdb52d04dc20036dbd8313ed055');
   
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 2,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 2,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 2,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 3,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 5,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 5,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 6,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 8,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 8,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 8,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 8,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 9,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 11,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 11,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 12,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 14,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 16,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 17,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 17,
   '#Comment',
   CURRENT_TIMESTAMP);
   INSERT INTO Comments (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE)
   VALUES (COMMENT_ID_SEQ.nextval, 19,
   '#Comment',
   CURRENT_TIMESTAMP); 

/
    
   INSERT ALL
   
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (1, 1)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (2, 3)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (3, 2)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (4, 1)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (5, 5)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (6, 6)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (7, 6)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (8, 6)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (9, 1)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (10, 5)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (11, 4)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (12, 2)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (13, 2)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (14, 4)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (15, 3)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (16, 5)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (17, 1)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (18, 2)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (19, 6)
   INTO News_Author (NEWS_ID, AUTHOR_ID)
   VALUES (20, 2)
   
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (1, 1)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (1, 3)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (1, 8)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (2, 6)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (2, 7)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (3, 2)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (4, 3)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (4, 1)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (5, 5)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (6, 8)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (7, 2)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (7, 4)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (7, 6)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (8, 1)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (8, 2)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (9, 3)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (9, 4)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (9, 7)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (9, 8)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (10, 2)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (11, 1)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (11, 8)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (12, 3)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (13, 4)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (13, 5)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (13, 7)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (14, 2)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (14, 3)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (15, 8)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (16, 5)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (17, 2)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (17, 4)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (18, 4)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (19, 1)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (19, 3)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (19, 6)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (19, 7)
   INTO News_Tag (NEWS_ID, TAG_ID)
   VALUES (20, 1)
   
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (1, 
   'ADMIN')
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (2, 
   'AUTHOR')
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (3, 
   'AUTHOR')
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (4, 
   'AUTHOR')
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (5, 
   'AUTHOR')
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (6, 
   'AUTHOR')
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (7, 
   'AUTHOR')
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (8, 
   'AUDIENCE')
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (9, 
   'AUDIENCE')
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (10, 
   'AUDIENCE')
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (11, 
   'AUDIENCE')
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (12, 
   'AUDIENCE')
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (13, 
   'AUDIENCE')
   INTO Roles (USER_ID, ROLE_NAME)
   VALUES (14, 
   'AUDIENCE')
 
SELECT 1 FROM DUAL; ​