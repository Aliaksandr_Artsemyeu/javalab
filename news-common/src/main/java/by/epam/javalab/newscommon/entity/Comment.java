package by.epam.javalab.newscommon.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import by.epam.javalab.newscommon.entity.converter.JodaTimeToTimestampConverter;

/**
 * Instances of this class is the DTO objects, they serve to transfer values
 * from database to client and in an opposite direction. This class represent
 * Comment entity and corresponds to COMMENTS table records.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
@Entity
@Table(name = "COMMENTS")
public class Comment implements Serializable {

	private static final long serialVersionUID = 1L;

	/** This fields value corresponds to COMMENT_ID column value */
	@Id
	@Column(name = "COMMENT_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "commentSeqGen")
	@SequenceGenerator(name = "commentSeqGen", sequenceName = "COMMENT_ID_SEQ", allocationSize = 1)
	private Long commentId;

	/** This fields value corresponds to NEWS_ID column value */
	@NotNull
	@Column(name = "NEWS_ID")
	private Long newsId;

	/** This fields value corresponds to COMMENT_TEXT column value */
	@NotEmpty(message = "{comment.text.empty}")
	@Size(max = 100, message = "{comment.text.size}")
	@Column(name = "COMMENT_TEXT", columnDefinition = "nvarchar2 (100)")
	private String text;

	/** This fields value corresponds to CREATION_DATE column value */
	@DateTimeFormat(pattern = "MM-dd-yyyy")
	@Convert(converter = JodaTimeToTimestampConverter.class, disableConversion = false)
	@Column(name = "CREATION_DATE")
	private DateTime creationDate;

	public Comment() {
		creationDate = new DateTime();
	}

	public Comment(Long newsId, String text) {
		this();
		this.newsId = newsId;
		this.text = text;
	}

	public Comment(Long commentId, Long newsId, String text) {
		this();
		this.commentId = commentId;
		this.newsId = newsId;
		this.text = text;
	}

	public Comment(Long commentId, Long newsId, String text, DateTime creationDate) {
		this.commentId = commentId;
		this.newsId = newsId;
		this.text = text;
		this.creationDate = creationDate;
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public DateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(DateTime creationDate) {
		this.creationDate = creationDate;
	}
	
	public Timestamp getCreationDateTimestamp() {
		return new Timestamp(creationDate.getMillis());
	}

	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", newsId=" + newsId + ", text=" + text + ", creationDate="
				+ creationDate + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentId == null) {
			if (other.commentId != null)
				return false;
		} else if (!commentId.equals(other.commentId))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

}
