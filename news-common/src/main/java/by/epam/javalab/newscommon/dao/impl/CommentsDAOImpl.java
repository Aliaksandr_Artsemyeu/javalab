package by.epam.javalab.newscommon.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import by.epam.javalab.newscommon.dao.CommentsDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.dao.util.ConnectionManager;
import by.epam.javalab.newscommon.entity.Comment;

/**
 * Implementation of CommentsDAO interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class CommentsDAOImpl implements CommentsDAO {

	private static final String SQL_INSERT_COMMENT = "INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) values (COMMENT_ID_SEQ.nextval, ?, ?, ?)";
	private static final String SQL_SELECT_COMMENT = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE COMMENT_ID = ?";
	private static final String SQL_UPDATE_COMMENT = "UPDATE COMMENTS SET NEWS_ID = ?, COMMENT_TEXT = ?, CREATION_DATE = ? WHERE COMMENT_ID = ?";
	private static final String SQL_DELETE_COMMENT = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
	private static final String SQL_DELETE_NEWS_COMMENTS = "DELETE FROM COMMENTS WHERE NEWS_ID IN ( ${numOfParam} )";
	private static final String SQL_SELECT_NEWS_COMMENTS = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE COMMENTS.NEWS_ID = ? ORDER BY CREATION_DATE";

	/**
	 * Instance of ConnectionManager utility class. Used to obtain connections
	 * and to release resources.
	 */
	@Inject
	private ConnectionManager connectionManager;

	@Override
	public Long insert(Comment comment) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_INSERT_COMMENT, new String[] { "COMMENT_ID" });
			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setString(2, comment.getText());
			preparedStatement.setTimestamp(3, comment.getCreationDateTimestamp());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DaoException("Inserting comment failed, no ID obtained.");
			}
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new NoSuchRecordException("There is no News with such ID.");
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public Comment select(Long commentId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_COMMENT);
			preparedStatement.setLong(1, commentId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return buildComment(resultSet);
			} else {
				throw new NoSuchRecordException();
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public void update(Comment comment) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_COMMENT);
			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setString(2, comment.getText());
			preparedStatement.setTimestamp(3, comment.getCreationDateTimestamp());
			preparedStatement.setLong(4, comment.getCommentId());
			int affectedRows = preparedStatement.executeUpdate();
			if (affectedRows == 0) {
				throw new NoSuchRecordException("There is no Comment with such ID.");
			}
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new NoSuchRecordException("There is no News with such ID.");
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}
	}

	@Override
	public void delete(Long commentId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT);
			preparedStatement.setLong(1, commentId);
			int affectedRows = preparedStatement.executeUpdate();
			if (affectedRows == 0) {
				throw new NoSuchRecordException("Deleting comment failed, no rows affected.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}
	}

	@Override
	public void removeComments(Long... newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			String paramString = StringUtils.repeat("?", ", ", newsId.length);
			String query = StringUtils.replaceOnce(SQL_DELETE_NEWS_COMMENTS, "${numOfParam}", paramString);
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(query);
			int count = 0;
			for (Long id : newsId) {
				preparedStatement.setLong(++count, id);
			}
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}
	}

	@Override
	public List<Comment> selectComments(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			List<Comment> comments = new ArrayList<>();
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS_COMMENTS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				comments.add(buildComment(resultSet));
			}
			return comments;
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	/**
	 * Initialize a new Comment instance with the values obtained from
	 * ResultSet.
	 *
	 * @param resultSet
	 *            returned ResultSet of SQL query.
	 * @return initialized Comment instance.
	 * @throws SQLException
	 *             if the columnLabel is not valid; if a database access error
	 *             occurs or this method is called on a closed result set.
	 */
	private Comment buildComment(ResultSet resultSet) throws SQLException {
		Long commentId = resultSet.getLong("COMMENT_ID");
		Long newsId = resultSet.getLong("NEWS_ID");
		String text = resultSet.getString("COMMENT_TEXT");
		DateTime creationDate = new DateTime(resultSet.getTimestamp("CREATION_DATE").getTime());
		return new Comment(commentId, newsId, text, creationDate);
	}
}
