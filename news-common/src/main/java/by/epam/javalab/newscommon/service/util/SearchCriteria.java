package by.epam.javalab.newscommon.service.util;

import java.io.Serializable;
import java.util.List;

/**
 * This class is used to perform specified selection from NEWS table. Contains
 * values of type Long, that represent AUTHOR_ID and TAG_ID column values, in
 * NEWS_AUTHOR and NEWS_TAG tables respectively.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class SearchCriteria implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * This fields value corresponds to AUTHOR_ID column value in the
	 * NEWS_AUTHOR table
	 */
	private Long authorId;

	/**
	 * This field is a List. It contains values that corresponds to TAG_ID
	 * column value in the NEWS_TAG table
	 */
	private List<Long> tagIdList;

	/**
	 * Used by client-side validator, to ensure that the filter was applied.
	 */
	private boolean searchPerformed;

	public SearchCriteria() {
	}

	public SearchCriteria(Long authorID, List<Long> tagIdList) {
		this.authorId = authorID;
		this.tagIdList = tagIdList;
	}

	public boolean isSearchPerformed() {
		return searchPerformed;
	}

	public void setSearchPerformed(boolean searchPerformed) {
		this.searchPerformed = searchPerformed;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagIdList() {
		return tagIdList;
	}

	public void setTagIdList(List<Long> tagIdList) {
		this.tagIdList = tagIdList;
	}
	
	public void setAuthorAndTags(SearchCriteria searchCriteria) {
		this.authorId = searchCriteria.getAuthorId();
		this.tagIdList = searchCriteria.getTagIdList();
	}

	public void setAuthorAndTags(Long authorId, List<Long> tagIdList) {
		this.authorId = authorId;
		this.tagIdList = tagIdList;
	}

	public void resetAuthorAndTags() {
		this.authorId = null;
		this.tagIdList = null;
		searchPerformed = false;
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "SearchCriteria [authorId=" + authorId + ", tagIdList="
				+ (tagIdList != null ? tagIdList.subList(0, Math.min(tagIdList.size(), maxLen)) : null) + "]";
	}

}
