package by.epam.javalab.newscommon.service;

import java.util.List;

import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.service.exception.ServiceException;
import by.epam.javalab.newscommon.service.util.SearchCriteria;

/**
 * Service interface for News entity. Extends CommonService interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public interface NewsService extends CommonService<News> {

	/**
	 * Selects all records from the NEWS table, that have the NEWS_ID column
	 * value matching to any value from idList method parameter.
	 *
	 * @param idList
	 *            List of values, that corresponds to the NEWS_ID column values.
	 * @return List of News instances.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	List<News> selectNews(List<Long> idList) throws ServiceException;

	/**
	 * Selects NEWS_ID column values from the NEWS table, using SQL query,
	 * constructed from the SearchCriteria method parameter values.
	 *
	 * @param searchCriteria
	 *            SearchCriteria instance, contains author ID and List of tag
	 *            IDs .
	 * @return List of values of type Long.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	List<Long> selectIdList(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Checks passed parameters and calls to the appropriate Dao method.
	 *
	 * @param newsId
	 *            Id values of the records, that need to be deleted.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	void deleteNews(Long... newsId) throws ServiceException;
}
