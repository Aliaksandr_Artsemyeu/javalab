package by.epam.javalab.newscommon.dao.impl.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleStateException;

import by.epam.javalab.newscommon.dao.TagDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.entity.Tag;

public class TagDAOHibernateImpl implements TagDAO {
	
	@Inject
	private SessionFactory sessionFactory;
	
	private final static String HQL_SELECT_ALL_ROWS = "select t from Tag t";
	private final static String HQL_SELECT_NEWS_LIST = "select n from News n where id in (:idList)";
	private final static String HQL_SELECT_TAGS_LIST = "select t from Tag t where id in (:idList)";
	private final static String HQL_SELECT_NEWS_TAGS = "select n from News n join n.tags nt where nt.id = :tagId";
	
	@Override
	public Long insert(Tag t) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.save(t);
			session.flush();
			return t.getId();
		} catch (HibernateException | ConstraintViolationException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public Tag select(Long id) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Tag tag = session.get(Tag.class, id);
			if (tag == null) {
				throw new NoSuchRecordException();
			}
			return tag;
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public void update(Tag t) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.update(t);
			session.flush();
		} catch (StaleStateException | ConstraintViolationException e) {
			throw new NoSuchRecordException(e);
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void delete(Long id) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Tag tag = session.get(Tag.class, id);
			Query query = session.createQuery(HQL_SELECT_NEWS_TAGS);
			query.setParameter("tagId", id);
			List<News> newsList = query.list();
			for (News n : newsList) {
				n.getTags().remove(tag);
			}
			session.delete(tag);
			session.flush();
		} catch (IllegalArgumentException e) {
			throw new NoSuchRecordException(e);
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> selectEntries() throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			List<Tag> tagList = session.createQuery(HQL_SELECT_ALL_ROWS).list();
			return tagList;
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public List<Tag> selectTags(Long newsId) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			News news = session.get(News.class, newsId);
			if (news == null || news.getTags() == null) {
				return new ArrayList<Tag>();
			}
			return news.getTags();
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void bindTagsToNews(Long newsId, List<Long> tagIdList) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			News news = session.get(News.class, newsId);
			Query query = session.createQuery(HQL_SELECT_TAGS_LIST);
			query.setParameterList("idList", tagIdList);
			List<Tag> tagList = query.list();
			if (news == null || tagList == null || tagList.size() == 0) {
				throw new DaoException();
			}
			news.getTags().addAll(tagList);
			session.flush();
		} catch (HibernateException | ConstraintViolationException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void unbindTagsFromNews(Long... newsId) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery(HQL_SELECT_NEWS_LIST);
			query.setParameterList("idList", newsId);
			List<News> newsList = query.list();
			for (News n : newsList) {
				n.setTags(null);;
			}
			session.flush();
		} catch (HibernateException | ConstraintViolationException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

}
