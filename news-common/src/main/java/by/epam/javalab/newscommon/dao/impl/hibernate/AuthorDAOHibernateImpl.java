package by.epam.javalab.newscommon.dao.impl.hibernate;

import java.util.List;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleStateException;

import by.epam.javalab.newscommon.dao.AuthorDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Author;
import by.epam.javalab.newscommon.entity.News;

public class AuthorDAOHibernateImpl implements AuthorDAO {

	@Inject
	private SessionFactory sessionFactory;
	
	private final static String HQL_SELECT_ALL_ROWS = "select a from Author a";
	private final static String HQL_SELECT_ALL_UNEXPIRED = "select a from Author a where expired > current_timestamp";
	private final static String HQL_SELECT_NEWS_LIST = "select n from News n where id in (:idList)";

	@Override
	public Long insert(Author t) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.save(t);
			session.flush();
			return t.getId();
		} catch (HibernateException | ConstraintViolationException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public Author select(Long id) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Author author = session.get(Author.class, id);
			if (author == null) {
				throw new NoSuchRecordException();
			}
			return author;
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public void update(Author t) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.update(t);
			session.flush();
		} catch (StaleStateException | ConstraintViolationException e) {
			throw new NoSuchRecordException(e);
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public void delete(Long id) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Author author = session.get(Author.class, id);
			session.delete(author);
			session.flush();
		} catch (IllegalArgumentException e) {
			throw new NoSuchRecordException(e);
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> selectEntries() throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			List<Author> authorList = session.createQuery(HQL_SELECT_ALL_ROWS).list();
			return authorList;
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Author> selectUnexpiredEntries() throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			List<Author> authorList = session.createQuery(HQL_SELECT_ALL_UNEXPIRED).list();
			return authorList;
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public Author selectAuthor(Long newsId) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			News news = session.get(News.class, newsId);
			if (news == null || news.getAuthor() == null) {
				throw new NoSuchRecordException();
			}
			return news.getAuthor();
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public void bindAuthorToNews(Long newsId, Long authorId) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			News news = session.get(News.class, newsId);
			Author author = session.get(Author.class, authorId);
			if (news == null || author == null) {
				throw new NoSuchRecordException();
			}
			news.setAuthor(author);
			session.flush();
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void unbindAuthorFromNews(Long... newsId) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery(HQL_SELECT_NEWS_LIST);
			query.setParameterList("idList", newsId);
			List<News> newsList = query.list();
			for (News n : newsList) {
				n.setAuthor(null);
			}
			session.flush();
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

}
