package by.epam.javalab.newscommon.service;

import java.util.List;

import by.epam.javalab.newscommon.entity.Author;
import by.epam.javalab.newscommon.service.exception.ServiceException;

/**
 * Service interface for Author entity. Extends CommonService interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public interface AuthorService extends CommonService<Author> {

	/**
	 * Selects all rows from the AUTHOR table.
	 *
	 * @return List of instances of type Author.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	List<Author> selectEntries() throws ServiceException;

	/**
	 * Selects all rows from the AUTHOR table that is not expired yet.
	 *
	 * @return List of instances of type Author.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	List<Author> selectUnexpiredEntries() throws ServiceException;

	/**
	 * Selects one row from AUTHOR table with the AUTHOR_ID, selected from the
	 * NEWS_AUTHOR table, which corresponds to the NEWS_ID, which matches newsID
	 * method parameter. Builds Author entity based one this record values.
	 *
	 * @param newsId
	 *            ID of the record in NEWS_AUTHOR table.
	 * @return instance of Author, initialized with values form selected record,
	 *         or null if if there is no such record.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	Author selectAuthor(Long newsId) throws ServiceException;

	/**
	 * Inserts a new record to the NEWS_AUTHOR table.
	 *
	 * @param newsId
	 *            ID of record from NEWS table.
	 * @param authorId
	 *            ID of the record from AUTHOR table.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution, or insert
	 *             operation failed.
	 */
	void bindAuthorToNews(Long newsId, Long authorId) throws ServiceException;

	/**
	 * Deletes a record from the NEWS_AUTHOR table, with the NEWS_ID column
	 * value matching newsID method parameter.
	 *
	 * @param newsId
	 *            ID of record from NEWS table.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution, or delete
	 *             operation failed.
	 */
	void unbindAuthorFromNews(Long... newsId) throws ServiceException;
}
