package by.epam.javalab.newscommon.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import by.epam.javalab.newscommon.entity.converter.JodaTimeToDateConverter;
import by.epam.javalab.newscommon.entity.converter.JodaTimeToTimestampConverter;

/**
 * Instances of this class is the DTO objects, they serve to transfer values
 * from database to client and in an opposite direction. This class represents
 * News entity and corresponds to NEWS table records.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
@Entity
@Table(name = "NEWS")
public class News implements Serializable {

	private static final long serialVersionUID = 1L;

	/** This fields value corresponds to NEWS_ID column value */
	@Id
	@Column(name = "NEWS_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "newsSeqGen")
	@SequenceGenerator(name = "newsSeqGen", sequenceName = "NEWS_ID_SEQ", allocationSize = 1)
	private Long id;

	/** This fields value corresponds to TITLE column value */
	@NotEmpty(message = "{news.title.empty}")
	@Size(max = 30, message = "{news.title.size}")
	@Column(name = "TITLE", columnDefinition = "nvarchar2 (30)")
	private String title;

	/** This fields value corresponds to SHORT_TEXT column value */
	@NotEmpty(message = "{news.shortText.empty}")
	@Size(max = 100, message = "{news.shortText.size}")
	@Column(name = "SHORT_TEXT", columnDefinition = "nvarchar2 (100)")
	private String shortText;

	/** This fields value corresponds to FULL_TEXT column value */
	@NotEmpty(message = "{news.fullText.empty}")
	@Size(max = 2000, message = "{news.fullText.size}")
	@Column(name = "FULL_TEXT", columnDefinition = "nvarchar2 (2000)")
	private String fullText;

	/** This fields value corresponds to CREATION_DATE column value */
	@DateTimeFormat(pattern = "MM-dd-yyyy")
	@Convert(converter = JodaTimeToTimestampConverter.class, disableConversion = false)
	@Column(name = "CREATION_DATE")
	private DateTime creationDate;

	/** This fields value corresponds to MODIFICATION_DATE column value */
	@DateTimeFormat(pattern = "MM-dd-yyyy")
	@Convert(converter = JodaTimeToDateConverter.class, disableConversion = false)
	@Column(name = "MODIFICATION_DATE")
	private DateTime modificationDate;
	
	/** This fields value corresponds to one record from the AUTHOR table */
	@NotNull(message = "{author.id.null}")
	@OneToOne(fetch = FetchType.EAGER)
	@JoinTable(name = "NEWS_AUTHOR", joinColumns = @JoinColumn(name = "NEWS_ID"), inverseJoinColumns = @JoinColumn(name = "AUTHOR_ID"))
	private Author author;
	
	/** This fields values corresponds to a few records from the TAG table */
	@OneToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "NEWS_TAG", joinColumns = @JoinColumn(name = "NEWS_ID"), inverseJoinColumns = @JoinColumn(name = "TAG_ID"))
	private List<Tag> tags;
	
	/**
	 * This fields values corresponds to a few records from the COMMENTS table
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE}, orphanRemoval = true)
	@JoinColumn(name = "NEWS_ID")
	private List<Comment> comments;

	public News() {
		this.creationDate = new DateTime();
		this.modificationDate = new DateTime();
		this.author = new Author();
		this.tags = new ArrayList<>();
		this.comments = new ArrayList<>();
	}

	public News(Long id, String title, String shortText, String fullText) {
		this();
		this.id = id;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
	}

	public News(String title, String shortText, String fullText) {
		this();
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
	}

	public News(Long id, String title, String shortText, String fullText, DateTime creationDate,
			DateTime modificationDate) {
		this.id = id;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.author = new Author();
		this.tags = new ArrayList<>();
		this.comments = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public DateTime getCreationDate() {
		return creationDate;
	}
	
	public Timestamp getCreationDateTimestamp() {
		return new Timestamp(creationDate.getMillis());
	}

	public void setCreationDate(DateTime creationDate) {
		this.creationDate = creationDate;
	}

	public DateTime getModificationDate() {
		return modificationDate;
	}
	
	public Date getModificationDateSql() {
		return new Date(modificationDate.getMillis());
	}

	public void setModificationDate(DateTime modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", title=" + title + ", shortText=" + shortText + ", fullText=" + fullText
				+ ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + ", author=" + author
				+ ", tags=" + tags + ", comments=" + comments + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
