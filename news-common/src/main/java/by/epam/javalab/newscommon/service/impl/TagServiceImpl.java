package by.epam.javalab.newscommon.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import by.epam.javalab.newscommon.dao.TagDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Tag;
import by.epam.javalab.newscommon.service.TagService;
import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;

/**
 * Implementation of TagService interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class TagServiceImpl implements TagService {

	private static final Logger logger = Logger.getLogger(TagService.class);

	@Inject
	private TagDAO tagDAO;

	@Override
	public Long insert(Tag tag) throws ServiceException {
		if (tag == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			Long tagId = tagDAO.insert(tag);
			return tagId;
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public Tag select(Long tagId) throws ServiceException {
		if (tagId == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			Tag tag = tagDAO.select(tagId);
			return tag;
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(Tag tag) throws ServiceException {
		if (tag == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			tagDAO.update(tag);
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long tagId) throws ServiceException {
		if (tagId == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			tagDAO.delete(tagId);
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Tag> selectEntries() throws ServiceException {
		try {
			List<Tag> tagList = tagDAO.selectEntries();
			return tagList;
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Tag> selectTags(Long newsId) throws ServiceException {
		if (newsId == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			List<Tag> tags = tagDAO.selectTags(newsId);
			return tags;
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void bindTagsToNews(Long newsId, List<Long> tagList) throws ServiceException {
		if (newsId == null || tagList == null) {
			throw new ServiceException("Passed parameter is null!");
		} else if (tagList.size() != 0) {
			for (Long id : tagList) {
				if (id == null) {
					throw new ServiceException("Passed parameter is null!");
				}
			}
			try {
				tagDAO.bindTagsToNews(newsId, tagList);
			} catch (DaoException e) {
				logger.error(e.getMessage(), e);
				throw new ServiceException(e);
			}
		}
	}

	@Override
	public void unbindTagsFromNews(Long... newsId) throws ServiceException {
		if (newsId == null) {
			throw new ServiceException("Passed parameter is null!");
		} else if (newsId.length != 0) {
			for (Long id : newsId) {
				if (id == null) {
					throw new ServiceException("Passed parameter is null!");
				}
			}
			try {
				tagDAO.unbindTagsFromNews(newsId);
			} catch (DaoException e) {
				logger.error(e.getMessage(), e);
				throw new ServiceException(e);
			}
		}
	}

}
