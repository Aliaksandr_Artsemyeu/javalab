package by.epam.javalab.newscommon.dao;

import java.util.List;

import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.service.util.SearchCriteria;

/**
 * DAO interface for News entity. Extends CommonDAO interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public interface NewsDAO extends CommonDAO<News> {

	/**
	 * Selects records from the NEWS table, using List of Id values.
	 *
	 * @param idList
	 *            List that contains Id of rows to be selected, in specified
	 *            order.
	 * @return List of News instances, created from ResultSet.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	List<News> selectNews(List<Long> idList) throws DaoException;

	/**
	 * Selects Id of records from the NEWS table, using SQL query, constructed
	 * from the SearchCriteria method parameter values.
	 *
	 * @param searchCriteria
	 *            SearchCriteria instance, contains author ID and List of tag
	 *            IDs .
	 * @return List of Id values.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	List<Long> selectIdList(SearchCriteria searchCriteria) throws DaoException;

	/**
	 * Used to delete any number of records from the NEWS table, with the record
	 * Id matching to passed parameters.
	 *
	 * @param newsId
	 *            Id values of the records, that need to be deleted.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	void deleteNews(Long... newsId) throws DaoException;
}
