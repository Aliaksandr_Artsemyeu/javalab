package by.epam.javalab.newscommon.dao.exception;

/**
 * Custom exception. Thrown when error occurs during DAO method execution.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class DaoException extends Exception {

	private static final long serialVersionUID = 1L;

	public DaoException() {
	}

	public DaoException(String message) {
		super(message);
	}

	public DaoException(Throwable cause) {
		super(cause);
	}

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}
}
