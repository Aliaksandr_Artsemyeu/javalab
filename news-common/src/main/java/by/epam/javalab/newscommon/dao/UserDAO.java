package by.epam.javalab.newscommon.dao;

import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.entity.User;

/**
 * DAO interface for User entity. Extends CommonDAO interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public interface UserDAO extends CommonDAO<User> {

	/**
	 * Selects a record from the TUSER table, where LOGIN column value equals to
	 * passed method parameter.
	 *
	 * @param login
	 *            users login.
	 * @return instance of User.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	User find(String login) throws DaoException;
}
