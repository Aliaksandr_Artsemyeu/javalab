package by.epam.javalab.newscommon.service.impl;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import by.epam.javalab.newscommon.dao.UserDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.User;
import by.epam.javalab.newscommon.service.UserService;
import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;

/**
 * Implementation of UserService interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class UserServiceImpl implements UserService, UserDetailsService {

	private static final Logger logger = Logger.getLogger(UserService.class);

	@Inject
	private UserDAO userDAO;

	@Override
	public Long insert(User user) throws ServiceException {
		if (user == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			Long userId = userDAO.insert(user);
			return userId;
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public User select(Long userId) throws ServiceException {
		if (userId == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			User user = userDAO.select(userId);
			return user;
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(User user) throws ServiceException {
		if (user == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			userDAO.update(user);
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long userId) throws ServiceException {
		if (userId == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			userDAO.delete(userId);
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Implemented method of UserDetailsService interface, used by Spring
	 * Security to look for the existing user with specified login.
	 *
	 * @param login
	 *            corresponds to the LOGIN column value in the TUSER table.
	 * @return instance of UserDetails, used by Spring Security to authorize
	 *         user.
	 * @throws UsernameNotFoundException
	 *             if there is no user with such login.
	 */
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		if (login == null) {
			throw new UsernameNotFoundException("Passed parameter is null!");
		}
		try {
			User user = userDAO.find(login);
			return user;
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new UsernameNotFoundException("User " + login + " not found.");
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new UsernameNotFoundException("Database error!");
		}
	}

}
