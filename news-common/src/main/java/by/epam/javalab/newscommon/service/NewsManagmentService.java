package by.epam.javalab.newscommon.service;

import java.util.List;

import by.epam.javalab.newscommon.entity.Author;
import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.entity.Tag;
import by.epam.javalab.newscommon.service.exception.ServiceException;

/**
 * Combined service interface. Include NewsService, AuthorService, TagService,
 * CommentsService interfaces. Used to perform actions that use more then one
 * service interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public interface NewsManagmentService {

	/**
	 * Deletes all rows from the NEWS, NEWS_TAG, NEWS_AUTHOR, COMMENTS tables
	 * with the Id(Primary key), matching to any of newsId method parameters.
	 *
	 * @param newsId
	 *            Id of the record to be deleted.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	void delete(Long... newsId) throws ServiceException;

	/**
	 * Inserts new records to the NEWS, NEWS_AUTHOR and NEWS_TAG tables.
	 *
	 * @param news
	 *            instance of the News entity, used to create new record with
	 *            its value.
	 * @param author
	 *            instance of the Author entity, used to create new record with
	 *            its value.
	 * @param tags
	 *            instance of the Tag entity, used to create new records with
	 *            its values.
	 * @return Id of the record inserted to the NEWS table.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	Long insert(News news, Author author, List<Tag> tags) throws ServiceException;

	/**
	 * Updates existing records in the NEWS, NEWS_AUTHOR, NEWS_TAG tables.
	 *
	 * @param news
	 *            instance of the News entity, used to create new record with
	 *            its value.
	 * @param author
	 *            instance of the Author entity, used to create new record with
	 *            its value.
	 * @param tags
	 *            instance of the Tag entity, used to create new records with
	 *            its values.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	void update(News news, Author author, List<Tag> tags) throws ServiceException;

	/**
	 * Instantiates a new DTONews object and initialize it with the values
	 * obtained from the NEWS, TAG, AUTHOR, COMMENTS tables, where NEWS_ID
	 * column value in this tables match to the newsId method parameter.
	 *
	 * @param newsId
	 *            Id of the records in the NEWS, TAG, AUTHOR, COMMENTS tables.
	 * @return instance of DTONews, initialized with values form selected
	 *         records.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	News select(Long newsId) throws ServiceException;

	/**
	 * Selects records from the NEWS, TAG, AUTHOR, COMMENTS tables, where
	 * NEWS_ID column values match to any value in sublist of idList List passed
	 * to method as a parameter. Sublist of idList is created using int page
	 * passed parameter, that specifies witch part of idList will be sublisted.
	 *
	 * @param subList
	 *            List that contains Id values of News records, selected with
	 *            the use of SearchCriteria.
	 * @return List of DTONews instances.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	List<News> selectList(List<Long> subList) throws ServiceException;

}
