package by.epam.javalab.newscommon.entity.converter;

import java.sql.Date;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.joda.time.DateTime;

@Converter
public class JodaTimeToDateConverter implements AttributeConverter<DateTime, Date>{

	@Override
	public Date convertToDatabaseColumn(DateTime dateTime) {
		return new Date(dateTime.getMillis());
	}

	@Override
	public DateTime convertToEntityAttribute(Date date) {
		return new DateTime(date.getTime());
	}

}
