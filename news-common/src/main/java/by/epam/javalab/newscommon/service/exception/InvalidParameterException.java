package by.epam.javalab.newscommon.service.exception;

public class InvalidParameterException extends ServiceException {

	private static final long serialVersionUID = 1L;

	public InvalidParameterException() {
	}

	public InvalidParameterException(String message) {
		super(message);
	}

	public InvalidParameterException(Throwable cause) {
		super(cause);
	}

	public InvalidParameterException(String message, Throwable cause) {
		super(message, cause);
	}

}
