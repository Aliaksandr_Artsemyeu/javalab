package by.epam.javalab.newscommon.dao.impl.jpa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.validation.ConstraintViolationException;

import org.springframework.transaction.annotation.Transactional;

import by.epam.javalab.newscommon.dao.NewsDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Comment;
import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.service.util.SearchCriteria;

@Transactional(rollbackFor = {Exception.class})
public class NewsDAOJpaImpl implements NewsDAO {
	
	private static final String JPQL_DELETE_COMMENTS = "delete from Comment c where c.newsId = :newsId";
	private static final String JPQL_SELECT_NEWS_LIST = "select n from News n where n.id in :idList";
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long insert(News t) throws DaoException {
		try {
			entityManager.persist(t);
			return t.getId();
		} catch (PersistenceException | ConstraintViolationException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public News select(Long id) throws DaoException {
		try {
			News news = entityManager.find(News.class, id);
			if (news == null) {
				throw new NoSuchRecordException();
			}
			return news;
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void update(News t) throws DaoException {
		try {
			News news = entityManager.find(News.class, t.getId());
			if (news == null) {
				throw new NoSuchRecordException();
			}
			entityManager.merge(t);
		} catch (ConstraintViolationException e) {
			throw new NoSuchRecordException(e);
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void delete(Long id) throws DaoException {
		try {
			News news = entityManager.find(News.class, id);
			if (news == null) {
				throw new NoSuchRecordException();
			}
			entityManager.createQuery(JPQL_DELETE_COMMENTS).setParameter("newsId", id).executeUpdate();
			entityManager.remove(news);
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public List<News> selectNews(List<Long> idList) throws DaoException {
		try {
			List<News> newsList = new ArrayList<>();
			entityManager.createQuery(JPQL_SELECT_NEWS_LIST, News.class).setParameter("idList", idList).getResultList();
			for (Long id : idList) {
				News news = entityManager.find(News.class, id);
				if (news != null) {
					newsList.add(news);
				}
			}
			return newsList;
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public List<Long> selectIdList(SearchCriteria searchCriteria) throws DaoException {
		try {
			List<Long> idList = new ArrayList<>();
			CriteriaQuery<Tuple> criteria = constructCriteria(searchCriteria);
			List<Tuple> news = entityManager.createQuery(criteria).getResultList();
			for (Tuple t : news) {
				idList.add(Long.parseLong(t.get(0).toString()));
			}
			return idList;
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void deleteNews(Long... newsId) throws DaoException {
		try {
			List<News> newsList = entityManager.createQuery(JPQL_SELECT_NEWS_LIST, News.class)
					.setParameter("idList", Arrays.asList(newsId)).getResultList();
			for (News n : newsList) {
				entityManager.createQuery(JPQL_DELETE_COMMENTS).setParameter("newsId", n.getId()).executeUpdate();
				entityManager.remove(n);
			}
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}
	

	@SuppressWarnings("unchecked")
	private CriteriaQuery<Tuple> constructCriteria(SearchCriteria searchCriteria) {
		Long authorId = searchCriteria.getAuthorId();
		List<Long> tagsId = searchCriteria.getTagIdList();
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Tuple> criteria = criteriaBuilder.createTupleQuery();
		Root<News> rootNews = criteria.from(News.class);
		Join<News, Comment> commentsTable = rootNews.join("comments", JoinType.LEFT);
		List<Predicate> predicates = new ArrayList<>();
		if (authorId != null) {
			predicates.add(criteriaBuilder.equal(rootNews.get("author").get("id"), authorId));
		}
		if (tagsId != null && tagsId.size() != 0) {
			predicates.add(rootNews.get("tags").get("id").in(tagsId));
		}
		Expression<Long> numberOfComments = criteriaBuilder.count(commentsTable);
		Selection<Tuple>[] selections = new Selection[]{rootNews.get("id")};
		if (predicates.size() > 0) {
			criteria.multiselect(selections).where(criteriaBuilder.or(predicates.toArray(new Predicate[]{})));
		} else {
			criteria.multiselect(selections);
		}
		criteria.groupBy(Arrays.asList(rootNews.get("id"), 
				rootNews.get("title"), 
				rootNews.get("shortText"), 
				rootNews.get("fullText"),
				rootNews.get("creationDate"), 
				rootNews.get("modificationDate")));
		criteria.orderBy(Arrays.asList(criteriaBuilder.desc(numberOfComments),
				criteriaBuilder.desc(rootNews.get("modificationDate"))));
		return criteria;
	}

}
