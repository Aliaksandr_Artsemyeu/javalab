package by.epam.javalab.newscommon.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Inject;

import by.epam.javalab.newscommon.dao.UserDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.dao.util.ConnectionManager;
import by.epam.javalab.newscommon.entity.User;

/**
 * Implementation of UserDAO interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class UserDAOImpl implements UserDAO {

	private static final String SQL_INSERT_USER = "INSERT INTO TUSER (USER_ID, USER_NAME, LOGIN, PASSWORD) values (USER_ID_SEQ.nextval, ?, ?, ?)";
	private static final String SQL_INSERT_USER_ROLE = "INSERT INTO ROLES (USER_ID, ROLE_NAME) VALUES (USER_ID_SEQ.currval, ?)";
	private static final String SQL_SELECT_USER = "SELECT TUSER.USER_ID, TUSER.USER_NAME, TUSER.LOGIN, TUSER.PASSWORD, ROLES.ROLE_NAME FROM TUSER JOIN ROLES ON TUSER.USER_ID = ROLES.USER_ID WHERE TUSER.USER_ID = ?";
	private static final String SQL_UPDATE_USER = "UPDATE TUSER SET USER_NAME = ?, LOGIN = ?, PASSWORD = ? WHERE USER_ID = ?";
	private static final String SQL_DELETE_USER = "DELETE FROM TUSER WHERE USER_ID = ?";
	private static final String SQL_DELETE_USER_ROLE = "DELETE FROM ROLES WHERE USER_ID = ?";
	private static final String SQL_FIND_USER = "SELECT TUSER.USER_ID, TUSER.USER_NAME, TUSER.LOGIN, TUSER.PASSWORD, ROLES.ROLE_NAME FROM TUSER JOIN ROLES ON TUSER.USER_ID = ROLES.USER_ID WHERE TUSER.LOGIN = ?";

	/**
	 * Instance of ConnectionManager utility class. Used to obtain connections
	 * and to release resources.
	 */
	@Inject
	private ConnectionManager connectionManager;

	@Override
	public Long insert(User user) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		PreparedStatement rolePreparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_INSERT_USER, new String[] { "USER_ID" });
			preparedStatement.setString(1, user.getUserName());
			preparedStatement.setString(2, user.getLogin());
			preparedStatement.setString(3, user.getPassword());
			preparedStatement.executeUpdate();
			rolePreparedStatement = connection.prepareStatement(SQL_INSERT_USER_ROLE);
			rolePreparedStatement.setString(1, user.getRole());
			rolePreparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DaoException("Inserting user failed, no ID obtained.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement, rolePreparedStatement);
		}
	}

	@Override
	public User select(Long userId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_USER);
			preparedStatement.setLong(1, userId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return buildUser(resultSet);
			} else {
				throw new NoSuchRecordException();
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public void update(User user) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_USER);
			preparedStatement.setString(1, user.getUserName());
			preparedStatement.setString(2, user.getLogin());
			preparedStatement.setString(3, user.getPassword());
			preparedStatement.setLong(4, user.getId());
			int affectedRows = preparedStatement.executeUpdate();
			if (affectedRows == 0) {
				throw new NoSuchRecordException("Updating user failed, no rows affected.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}
	}

	@Override
	public void delete(Long userId) throws DaoException {
		Connection connection = null;
		PreparedStatement deleteRolePreparedStatement = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = connectionManager.getConnection();
			deleteRolePreparedStatement = connection.prepareStatement(SQL_DELETE_USER_ROLE);
			deleteRolePreparedStatement.setLong(1, userId);
			deleteRolePreparedStatement.executeUpdate();
			preparedStatement = connection.prepareStatement(SQL_DELETE_USER);
			preparedStatement.setLong(1, userId);
			int affectedRows = preparedStatement.executeUpdate();
			if (affectedRows == 0) {
				throw new NoSuchRecordException("Deleting user failed, no rows affected.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement, deleteRolePreparedStatement);
		}
	}

	@Override
	public User find(String login) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_FIND_USER);
			preparedStatement.setString(1, login);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return buildUser(resultSet);
			} else {
				throw new NoSuchRecordException();
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	/**
	 * Initialize a new User instance with the values obtained from ResultSet.
	 *
	 * @param resultSet
	 *            returned ResultSet of SQL query.
	 * @return initialized User instance.
	 * @throws SQLException
	 *             if the columnLabel is not valid; if a database access error
	 *             occurs or this method is called on a closed result set.
	 */
	private User buildUser(ResultSet resultSet) throws SQLException {
		Long userID = resultSet.getLong("USER_ID");
		String userName = resultSet.getString("USER_NAME");
		String login = resultSet.getString("LOGIN");
		String password = resultSet.getString("PASSWORD");
		String role = resultSet.getString("ROLE_NAME");
		return new User(userID, userName, login, password, role);
	}

}
