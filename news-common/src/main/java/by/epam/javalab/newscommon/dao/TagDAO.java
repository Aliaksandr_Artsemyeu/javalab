package by.epam.javalab.newscommon.dao;

import java.util.List;

import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.entity.Tag;

/**
 * DAO interface for Tag entity. Extends CommonDAO interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public interface TagDAO extends CommonDAO<Tag> {

	/**
	 * Selects all existing records from the table TAG. Creates instance of Tag
	 * for each returned row and initialize this instance with the row values.
	 * Adds all created Tag instances to the List.
	 *
	 * @return List that contains instances of type Tag, represents all existing
	 *         records in the TAG table.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	List<Tag> selectEntries() throws DaoException;

	/**
	 * Selects all records from the TAG table, with the TAG_ID column value,
	 * matching TAG_ID column value in the NEWS_TAG table, in the records with
	 * the NEWS_ID column value matching newsID method parameter.
	 *
	 * @param newsId
	 *            ID(Primary Key) of record from NEWS table.
	 * @return List of Tag instances, created from ResultSet.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	List<Tag> selectTags(Long newsId) throws DaoException;

	/**
	 * Inserts a new record to the NEWS_TAG table for each value from the tagIDs
	 * List in the method parameters.
	 *
	 * @param newsId
	 *            ID(Primary Key) of record from NEWS table.
	 * @param tagIdList
	 *            List of IDs(Primary Keys) for the records from TAG table.
	 * @throws DaoException
	 *             if SQLException occurred during method execution, or insert
	 *             operation failed.
	 */
	void bindTagsToNews(Long newsId, List<Long> tagIdList) throws DaoException;

	/**
	 * Delete all records from the NEWS_TAG table, with the NEWS_ID column value
	 * matching any of newsId method parameters.
	 *
	 * @param newsId
	 *            ID(Primary Key) of record from NEWS table.
	 * @throws DaoException
	 *             if SQLException occurred during method execution, or delete
	 *             operation failed.
	 */
	void unbindTagsFromNews(Long... newsId) throws DaoException;
}
