package by.epam.javalab.newscommon.dao.impl.jpa;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;

import org.springframework.transaction.annotation.Transactional;

import by.epam.javalab.newscommon.dao.CommentsDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Comment;
import by.epam.javalab.newscommon.entity.News;

@Transactional(rollbackFor = {Exception.class})
public class CommentsDAOJpaImpl implements CommentsDAO {
	
	private final static String JPQL_SELECT_NEWS = "select n from News n where n.id in :idList";
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long insert(Comment t) throws DaoException {
		try {
			entityManager.persist(t);
			return t.getCommentId();
		} catch (PersistenceException | ConstraintViolationException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public Comment select(Long id) throws DaoException {
		try {
			Comment comment = entityManager.find(Comment.class, id);
			if (comment == null) {
				throw new NoSuchRecordException();
			}
			return comment;
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void update(Comment t) throws DaoException {
		try {
			Comment comment = entityManager.find(Comment.class, t.getCommentId());
			if (comment == null) {
				throw new NoSuchRecordException();
			}
			entityManager.merge(t);
		} catch (ConstraintViolationException e) {
			throw new NoSuchRecordException(e);
		} catch (PersistenceException e) {
			throw new DaoException(e);
		} 
	}

	@Override
	public void delete(Long id) throws DaoException {
		try {
			Comment comment = entityManager.find(Comment.class, id);
			if (comment == null) {
				throw new NoSuchRecordException();
			}
			entityManager.remove(comment);
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void removeComments(Long... newsId) throws DaoException {
		try {
			List<News> newsList = entityManager.createQuery(JPQL_SELECT_NEWS, News.class).setParameter("idList", Arrays.asList(newsId)).getResultList();
			for (News n : newsList) {
				entityManager.refresh(n);
				n.setComments(null);
			}
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public List<Comment> selectComments(Long newsId) throws DaoException {
		try {
			News news = entityManager.find(News.class, newsId);
			if (news == null) {
				throw new NoSuchRecordException();
			}
			entityManager.refresh(news);
			List<Comment> commentList = news.getComments();
			return commentList;
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

}
