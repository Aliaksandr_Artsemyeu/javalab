package by.epam.javalab.newscommon.entity.converter;

import java.sql.Timestamp;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.joda.time.DateTime;

@Converter
public class JodaTimeToTimestampConverter implements AttributeConverter<DateTime, Timestamp> {

	@Override
	public Timestamp convertToDatabaseColumn(DateTime dateTime) {
		return new Timestamp(dateTime.getMillis());
	}

	@Override
	public DateTime convertToEntityAttribute(Timestamp timestamp) {
		return new DateTime(timestamp.getTime());
	}

}
