package by.epam.javalab.newscommon.service.util;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;

import by.epam.javalab.newscommon.service.exception.InvalidParameterException;

/**
 * This class contains List of ID of news records. And provides methods to work
 * with this List.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class NewsListWrapper implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Page count starts from 0.
	 */
	private static final int DEFAULT_START_PAGE = 0;

	/**
	 * Specifies number of news records that will be shown on the page at one
	 * time.
	 */
	@Value("${page.news.amount}")
	private int numberOfNewsOnPage;

	/**
	 * Contains id of all news by default, or only those that remains after
	 * filtering.
	 */
	private List<Long> idList;

	/**
	 * Id of news record, that is currently viewed.
	 */
	private int curIndex;

	/**
	 * This method is used to set current index, which is id of news record. If
	 * idList do not contain such id, then exception will be thrown.
	 * 
	 * @param newsId
	 *            id of news record.
	 * @throws InvalidParameterException
	 *             if idList do not contain passed id.
	 */
	public void setIndex(Long newsId) throws InvalidParameterException {
		if (idList != null && !idList.contains(newsId)) {
			throw new InvalidParameterException();
		}
		curIndex = idList.indexOf(newsId);
	}

	/**
	 * This method is used to get subList of the idList, which calculating
	 * depends on passed parameter.
	 * 
	 * @param page
	 *            number of the page.
	 * @return subList of idList.
	 * @throws InvalidParameterException
	 *             if the page attribute is invalid.
	 */
	public List<Long> getIdSubList(int page) throws InvalidParameterException {
		int processedPage = page * numberOfNewsOnPage;
		if (processedPage > idList.size()) {
			throw new InvalidParameterException();
		}
		List<Long> subList = idList.subList(processedPage, (processedPage + numberOfNewsOnPage > idList.size()
				? idList.size() : processedPage + numberOfNewsOnPage));
		return subList;
	}

	/**
	 * This method is used to get subList of the idList. Returns first 5
	 * elements of idList or number of the elements equal to idList size if its
	 * size is less then 5.
	 * 
	 * @return subList of idList.
	 */
	public List<Long> getIdSubList() {
		List<Long> subList = idList.subList(DEFAULT_START_PAGE,
				(numberOfNewsOnPage > idList.size() ? idList.size() : numberOfNewsOnPage));
		return subList;
	}

	/**
	 * This method is used to get id of next news record, if there is no such
	 * record null will be returned.
	 * 
	 * @return ID of news record.
	 */
	public Long getNext() {
		if (curIndex == (idList.size() - 1)) {
			return null;
		} else {
			return idList.get(curIndex + 1);
		}
	}

	/**
	 * This method is used to get id of previous news record, if there is no
	 * such record null will be returned.
	 * 
	 * @return ID of news record.
	 */
	public Long getPrev() {
		if (curIndex == 0) {
			return null;
		} else {
			return idList.get(curIndex - 1);
		}
	}

	/**
	 * This method is used, to set up number of news on page within the JSP
	 * page.
	 * 
	 * @return predefined number of news on page.
	 */
	public int getNumberOfNewsOnPage() {
		return numberOfNewsOnPage;
	}

	public void setList(List<Long> idList) {
		this.idList = idList;
	}

	public List<Long> getIdList() {
		return idList;
	}

}
