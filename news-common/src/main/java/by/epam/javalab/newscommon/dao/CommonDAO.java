package by.epam.javalab.newscommon.dao;

import by.epam.javalab.newscommon.dao.exception.DaoException;

/**
 * Common DAO generic interface. Contains methods for basic CRUD operations.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public interface CommonDAO<T> {

	/**
	 * Inserts a new record, based on passed parameter of type T, to the
	 * specified table.
	 *
	 * @param t
	 *            instance of type T.
	 * @return inserted record Id.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	Long insert(T t) throws DaoException;

	/**
	 * Selects one row from the specified table where id of the record, matches
	 * id method parameter. Builds entity based one this record values.
	 *
	 * @param id
	 *            ID of the record.
	 * @return instance of type T, initialized with values from selected record.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	T select(Long id) throws DaoException;

	/**
	 * Updates one row of the specified table with the values from passed
	 * parameter of type T, which also contains ID of the existing record.
	 *
	 * @param t
	 *            instance of type TS, used to update existing record with its
	 *            values.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	void update(T t) throws DaoException;

	/**
	 * Deletes one row from the specified table with the row ID, matching id
	 * method parameter.
	 *
	 * @param id
	 *            ID of the record to be deleted.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	void delete(Long id) throws DaoException;
}
