package by.epam.javalab.newscommon.dao.impl.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.ConstraintViolationException;

import org.joda.time.DateTime;
import org.springframework.transaction.annotation.Transactional;

import by.epam.javalab.newscommon.dao.AuthorDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Author;
import by.epam.javalab.newscommon.entity.News;

@Transactional(rollbackFor = {Exception.class})
public class AuthorDAOJpaImpl implements AuthorDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long insert(Author t) throws DaoException {
		try {
			entityManager.persist(t);
			return t.getId();
		} catch (PersistenceException | ConstraintViolationException e) {
			throw new DaoException(e);
		} 
	}

	@Override
	public Author select(Long id) throws DaoException {
		try {
			Author author = entityManager.find(Author.class, id);
			if (author == null) {
				throw new NoSuchRecordException();
			}
			return author;
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void update(Author t) throws DaoException {
		try {
			Author author = entityManager.find(Author.class, t.getId());
			if (author == null) {
				throw new NoSuchRecordException();
			}
			entityManager.merge(t);
		} catch (ConstraintViolationException e) {
			throw new NoSuchRecordException(e);
		} catch (PersistenceException e) {
			throw new DaoException(e);
		} 
	}

	@Override
	public void delete(Long id) throws DaoException {
		try {
			Author author = entityManager.find(Author.class, id);
			if (author == null) {
				throw new NoSuchRecordException();
			}
			entityManager.remove(author);
		} catch (PersistenceException | ConstraintViolationException e) {
			throw new DaoException(e);
		} 
	}

	@Override
	public List<Author> selectEntries() throws DaoException {
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Author> criteria = criteriaBuilder.createQuery(Author.class);
			criteria.select(criteria.from(Author.class));
			List<Author> authorList = entityManager.createQuery(criteria).getResultList();
			return authorList;
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public List<Author> selectUnexpiredEntries() throws DaoException {
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Author> criteria = criteriaBuilder.createQuery(Author.class);
			Root<Author> author = criteria.from(Author.class);
			criteria.select(author).where(criteriaBuilder.greaterThan(author.get("expired"), new DateTime()));
			List<Author> authorList = entityManager.createQuery(criteria).getResultList();
			return authorList;
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public Author selectAuthor(Long newsId) throws DaoException {
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<News> criteria = criteriaBuilder.createQuery(News.class);
			Root<News> n = criteria.from(News.class);
			criteria.select(n).where(criteriaBuilder.equal(n.get("id"), newsId));
			News news = entityManager.createQuery(criteria).getSingleResult();
			Author author = news.getAuthor();
			if (author == null) {
				throw new NoSuchRecordException();
			}
			entityManager.refresh(author);
			return author;
		} catch (NoResultException e) {
			throw new NoSuchRecordException(e);
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void bindAuthorToNews(Long newsId, Long authorId) throws DaoException {
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<News> criteriaNews = criteriaBuilder.createQuery(News.class);
			CriteriaQuery<Author> criteriaAuthor = criteriaBuilder.createQuery(Author.class);
			Root<News> n = criteriaNews.from(News.class);
			criteriaNews.select(n).where(criteriaBuilder.equal(n.get("id"), newsId));
			News news = entityManager.createQuery(criteriaNews).getSingleResult();
			Root<Author> a = criteriaAuthor.from(Author.class);
			criteriaAuthor.select(a).where(criteriaBuilder.equal(a.get("id"), authorId));
			Author author = entityManager.createQuery(criteriaAuthor).getSingleResult();
			entityManager.refresh(author);
			news.setAuthor(author);
		} catch (NoResultException e) {
			throw new NoSuchRecordException(e);
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void unbindAuthorFromNews(Long... newsId) throws DaoException {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<News> criteria = criteriaBuilder.createQuery(News.class);
		Root<News> n = criteria.from(News.class);
		criteria.select(n).where(n.get("id").in(criteriaBuilder.parameter(List.class, "newsId")));
		List<News> newsList = entityManager.createQuery(criteria).setParameter("newsId", newsId).getResultList();
		for (News news : newsList) {
			news.setAuthor(null);
		}
	}

}
