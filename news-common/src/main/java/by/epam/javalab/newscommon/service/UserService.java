package by.epam.javalab.newscommon.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import by.epam.javalab.newscommon.entity.User;

/**
 * Service interface for User entity. Extends CommonService interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public interface UserService extends CommonService<User>, UserDetailsService {
}
