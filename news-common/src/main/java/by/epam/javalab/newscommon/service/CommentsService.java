package by.epam.javalab.newscommon.service;

import java.util.List;

import by.epam.javalab.newscommon.entity.Comment;
import by.epam.javalab.newscommon.service.exception.ServiceException;

/**
 * Service interface for Comment entity. Extends CommonService interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public interface CommentsService extends CommonService<Comment> {

	/**
	 * Deletes all records from the COMMENTS table, with the NEWS_ID column
	 * value matching newsId method parameter.
	 *
	 * @param newsId
	 *            ID of record from NEWS table.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	void removeComments(Long... newsId) throws ServiceException;

	/**
	 * Selects all records from the COMMENTS table, with the NEWS_ID column
	 * value matching newsId method parameter.
	 *
	 * @param newsId
	 *            ID of record from NEWS table.
	 * @return List of Comment instances, created from ResultSet.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	List<Comment> selectComments(Long newsId) throws ServiceException;
}
