package by.epam.javalab.newscommon.dao.util;

import java.sql.Connection;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * This class is a utility class. It is used to reduce code duplication.
 * 
 * @author Aliaksandr_Artsemyeu
 * @version 1.0
 */
public class ConnectionManager {

	private static final Logger logger = Logger.getLogger(ConnectionManager.class);

	/**
	 * DataSource instance. Used to obtain Connection.
	 */
	@Inject
	private DataSource dataSource;

	/**
	 * This method is used to obtain Connection. When this method is called, it
	 * delegates Connection obtaining to the Spring DataSourceUtils utility
	 * class. Which is needed to take advantage of Spring transaction abilities.
	 * 
	 * @return Connection instance.
	 */
	public Connection getConnection() {
		return DataSourceUtils.getConnection(dataSource);
	}

	/**
	 * This is a utility method. It is used to reduce code duplication. It takes
	 * as arguments Connection instance and any number of ResultSet and
	 * Statement instances. Connection instance is closed with the use of Spring
	 * DataSourceUtils utility class. Which is needed to take advantage of
	 * Spring transaction abilities. And ResultSet and Statement instances is
	 * closed within the forEach loop. They are checked for null, before being
	 * closed.
	 * 
	 * @param connection
	 *            Connection instance.
	 * @param resources
	 *            varArg argument. Any number of ResultSet and Statement
	 *            instances.
	 */
	public void closeResources(Connection connection, AutoCloseable... resources) {
		try {
			for (AutoCloseable r : resources) {
				if (r != null) {
					r.close();
				}
			}
		} catch (Exception e) {
			logger.error("Error during resource closing.", e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

}
