package by.epam.javalab.newscommon.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import by.epam.javalab.newscommon.dao.AuthorDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.dao.util.ConnectionManager;
import by.epam.javalab.newscommon.entity.Author;

/**
 * Implementation of AuthorDAO interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class AuthorDAOImpl implements AuthorDAO {
	
	private static final String SQL_SELECT_AUTHOR = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";
	private static final String SQL_SELECT_AUTHOR_ENTRIES = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR";
	private static final String SQL_SELECT_UNEXPIRED_AUTHOR_ENTRIES = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE EXPIRED > ?";
	private static final String SQL_INSERT_AUTHOR = "INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME, EXPIRED) values (AUTHOR_ID_SEQ.nextval, ?, ?)";
	private static final String SQL_UPDATE_AUTHOR = "UPDATE AUTHOR SET AUTHOR_NAME = ?, EXPIRED = ? WHERE AUTHOR_ID = ?";
	private static final String SQL_DELETE_AUTHOR = "DELETE FROM AUTHOR WHERE AUTHOR_ID = ?";
	private static final String SQL_SELECT_NEWS_AUTHOR = "SELECT AUTHOR.AUTHOR_ID, AUTHOR.AUTHOR_NAME, AUTHOR.EXPIRED FROM AUTHOR JOIN NEWS_AUTHOR ON NEWS_AUTHOR.AUTHOR_ID = AUTHOR.AUTHOR_ID WHERE NEWS_AUTHOR.NEWS_ID = ?";
	private static final String SQL_BIND_AUTHOR_TO_NEWS = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (?, ?)";
	private static final String SQL_UNBIND_AUTHOR_FROM_NEWS = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID IN ( ${numOfParam} )";

	/**
	 * Instance of ConnectionManager utility class. Used to obtain connections
	 * and to release resources.
	 */
	@Inject
	private ConnectionManager connectionManager;

	@Override
	public Author select(Long authorId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR);
			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return buildAuthor(resultSet);
			} else {
				throw new NoSuchRecordException();
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public Long insert(Author author) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_INSERT_AUTHOR, new String[] { "AUTHOR_ID" });
			preparedStatement.setString(1, author.getName());
			preparedStatement.setTimestamp(2, author.getExpiredTimestamp());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DaoException("Inserting author failed, no ID obtained.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public void update(Author author) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
			preparedStatement.setString(1, author.getName());
			preparedStatement.setTimestamp(2, author.getExpiredTimestamp());
			preparedStatement.setLong(3, author.getId());
			int affectedRows = preparedStatement.executeUpdate();
			if (affectedRows == 0) {
				throw new NoSuchRecordException("Updating author failed, no rows affected.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}
	}

	@Override
	public void delete(Long authorId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR);
			preparedStatement.setLong(1, authorId);
			int affectedRows = preparedStatement.executeUpdate();
			if (affectedRows == 0) {
				throw new NoSuchRecordException("Deleting author failed, no rows affected.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}
	}

	@Override
	public Author selectAuthor(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS_AUTHOR);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return buildAuthor(resultSet);
			} else {
				throw new NoSuchRecordException();
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public List<Author> selectEntries() throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			List<Author> authorList = new ArrayList<>();
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_ENTRIES);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				authorList.add(buildAuthor(resultSet));
			}
			return authorList;
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public List<Author> selectUnexpiredEntries() throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			List<Author> authorList = new ArrayList<>();
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_UNEXPIRED_AUTHOR_ENTRIES);
			preparedStatement.setTimestamp(1, new Timestamp(new DateTime().getMillis()));
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				authorList.add(buildAuthor(resultSet));
			}
			return authorList;
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public void bindAuthorToNews(Long newsId, Long authorId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_BIND_AUTHOR_TO_NEWS);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new NoSuchRecordException("Binding author to news failed.");
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}
	}

	@Override
	public void unbindAuthorFromNews(Long... newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			String paramString = StringUtils.repeat("?", ", ", newsId.length);
			String query = StringUtils.replaceOnce(SQL_UNBIND_AUTHOR_FROM_NEWS, "${numOfParam}", paramString);
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(query);
			int count = 0;
			for (Long id : newsId) {
				preparedStatement.setLong(++count, id);
			}
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}
	}

	/**
	 * Initialize a new Author instance with the values obtained from ResultSet.
	 *
	 * @param resultSet
	 *            returned ResultSet of SQL query.
	 * @return initialized instance of Author.
	 * @throws SQLException
	 *             if the columnLabel is not valid; if a database access error
	 *             occurs or this method is called on a closed result set.
	 */
	private Author buildAuthor(ResultSet resultSet) throws SQLException {
		Long authorId = resultSet.getLong("AUTHOR_ID");
		String authorName = resultSet.getString("AUTHOR_NAME");
		DateTime expired = new DateTime(resultSet.getTimestamp("EXPIRED").getTime());
		return new Author(authorId, authorName, expired);
	}

}
