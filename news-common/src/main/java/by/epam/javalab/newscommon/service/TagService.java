package by.epam.javalab.newscommon.service;

import java.util.List;

import by.epam.javalab.newscommon.entity.Tag;
import by.epam.javalab.newscommon.service.exception.ServiceException;

/**
 * Service interface for Tag entity. Extends CommonService interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public interface TagService extends CommonService<Tag> {

	/**
	 * Selects all rows from TAG table.
	 *
	 * @return List of instances of type Tag.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	List<Tag> selectEntries() throws ServiceException;

	/**
	 * Validates passed parameter and call to the appropriate DAO method.
	 *
	 * @param newsId
	 *            Id of the record in the NEWS table.
	 * @return List of Tag instances.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	List<Tag> selectTags(Long newsId) throws ServiceException;

	/**
	 * Inserts new records to the NEWS_TAG table.
	 *
	 * @param newsId
	 *            Id of the record from the NEWS table.
	 * @param tagIds
	 *            List of Id values of the records from the TAG table.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution, or insert
	 *             operation failed.
	 */
	void bindTagsToNews(Long newsId, List<Long> tagIds) throws ServiceException;

	/**
	 * Deletes records from the NEWS_TAG table, with the NEWS_ID column value
	 * matching newsId method parameter.
	 *
	 * @param newsId
	 *            Id of record from NEWS table.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution, or delete
	 *             operation failed.
	 */
	void unbindTagsFromNews(Long... newsId) throws ServiceException;
}
