package by.epam.javalab.newscommon.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.transaction.annotation.Transactional;

import by.epam.javalab.newscommon.entity.Author;
import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.entity.Tag;
import by.epam.javalab.newscommon.service.AuthorService;
import by.epam.javalab.newscommon.service.CommentsService;
import by.epam.javalab.newscommon.service.NewsManagmentService;
import by.epam.javalab.newscommon.service.NewsService;
import by.epam.javalab.newscommon.service.TagService;
import by.epam.javalab.newscommon.service.exception.ServiceException;

public class NewsManagmentServiceImpl implements NewsManagmentService {

	@Inject
	private NewsService newsService;

	@Inject
	private AuthorService authorService;

	@Inject
	private TagService tagService;

	@Inject
	private CommentsService commentsService;

	@Override
	@Transactional(rollbackFor = { ServiceException.class })
	public void delete(Long... newsId) throws ServiceException {
		if (newsId != null && newsId.length != 0) {
			authorService.unbindAuthorFromNews(newsId);
			tagService.unbindTagsFromNews(newsId);
			commentsService.removeComments(newsId);
			newsService.deleteNews(newsId);
		}
	}

	@Override
	@Transactional(rollbackFor = { ServiceException.class })
	public Long insert(News news, Author author, List<Tag> tags) throws ServiceException {
		if (news == null || author == null) {
			throw new ServiceException("One or few of  passed parameters is null.");
		}
		Long newsId = newsService.insert(news);
		authorService.bindAuthorToNews(newsId, author.getId());
		tagService.bindTagsToNews(newsId, getTagIdList(tags));
		return newsId;
	}

	@Override
	@Transactional(rollbackFor = { ServiceException.class })
	public void update(News news, Author author, List<Tag> tags) throws ServiceException {
		if (news == null || author == null) {
			throw new ServiceException("One or few of  passed parameters is null.");
		}
		Long newsId = news.getId();
		newsService.update(news);
		authorService.unbindAuthorFromNews(newsId);
		authorService.bindAuthorToNews(newsId, author.getId());
		tagService.unbindTagsFromNews(newsId);
		tagService.bindTagsToNews(newsId, getTagIdList(tags));
	}

	@Override
	public News select(Long newsId) throws ServiceException {
		News news = newsService.select(newsId);
		buildNews(news);
		return news;
	}

	@Override
	public List<News> selectList(List<Long> subList) throws ServiceException {
		List<News> newsList = null;
		if (subList != null && subList.size() != 0) {
			newsList = newsService.selectNews(subList);
			for (News news : newsList) {
				buildNews(news);
			}
		}
		return newsList;
	}

	private void buildNews(News news) throws ServiceException {
		Long newsId = news.getId();
		news.setAuthor(authorService.selectAuthor(newsId));
		news.setTags(tagService.selectTags(newsId));
		news.setComments(commentsService.selectComments(newsId));
	}

	/**
	 * This method is used to obtain id field values from Tag instances and to
	 * fill a new List with this values.
	 * 
	 * @param tags
	 *            List of Tag instances.
	 * @return List that contains values, which correspond to Tag id field
	 *         values.
	 */
	private List<Long> getTagIdList(List<Tag> tags) {
		List<Long> tagIdList = new ArrayList<>();
		if (tags != null && tags.size() != 0) {
			for (Tag tag : tags) {
				Long tagId = tag.getId();
				tagIdList.add(tagId);
			}
		}
		return tagIdList;
	}
}
