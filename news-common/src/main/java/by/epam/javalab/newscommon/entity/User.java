package by.epam.javalab.newscommon.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Instances of this class is the DTO objects, they serve to transfer values
 * from database to client and in an opposite direction. This class represents
 * User entity and corresponds to TUSER table records.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
@Entity
@Table(name = "TUSER")
@SecondaryTable(name = "ROLES", pkJoinColumns = @PrimaryKeyJoinColumn(name = "USER_ID"))
public class User implements Serializable, UserDetails {

	private static final long serialVersionUID = 1L;

	/** This fields value corresponds to USER_ID column value */
	@Id
	@Column(name = "USER_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userSeqGen")
	@SequenceGenerator(name = "userSeqGen", sequenceName = "USER_ID_SEQ", allocationSize = 1)
	private Long id;

	/** This fields value corresponds to USER_NAME column value */
	@Column(name = "USER_NAME", columnDefinition = "nvarchar2 (50)")
	private String userName;

	/** This fields value corresponds to LOGIN column value */
	@Column(name = "LOGIN")
	private String login;

	/** This fields value corresponds to PASSWORD column value */
	@Column(name = "PASSWORD")
	private String password;

	/**
	 * This fields value corresponds to ROLE_NAME column value in the ROLES
	 * table
	 */
	@Column(table = "ROLES", name = "ROLE_NAME")
	private String role;

	public User() {
	}

	public User(Long id, String userName, String login, String password) {
		this.id = id;
		this.userName = userName;
		this.login = login;
		this.password = password;
	}

	public User(String userName, String login, String password, String role) {
		this.userName = userName;
		this.login = login;
		this.password = password;
		this.role = role;
	}

	public User(Long id, String userName, String login, String password, String role) {
		this.id = id;
		this.userName = userName;
		this.login = login;
		this.password = password;
		this.role = role;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("ROLE_" + role));
		return authorities;
	}

	@Override
	public String getUsername() {
		return login;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public String toString() {
		return "ID: " + id + ", UserName: " + userName + ", Role: " + role + ".";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}
}
