package by.epam.javalab.newscommon.dao.exception;

/**
 * Custom exception. Thrown when executing of SELECT SQL query do not return any
 * records.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class NoSuchRecordException extends DaoException {

	private static final long serialVersionUID = 1L;

	public NoSuchRecordException() {
	}

	public NoSuchRecordException(String message) {
		super(message);
	}

	public NoSuchRecordException(Throwable cause) {
		super(cause);
	}

	public NoSuchRecordException(String message, Throwable cause) {
		super(message, cause);
	}
}
