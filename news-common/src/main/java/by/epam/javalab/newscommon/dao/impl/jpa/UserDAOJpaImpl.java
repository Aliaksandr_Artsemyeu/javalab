package by.epam.javalab.newscommon.dao.impl.jpa;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;

import org.springframework.transaction.annotation.Transactional;

import by.epam.javalab.newscommon.dao.UserDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.User;

@Transactional(rollbackFor = { Exception.class })
public class UserDAOJpaImpl implements UserDAO {
	
	private static final String JPQL_SELECT_USER_BY_LOGIN = "select u from User u where u.login = :login";

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long insert(User t) throws DaoException {
		try {
			entityManager.persist(t);
			return t.getId();
		} catch (PersistenceException | ConstraintViolationException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public User select(Long id) throws DaoException {
		try {
			User user = entityManager.find(User.class, id);
			if (user == null) {
				throw new NoSuchRecordException();
			}
			return user;
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void update(User t) throws DaoException {
		try {
			User user = entityManager.find(User.class, t.getId());
			if (user == null) {
				throw new NoSuchRecordException();
			}
			entityManager.merge(t);
		} catch (ConstraintViolationException e) {
			throw new NoSuchRecordException(e);
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void delete(Long id) throws DaoException {
		try {
			User user = entityManager.find(User.class, id);
			if (user == null) {
				throw new NoSuchRecordException();
			}
			entityManager.remove(user);
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public User find(String login) throws DaoException {
		try {
			User user = entityManager.createQuery(JPQL_SELECT_USER_BY_LOGIN, User.class)
					.setParameter("login", login).getSingleResult();
			return user;
		} catch (NoResultException e) {
			throw new NoSuchRecordException(e);
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

}
