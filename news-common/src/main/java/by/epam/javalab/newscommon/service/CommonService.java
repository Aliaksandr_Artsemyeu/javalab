package by.epam.javalab.newscommon.service;

import by.epam.javalab.newscommon.service.exception.ServiceException;

/**
 * Common Service generic interface. Contains basic CRUD operations.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public interface CommonService<T> {

	/**
	 * Validates passed parameter and calls appropriate Dao method.
	 *
	 * @param t
	 *            instance of type T.
	 * @return Id value of inserted row.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	Long insert(T t) throws ServiceException;

	/**
	 * Validates passed parameter and calls appropriate Dao method.
	 *
	 * @param id
	 *            ID of the record in a table.
	 * @return instance of type T, initialized with values from selected record.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	T select(Long id) throws ServiceException;

	/**
	 * Updates one row of a table with the values from passed parameter, which
	 * also contains Id of the existing record.
	 *
	 * @param t
	 *            instance of type T, used to update existing record with its
	 *            values.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	void update(T t) throws ServiceException;

	/**
	 * Deletes one row from a table with the Id, that matches id method
	 * parameter.
	 *
	 * @param id
	 *            Id of the record to be deleted.
	 * @throws ServiceException
	 *             if DAOException occurred during method execution.
	 */
	void delete(Long id) throws ServiceException;

}
