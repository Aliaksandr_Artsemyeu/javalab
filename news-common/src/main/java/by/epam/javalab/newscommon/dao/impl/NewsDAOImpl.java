package by.epam.javalab.newscommon.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import by.epam.javalab.newscommon.dao.NewsDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.dao.util.ConnectionManager;
import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.service.util.SearchCriteria;

/**
 * Implementation of NewsDAO interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class NewsDAOImpl implements NewsDAO {

	private static final String SQL_SELECT_NEWS = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";
	private static final String SQL_INSERT_NEWS = "INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (NEWS_ID_SEQ.nextval, ?, ?, ?, ?, ?)";
	private static final String SQL_UPDATE_NEWS = "UPDATE NEWS SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, MODIFICATION_DATE = CURRENT_TIMESTAMP WHERE NEWS_ID = ?";
	private static final String SQL_DELETE_NEWS = "DELETE FROM NEWS	WHERE NEWS_ID = ?";
	private static final String SQL_DELETE_SEVERAL_NEWS = "DELETE FROM NEWS	WHERE NEWS_ID IN ( ${numOfParam} )";
	private static final String SQL_SELECT_NEWS_SEARCH_ID = "SELECT base.NEWS_ID, base.MODIFICATION_DATE, COUNT(COMMENTS.COMMENT_ID) AS COM_NUM FROM ( SELECT DISTINCT NEWS.NEWS_ID, NEWS.MODIFICATION_DATE FROM NEWS LEFT JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID LEFT JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID {builder} ) base LEFT JOIN COMMENTS ON base.NEWS_ID = COMMENTS.NEWS_ID GROUP BY base.NEWS_ID, base.MODIFICATION_DATE ORDER BY COM_NUM DESC, base.MODIFICATION_DATE DESC";

	/**
	 * Instance of ConnectionManager utility class. Used to obtain connections
	 * and to release resources.
	 */
	@Inject
	private ConnectionManager connectionManager;

	@Override
	public News select(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return buildNews(resultSet);
			} else {
				throw new NoSuchRecordException();
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public Long insert(News news) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_INSERT_NEWS, new String[] { "NEWS_ID" });
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			preparedStatement.setTimestamp(4, news.getCreationDateTimestamp());
			preparedStatement.setDate(5, news.getModificationDateSql());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DaoException("Inserting news failed, no ID obtained.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public void update(News news) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS);
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			preparedStatement.setLong(4, news.getId());
			int affectedRows = preparedStatement.executeUpdate();
			if (affectedRows == 0) {
				throw new NoSuchRecordException("Updating news failed, no rows affected.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}
	}

	@Override
	public void delete(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS);
			preparedStatement.setLong(1, newsId);
			int affectedRows = preparedStatement.executeUpdate();
			if (affectedRows == 0) {
				throw new NoSuchRecordException("Deleting news failed, no rows affected.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}
	}

	@Override
	public List<News> selectNews(List<Long> idList) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			List<News> newsList = new ArrayList<>();
			String query = constructQuery(idList);
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				newsList.add(buildNews(resultSet));
			}
			return newsList;
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public List<Long> selectIdList(SearchCriteria searchCriteria) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			List<Long> idList = new ArrayList<>();
			String query = constructQuery(searchCriteria, SQL_SELECT_NEWS_SEARCH_ID);
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				idList.add(resultSet.getLong("NEWS_ID"));
			}
			return idList;
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public void deleteNews(Long... newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			String paramString = StringUtils.repeat("?", ", ", newsId.length);
			String query = StringUtils.replaceOnce(SQL_DELETE_SEVERAL_NEWS, "${numOfParam}", paramString);
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(query);
			int count = 0;
			for (Long id : newsId) {
				preparedStatement.setLong(++count, id);
			}
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}
	}

	/**
	 * Initialize a new News instance with the values obtained from ResultSet.
	 *
	 * @param resultSet
	 *            returned ResultSet of SQL query.
	 * @return initialized News instance.
	 * @throws SQLException
	 *             if the columnLabel is not valid; if a database access error
	 *             occurs or this method is called on a closed result set.
	 */
	private News buildNews(ResultSet resultSet) throws SQLException {
		Long newsID = resultSet.getLong("NEWS_ID");
		String title = resultSet.getString("TITLE");
		String shortText = resultSet.getString("SHORT_TEXT");
		String fullText = resultSet.getString("FULL_TEXT");
		DateTime creationDate = new DateTime(resultSet.getTimestamp("CREATION_DATE").getTime());
		DateTime modificationDate = new DateTime(resultSet.getDate("MODIFICATION_DATE").getTime());
		return new News(newsID, title, shortText, fullText, creationDate, modificationDate);
	}

	/**
	 * Constructs SQL query using values from the SearchCriteria method
	 * parameter.
	 *
	 * @param SearchCriteria
	 *            SearchCriteria instance, contains author ID and List of tag
	 *            IDs.
	 * @return constructed SQL query string.
	 */
	private String constructQuery(SearchCriteria searchCriteria, String sql) {
		StringBuilder builder = new StringBuilder();
		Long authorId = searchCriteria.getAuthorId();
		List<Long> tagList = searchCriteria.getTagIdList();
		boolean validAuthor = (authorId != null);
		boolean validTagList = (tagList != null && tagList.size() != 0);
		if (validAuthor || validTagList) {
			builder.append("WHERE ");
			builder.append(validAuthor ? "NEWS_AUTHOR.AUTHOR_ID = " + authorId + (validTagList ? " OR " : " ") : "");
			if (validTagList) {
				Iterator<Long> i = tagList.iterator();
				while (i.hasNext()) {
					builder.append("TAG_ID = " + i.next() + " ");
					if (i.hasNext()) {
						builder.append("OR ");
					}
				}
			}
		}
		String query = sql.replace("{builder}", builder.toString());
		return query;
	}

	/**
	 * Constructs SQL query, that is used to select specified number of records
	 * from the NEWS table.
	 *
	 * @param idList
	 *            List that contains Id of rows to be selected, in specified
	 *            order.
	 * @return constructed SQL query string.
	 */
	private String constructQuery(List<Long> idList) {
		StringBuilder builder = new StringBuilder();
		Iterator<Long> iterator = idList.iterator();
		int counter = 0;
		while (iterator.hasNext()) {
			counter++;
			builder.append("SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, " + counter + " AS ORD FROM NEWS WHERE NEWS_ID = " + iterator.next());
			if (iterator.hasNext()) {
				builder.append(" UNION ");
			} else {
				builder.append(" ORDER BY ORD");
			}
		}
		return builder.toString();
	}

}
