package by.epam.javalab.newscommon.dao.impl.hibernate;

import java.util.List;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleStateException;
import org.hibernate.criterion.Restrictions;

import by.epam.javalab.newscommon.dao.UserDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.User;

public class UserDAOHibernateImpl implements UserDAO {
	
	@Inject
	private SessionFactory sessionFactory;

	@Override
	public Long insert(User t) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.save(t);
			session.flush();
			return t.getId();
		} catch (HibernateException | ConstraintViolationException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public User select(Long id) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			User user = session.get(User.class, id);
			if (user == null) {
				throw new NoSuchRecordException();
			}
			return user;
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public void update(User t) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.update(t);
			session.flush();
		} catch (StaleStateException | ConstraintViolationException e) {
			throw new NoSuchRecordException(e);
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public void delete(Long id) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			User user = session.get(User.class, id);
			session.delete(user);
			session.flush();
		} catch (IllegalArgumentException e) {
			throw new NoSuchRecordException(e);
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public User find(String login) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			List<User> user = session.createCriteria(User.class).add(Restrictions.eq("login", login)).list();
			if (user == null || user.size() == 0) {
				throw new NoSuchRecordException();
			}
			return user.get(0);
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

}
