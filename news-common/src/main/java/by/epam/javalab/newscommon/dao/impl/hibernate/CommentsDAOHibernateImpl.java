package by.epam.javalab.newscommon.dao.impl.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleStateException;
import org.hibernate.exception.ConstraintViolationException;

import by.epam.javalab.newscommon.dao.CommentsDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Comment;
import by.epam.javalab.newscommon.entity.News;

public class CommentsDAOHibernateImpl implements CommentsDAO {
	
	@Inject
	private SessionFactory sessionFactory;
	
	private final static String HQL_SELECT_NEWS_LIST = "select n from News n where id in (:idList)";

	@Override
	public Long insert(Comment t) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.save(t);
			session.flush();
			return t.getCommentId();
		} catch (ConstraintViolationException e) {
			throw new NoSuchRecordException(e);
		} catch (HibernateException | javax.validation.ConstraintViolationException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public Comment select(Long id) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Comment comment = session.get(Comment.class, id);
			if (comment == null) {
				throw new NoSuchRecordException();
			}
			return comment;
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public void update(Comment t) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.update(t);
			session.flush();
		} catch (StaleStateException | ConstraintViolationException | javax.validation.ConstraintViolationException e) {
			throw new NoSuchRecordException(e);
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public void delete(Long id) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Comment comment = session.get(Comment.class, id);
			session.delete(comment);
			session.flush();
		} catch (IllegalArgumentException e) {
			throw new NoSuchRecordException(e);
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void removeComments(Long... newsId) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery(HQL_SELECT_NEWS_LIST);
			query.setParameterList("idList", newsId);
			List<News> newsList = query.list();
			for (News n : newsList) {
				for (Comment c : n.getComments()) {
					session.delete(c);
				}
			}
			session.flush();
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public List<Comment> selectComments(Long newsId) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			News news = session.get(News.class, newsId);
			if (news == null) {
				return new ArrayList<Comment>();
			}
			return news.getComments();
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

}
