package by.epam.javalab.newscommon.dao;

import java.util.List;

import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.entity.Author;

/**
 * DAO interface for Author entity. Extends CommonDAO interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public interface AuthorDAO extends CommonDAO<Author> {

	/**
	 * Selects all existing records from the table AUTHOR. Creates instance of
	 * Author for each returned row and initialize this instance with the row
	 * values. Adds all created Author instances to the List.
	 *
	 * @return List that contains instances of type Author, represents all
	 *         existing records in the AUTHOR table.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	List<Author> selectEntries() throws DaoException;

	/**
	 * Selects all existing records that is not yet expired from the table
	 * AUTHOR. Creates instance of Author for each returned row and initialize
	 * this instance with the row values. Adds all created Author instances to
	 * the List.
	 *
	 * @return List that contains instances of type Author, represents all
	 *         records in the AUTHOR table that is not expired yet.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	List<Author> selectUnexpiredEntries() throws DaoException;

	/**
	 * Selects one row from the AUTHOR table, that represents author of the
	 * created news, with the news Id that matches newsId method parameter.
	 * Builds Author entity based one this record values.
	 *
	 * @param newsId
	 *            Id of the record in NEWS_AUTHOR table.
	 * @return instance of Author, initialized with values form selected record.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	Author selectAuthor(Long newsId) throws DaoException;

	/**
	 * Inserts a new record to the NEWS_AUTHOR table.
	 *
	 * @param newsId
	 *            ID of record in NEWS table.
	 * @param authorId
	 *            ID of the record in AUTHOR table.
	 * @throws DaoException
	 *             if SQLException occurred during method execution, or insert
	 *             operation failed.
	 */
	void bindAuthorToNews(Long newsId, Long authorId) throws DaoException;

	/**
	 * Deletes records from the NEWS_AUTHOR table, with the NEWS_ID column value
	 * matching to any of passed method parameters.
	 *
	 * @param newsId
	 *            ID of record from NEWS table.
	 * @throws DaoException
	 *             if SQLException occurred during method execution, or delete
	 *             operation failed.
	 */
	void unbindAuthorFromNews(Long... newsId) throws DaoException;
}
