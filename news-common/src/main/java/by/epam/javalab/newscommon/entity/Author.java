package by.epam.javalab.newscommon.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import by.epam.javalab.newscommon.entity.converter.JodaTimeToTimestampConverter;

/**
 * Instances of this class is the DTO objects, they serve to transfer values
 * from database to client and in an opposite direction. This class represents
 * Author entity and corresponds to AUTHOR table records.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
@Entity
@Table(name = "AUTHOR")
public class Author implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Author will be valid for 1 year since it's creation by default */
	private static final int DEFAULT_DURATION = 1;
	
	/** This fields value corresponds to AUTHOR_ID column value */
	@Id
	@Column(name = "AUTHOR_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "authorSeqGen")
	@SequenceGenerator(name = "authorSeqGen", sequenceName = "AUTHOR_ID_SEQ", allocationSize = 1)
	private Long id;

	/** This fields value corresponds to AUTHOR_NAME column value */
	@NotEmpty(message = "{author.name.emtpy}")
	@Size(max = 30, message = "{author.name.size}")
	@Column(name = "AUTHOR_NAME", columnDefinition = "nvarchar2 (30)")
	private String name;

	/**
	 * This fields value corresponds to EXPIRED column value, if current date is
	 * after this one, then author is not valid to create news any more.
	 */
	@DateTimeFormat(pattern = "MM-dd-yyyy")
	@Convert(converter = JodaTimeToTimestampConverter.class, disableConversion = false)
	@Column(name = "EXPIRED")
	private DateTime expired;

	public Author() {
		expired = new DateTime().plusYears(DEFAULT_DURATION);
	}
	
	public Author(Long id) {
		this();
		this.id = id;
	}

	public Author(String name) {
		this();
		this.name = name;
	}

	public Author(Long id, String name) {
		this();
		this.id = id;
		this.name = name;
	}

	public Author(Long id, String name, DateTime expired) {
		this.id = id;
		this.name = name;
		this.expired = expired;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DateTime getExpired() {
		return expired;
	}
	
	public Timestamp getExpiredTimestamp() {
		return new Timestamp(expired.getMillis());
	}

	public void setExpired(DateTime expired) {
		this.expired = expired;
	}

	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", expired=" + expired + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
