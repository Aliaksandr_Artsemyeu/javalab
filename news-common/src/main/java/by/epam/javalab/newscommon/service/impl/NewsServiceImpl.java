package by.epam.javalab.newscommon.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import by.epam.javalab.newscommon.dao.NewsDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.service.NewsService;
import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;
import by.epam.javalab.newscommon.service.util.SearchCriteria;

/**
 * Implementation of NewsService interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class NewsServiceImpl implements NewsService {

	private static final Logger logger = Logger.getLogger(NewsService.class);

	@Inject
	private NewsDAO newsDAO;

	@Override
	public Long insert(News news) throws ServiceException {
		if (news == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			Long newsId = newsDAO.insert(news);
			return newsId;
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public News select(Long newsId) throws ServiceException {
		if (newsId == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			News news = newsDAO.select(newsId);
			return news;
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(News news) throws ServiceException {
		if (news == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			newsDAO.update(news);
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long newsId) throws ServiceException {
		if (newsId == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			newsDAO.delete(newsId);
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public List<News> selectNews(List<Long> idList) throws ServiceException {
		if (idList == null || idList.size() == 0) {
			throw new ServiceException("Passed parameter is null or empty!");
		}
		for (Long id : idList) {
			if (id == null) {
				throw new ServiceException("One of List parameters is null!");
			}
		}
		try {
			List<News> newsList = newsDAO.selectNews(idList);
			return newsList;
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Long> selectIdList(SearchCriteria searchCriteria) throws ServiceException {
		if (searchCriteria == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			List<Long> idList = newsDAO.selectIdList(searchCriteria);
			return idList;
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteNews(Long... newsId) throws ServiceException {
		if (newsId == null) {
			throw new ServiceException("Passed parameter is null!");
		} else if (newsId.length != 0) {
			for (Long id : newsId) {
				if (id == null) {
					throw new ServiceException("Passed parameter is null!");
				}
			}
			try {
				newsDAO.deleteNews(newsId);
			} catch (DaoException e) {
				logger.error(e.getMessage(), e);
				throw new ServiceException(e);
			}
		}
	}

}
