package by.epam.javalab.newscommon.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import by.epam.javalab.newscommon.dao.TagDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.dao.util.ConnectionManager;
import by.epam.javalab.newscommon.entity.Tag;

/**
 * Implementation of TagDAO interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class TagDAOImpl implements TagDAO {

	private static final String SQL_SELECT_TAG = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID = ?";
	private static final String SQL_SELECT_TAG_ENTRIES = "SELECT TAG_ID, TAG_NAME FROM TAG";
	private static final String SQL_INSERT_TAG = "INSERT INTO TAG (TAG_ID, TAG_NAME) values (TAG_ID_SEQ.nextval, ?)";
	private static final String SQL_UPDATE_TAG = "UPDATE TAG SET TAG_NAME = ? WHERE TAG_ID = ?";
	private static final String SQL_DELETE_TAG = "DELETE FROM TAG WHERE TAG_ID = ?";
	private static final String SQL_SELECT_NEWS_TAGS = "SELECT TAG.TAG_ID, TAG.TAG_NAME FROM NEWS_TAG JOIN TAG ON NEWS_TAG.TAG_ID = TAG.TAG_ID	WHERE NEWS_TAG.NEWS_ID = ?";
	private static final String SQL_BIND_TAG_TO_NEWS = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) values (?,  ?)";
	private static final String SQL_UNBIND_TAGS_FROM_NEWS = "DELETE FROM NEWS_TAG WHERE NEWS_ID IN ( ${numOfParam} )";
	private static final String SQL_UNBIND_TAG = "DELETE FROM NEWS_TAG WHERE TAG_ID = ?";

	/**
	 * Instance of ConnectionManager utility class. Used to obtain connections
	 * and to release resources.
	 */
	@Inject
	private ConnectionManager connectionManager;

	@Override
	public Tag select(Long tagId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TAG);
			preparedStatement.setLong(1, tagId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return buildTag(resultSet);
			} else {
				throw new NoSuchRecordException();
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public Long insert(Tag tag) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_INSERT_TAG, new String[] { "TAG_ID" });
			preparedStatement.setString(1, tag.getName());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getLong(1);
			} else {
				throw new DaoException("Inserting tag failed, no ID obtained.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public void update(Tag tag) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG);
			preparedStatement.setString(1, tag.getName());
			preparedStatement.setLong(2, tag.getId());
			int affectedRows = preparedStatement.executeUpdate();
			if (affectedRows == 0) {
				throw new NoSuchRecordException("Updating tag failed, no rows affected.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}

	}

	@Override
	public void delete(Long tagId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		PreparedStatement preparedStatementUnbind = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatementUnbind = connection.prepareStatement(SQL_UNBIND_TAG);
			preparedStatementUnbind.setLong(1, tagId);
			preparedStatementUnbind.executeUpdate();
			preparedStatement = connection.prepareStatement(SQL_DELETE_TAG);
			preparedStatement.setLong(1, tagId);
			int affectedRows = preparedStatement.executeUpdate();
			if (affectedRows == 0) {
				throw new NoSuchRecordException("Deleting tag failed, no rows affected.");
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatementUnbind, preparedStatement);
		}
	}

	@Override
	public List<Tag> selectEntries() throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			List<Tag> tagList = new ArrayList<>();
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_TAG_ENTRIES);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tagList.add(buildTag(resultSet));
			}
			return tagList;
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public List<Tag> selectTags(Long newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			List<Tag> tags = new ArrayList<>();
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS_TAGS);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tags.add(buildTag(resultSet));
			}
			return tags;
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, resultSet, preparedStatement);
		}
	}

	@Override
	public void bindTagsToNews(Long newsId, List<Long> tagIdList) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(SQL_BIND_TAG_TO_NEWS);
			for (Long tagId : tagIdList) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagId);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}
	}

	@Override
	public void unbindTagsFromNews(Long... newsId) throws DaoException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			String paramString = StringUtils.repeat("?", ", ", newsId.length);
			String query = StringUtils.replaceOnce(SQL_UNBIND_TAGS_FROM_NEWS, "${numOfParam}", paramString);
			connection = connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(query);
			int count = 0;
			for (Long id : newsId) {
				preparedStatement.setLong(++count, id);
			}
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			connectionManager.closeResources(connection, preparedStatement);
		}
	}

	/**
	 * Initialize a new Tag instance with the values obtained from ResultSet.
	 *
	 * @param resultSet
	 *            returned ResultSet of SQL query.
	 * @return initialized Tag instance.
	 * @throws SQLException
	 *             if the columnLabel is not valid; if a database access error
	 *             occurs or this method is called on a closed result set.
	 */
	private Tag buildTag(ResultSet resultSet) throws SQLException {
		Long tagID = resultSet.getLong("TAG_ID");
		String tagName = resultSet.getString("TAG_NAME");
		return new Tag(tagID, tagName);
	}

}
