package by.epam.javalab.newscommon.dao.impl.jpa;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;

import org.springframework.transaction.annotation.Transactional;

import by.epam.javalab.newscommon.dao.TagDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.entity.Tag;

@Transactional(rollbackFor = {Exception.class})
public class TagDAOJpaImpl implements TagDAO {
	
	private final static String JPQL_SELECT_TAGS = "select t from Tag t";
	private final static String JPQL_SELECT_TAGS_LIST = "select t from Tag t where t.id in :idList";
	private final static String JPQL_SELECT_NEWS = "select n from News n where n.id in :idList";
	private final static String JPQL_SELECT_TAGS_NEWS = "select n from News n join n.tags nt where nt.id = :tagId";
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long insert(Tag t) throws DaoException {
		try {
			entityManager.persist(t);
			return t.getId();
		} catch (PersistenceException | ConstraintViolationException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public Tag select(Long id) throws DaoException {
		try {
			Tag tag = entityManager.find(Tag.class, id);
			if (tag == null) {
				throw new NoSuchRecordException();
			}
			return tag;
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void update(Tag t) throws DaoException {
		try {
			Tag tag = entityManager.find(Tag.class, t.getId());
			if (tag == null) {
				throw new NoSuchRecordException();
			}
			entityManager.merge(t);
		} catch (ConstraintViolationException e) {
			throw new NoSuchRecordException(e);
		} catch (PersistenceException e) {
			throw new DaoException(e);
		} 
	}

	@Override
	public void delete(Long id) throws DaoException {
		try {
			Tag tag = entityManager.find(Tag.class, id);
			if (tag == null) {
				throw new NoSuchRecordException();
			}
			List<News> newsList = entityManager.createQuery(JPQL_SELECT_TAGS_NEWS, News.class).setParameter("tagId", id).getResultList();
			for (News n : newsList) {
				n.getTags().remove(tag);
			}
			entityManager.remove(tag);
		} catch (PersistenceException | ConstraintViolationException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public List<Tag> selectEntries() throws DaoException {
		try {
			List<Tag> tags = entityManager.createQuery(JPQL_SELECT_TAGS, Tag.class).getResultList();
			return tags;
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public List<Tag> selectTags(Long newsId) throws DaoException {
		try {
			News news = entityManager.find(News.class, newsId);
			if (news == null) {
				throw new NoSuchRecordException();
			}
			entityManager.refresh(news);
			List<Tag> tagList = news.getTags();
			return tagList;
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void bindTagsToNews(Long newsId, List<Long> tagIdList) throws DaoException {
		try {
			News news = entityManager.find(News.class, newsId);
			if (news == null) {
				throw new NoSuchRecordException();
			}
			entityManager.refresh(news);
			List<Tag> tagsList = entityManager.createQuery(JPQL_SELECT_TAGS_LIST, Tag.class).setParameter("idList", tagIdList).getResultList();
			if (tagsList == null || tagsList.size() == 0) {
				throw new DaoException();
			}
			news.getTags().addAll(tagsList);
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

	@Override
	public void unbindTagsFromNews(Long... newsId) throws DaoException {
		try {
			List<News> newsList = entityManager.createQuery(JPQL_SELECT_NEWS, News.class).setParameter("idList", Arrays.asList(newsId)).getResultList();
			for (News n : newsList) {
				entityManager.refresh(n);
				n.setTags(null);
			}
		} catch (PersistenceException e) {
			throw new DaoException(e);
		}
	}

}
