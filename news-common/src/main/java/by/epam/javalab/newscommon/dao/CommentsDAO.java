package by.epam.javalab.newscommon.dao;

import java.util.List;

import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.entity.Comment;

/**
 * DAO interface for Comment entity. Extends CommonDAO interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public interface CommentsDAO extends CommonDAO<Comment> {

	/**
	 * Deletes all records from the COMMENTS table, with the NEWS_ID column
	 * value matching newsID method parameter.
	 *
	 * @param newsId
	 *            ID of record from NEWS table.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	void removeComments(Long... newsId) throws DaoException;

	/**
	 * Selects all records from the COMMENTS table, with the NEWS_ID column
	 * value matching newsID method parameter.
	 *
	 * @param newsId
	 *            ID of record from NEWS table.
	 * @return List of Comment instances, created from ResultSet.
	 * @throws DaoException
	 *             if SQLException occurred during method execution.
	 */
	List<Comment> selectComments(Long newsId) throws DaoException;
}
