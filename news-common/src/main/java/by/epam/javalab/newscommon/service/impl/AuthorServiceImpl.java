package by.epam.javalab.newscommon.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import by.epam.javalab.newscommon.dao.AuthorDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Author;
import by.epam.javalab.newscommon.service.AuthorService;
import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;

/**
 * Implementation of AuthorService interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class AuthorServiceImpl implements AuthorService {

	private static final Logger logger = Logger.getLogger(AuthorService.class);

	@Inject
	private AuthorDAO authorDAO;

	@Override
	public Long insert(Author author) throws ServiceException {
		if (author == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			Long authorId = authorDAO.insert(author);
			return authorId;
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public Author select(Long authorId) throws ServiceException {
		if (authorId == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			Author author = authorDAO.select(authorId);
			return author;
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(Author author) throws ServiceException {
		if (author == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			authorDAO.update(author);
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long authorId) throws ServiceException {
		if (authorId == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			authorDAO.delete(authorId);
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Author> selectEntries() throws ServiceException {
		try {
			List<Author> authorList = authorDAO.selectEntries();
			return authorList;
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Author> selectUnexpiredEntries() throws ServiceException {
		try {
			List<Author> authorList = authorDAO.selectUnexpiredEntries();
			return authorList;
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public Author selectAuthor(Long newsId) throws ServiceException {
		if (newsId == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			Author author = authorDAO.selectAuthor(newsId);
			return author;
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void bindAuthorToNews(Long newsId, Long authorId) throws ServiceException {
		if (newsId == null || authorId == null) {
			throw new ServiceException("Passed parameters is null!");
		}
		try {
			authorDAO.bindAuthorToNews(newsId, authorId);
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void unbindAuthorFromNews(Long... newsId) throws ServiceException {
		if (newsId == null) {
			throw new ServiceException("Passed parameter is null!");
		} else if (newsId.length != 0) {
			for (Long id : newsId) {
				if (id == null) {
					throw new ServiceException("Passed parameter is null!");
				}
			}
			try {
				authorDAO.unbindAuthorFromNews(newsId);
			} catch (DaoException e) {
				logger.error(e.getMessage(), e);
				throw new ServiceException(e);
			}
		}
	}

}
