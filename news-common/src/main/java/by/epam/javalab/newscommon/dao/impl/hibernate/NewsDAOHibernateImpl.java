package by.epam.javalab.newscommon.dao.impl.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleStateException;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.ResultTransformer;

import by.epam.javalab.newscommon.dao.NewsDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.service.util.SearchCriteria;

public class NewsDAOHibernateImpl implements NewsDAO {

	@Inject
	private SessionFactory sessionFactory;

	private static final String HQL_SELECT_NEWS_LIST = "select n from News n where id in (:idList)";
	private static final String HQL_DELETE_COMMENTS = "delete from Comment c where c.newsId = :newsId";

	@Override
	public Long insert(News t) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.save(t);
			session.flush();
			return t.getId();
		} catch (HibernateException | ConstraintViolationException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public News select(Long id) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			News news = session.get(News.class, id);
			if (news == null) {
				throw new NoSuchRecordException();
			}
			return news;
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public void update(News t) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.update(t);
			session.flush();
		} catch (StaleStateException | ConstraintViolationException e) {
			throw new NoSuchRecordException(e);
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public void delete(Long id) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			News news = session.get(News.class, id);
			session.createQuery(HQL_DELETE_COMMENTS).setParameter("newsId", id).executeUpdate();
			session.delete(news);
			session.flush();
		} catch (IllegalArgumentException e) {
			throw new NoSuchRecordException(e);
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@Override
	public List<News> selectNews(List<Long> idList) throws DaoException {
		Session session = null;
		List<News> newsList = new ArrayList<>();
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery(HQL_SELECT_NEWS_LIST);
			query.setParameterList("idList", idList).list();
			for (Long id : idList) {
				News n = session.get(News.class, id);
				if (n != null) {
					newsList.add(n);
				}
			}
			return newsList;
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Long> selectIdList(SearchCriteria searchCriteria) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = constructCriteria(session, searchCriteria);
			criteria.createAlias("newsAlias.comments", "commentsAlias", JoinType.LEFT_OUTER_JOIN);
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("newsAlias.id"))
					.add(Projections.groupProperty("newsAlias.title"))
					.add(Projections.groupProperty("newsAlias.shortText"))
					.add(Projections.groupProperty("newsAlias.fullText"))
					.add(Projections.groupProperty("newsAlias.creationDate"))
					.add(Projections.groupProperty("newsAlias.modificationDate"))
					.add(Projections.count("commentsAlias.commentId").as("numberOfComments")));
			criteria.addOrder(Order.desc("numberOfComments"))
					.addOrder(Order.desc("modificationDate"));
			criteria.setResultTransformer(new LongResultTransformer());
			List<Long> newsIdList = criteria.list();
			return newsIdList;
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteNews(Long... newsId) throws DaoException {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Query query = session.createQuery(HQL_SELECT_NEWS_LIST);
			query.setParameterList("idList", newsId);
			List<News> newsList = query.list();
			for (News n : newsList) {
				session.createQuery(HQL_DELETE_COMMENTS).setParameter("newsId", n.getId()).executeUpdate();
				session.delete(n);
			}
			session.flush();
		} catch (HibernateException e) {
			throw new DaoException(e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}
	
	private Criteria constructCriteria(Session session, SearchCriteria searchCriteria) {
		Criteria criteria = session.createCriteria(News.class, "newsAlias");
		Long authorId = searchCriteria.getAuthorId();
		List<Long> tagIdList = searchCriteria.getTagIdList();
		Disjunction or = Restrictions.disjunction();
		if (authorId != null) {
			or.add(Restrictions.eq("newsAlias.author.id", authorId));
		}
		if (tagIdList != null && tagIdList.size() != 0) {
			criteria.createAlias("newsAlias.tags", "tAlias");
			or.add(Restrictions.in("tAlias.id", tagIdList));
		}
		criteria.add(or);
		return criteria;
	}
	
	@SuppressWarnings("serial")
	private static class LongResultTransformer implements ResultTransformer {
		
		@Override
		public Object transformTuple(Object[] tuple, String[] aliases) {
			Long idValue = Long.parseLong(tuple[0].toString());
			return idValue;
		}

		@SuppressWarnings("rawtypes")
		@Override
		public List transformList(List collection) {
			return collection;
		}
		
	}

}
