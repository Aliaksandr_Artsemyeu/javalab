package by.epam.javalab.newscommon.service.exception;

/**
 * Custom exception. Thrown when error occurs during Service method execution.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class ServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	public ServiceException() {
	}

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

}
