package by.epam.javalab.newscommon.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;


/**
 * Instances of this class is the DTO objects, they serve to transfer values
 * from database to client and in an opposite direction. This class represents
 * Tag entity and corresponds to TAG table records.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
@Entity
@Table(name = "TAG")
public class Tag implements Serializable {

	private static final long serialVersionUID = 1L;

	/** This fields value corresponds to TAG_ID column value */
	@Id
	@Column(name = "TAG_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tagSeqGen")
	@SequenceGenerator(name = "tagSeqGen", sequenceName = "TAG_ID_SEQ", allocationSize = 1)
	private Long id;

	/** This fields value corresponds to TAG_NAME column value */
	@NotEmpty(message = "{tag.name.empty}")
	@Size(max = 30, message = "{tag.name.size}")
	@Column(name = "TAG_NAME", columnDefinition = "nvarchar2 (30)")
	private String name;

	public Tag() {
	}
	
	public Tag(Long id) {
		this.id = id;
	}

	public Tag(String name) {
		this.name = name;
	}

	public Tag(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Tag [id=" + id + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
