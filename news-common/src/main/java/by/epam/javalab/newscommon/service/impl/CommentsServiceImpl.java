package by.epam.javalab.newscommon.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import by.epam.javalab.newscommon.dao.CommentsDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Comment;
import by.epam.javalab.newscommon.service.CommentsService;
import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;

/**
 * Implementation of CommentsService interface.
 * 
 * @author Artsemyeu Aliaksandr
 * @version 1.0
 */
public class CommentsServiceImpl implements CommentsService {

	private static final Logger logger = Logger.getLogger(CommentsService.class);

	@Inject
	private CommentsDAO commentsDAO;

	@Override
	public Long insert(Comment comment) throws ServiceException {
		if (comment == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			Long commentID = commentsDAO.insert(comment);
			return commentID;
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public Comment select(Long commentId) throws ServiceException {
		if (commentId == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			Comment comment = commentsDAO.select(commentId);
			return comment;
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void update(Comment comment) throws ServiceException {
		if (comment == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			commentsDAO.update(comment);
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long commentId) throws ServiceException {
		if (commentId == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			commentsDAO.delete(commentId);
		} catch (NoSuchRecordException e) {
			logger.error(e.getMessage(), e);
			throw new InvalidParameterException(e);
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}

	@Override
	public void removeComments(Long... newsId) throws ServiceException {
		if (newsId == null) {
			throw new ServiceException("Passed parameter is null!");
		} else if (newsId.length != 0) {
			for (Long id : newsId) {
				if (id == null) {
					throw new ServiceException("Passed parameter is null!");
				}
			}
			try {
				commentsDAO.removeComments(newsId);
			} catch (DaoException e) {
				logger.error(e.getMessage(), e);
				throw new ServiceException(e);
			}
		}
	}

	@Override
	public List<Comment> selectComments(Long newsId) throws ServiceException {
		if (newsId == null) {
			throw new ServiceException("Passed parameter is null!");
		}
		try {
			List<Comment> comments = commentsDAO.selectComments(newsId);
			return comments;
		} catch (DaoException e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(e);
		}
	}
}
