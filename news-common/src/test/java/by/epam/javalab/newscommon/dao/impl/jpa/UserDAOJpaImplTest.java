package by.epam.javalab.newscommon.dao.impl.jpa;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.TransactionSystemException;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import by.epam.javalab.newscommon.dao.UserDAO;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/commonApplicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, 
						  DirtiesContextTestExecutionListener.class,
						  TransactionalTestExecutionListener.class, 
						  DbUnitTestExecutionListener.class })
@DatabaseSetup(value = { "classpath:/user_test.xml" })
@ActiveProfiles(profiles = {"test", "jpa"})
public class UserDAOJpaImplTest {

	@Inject
	private UserDAO userDAO;

	@Test
	public void testInsert_valid() throws Exception {
		String validUserName = "TestName";
		String validLogin = "TestLogin";
		String validPassword = "TestPassword";
		String validRole = "TestRole";
		User user = new User(validUserName, validLogin, validPassword, validRole);
		Long returnedId = userDAO.insert(user);
		assertNotNull(returnedId);
		User insertedUser = userDAO.select(returnedId);
		assertNotNull(insertedUser);
		user.setId(returnedId);
		assertEquals(user, insertedUser);
	}

	@Test(expected = TransactionSystemException.class)
	public void testInsert_invalidName() throws Exception {
		String invalidUserName = StringUtils.repeat("INVALID", "_", 10);
		String validLogin = "TestLogin";
		String validPassword = "TestPassword";
		String validRole = "TestRole";
		User user = new User(invalidUserName, validLogin, validPassword, validRole);
		userDAO.insert(user);
	}

	@Test(expected = TransactionSystemException.class)
	public void testInsert_invalidLogin() throws Exception {
		String validUserName = "TestName";
		String invalidLogin = StringUtils.repeat("INVALID", "_", 5);
		String validPassword = "TestPassword";
		String validRole = "TestRole";
		User user = new User(validUserName, invalidLogin, validPassword, validRole);
		userDAO.insert(user);
	}

	@Test(expected = TransactionSystemException.class)
	public void testInsert_invalidPassword() throws Exception {
		String validUserName = "TestName";
		String validLogin = "TestLogin";
		String invalidPassword = StringUtils.repeat("INVALID", "_", 10);
		String validRole = "TestRole";
		User user = new User(validUserName, validLogin, invalidPassword, validRole);
		userDAO.insert(user);
	}

	@Test(expected = TransactionSystemException.class)
	public void testInsert_invalidRole() throws Exception {
		String validUserName = "TestName";
		String validLogin = "TestLogin";
		String validPassword = "TestPassword";
		String invalidRole = StringUtils.repeat("INVALID", "_", 10);
		User user = new User(validUserName, validLogin, validPassword, invalidRole);
		userDAO.insert(user);
	}

	@Test
	public void testSelect_valid() throws Exception {
		Long validUserId = 4L;
		User user = userDAO.select(validUserId);
		assertNotNull(user);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testSelect_invalid() throws Exception {
		Long invalidUserId = 666L;
		userDAO.select(invalidUserId);
	}

	@Test
	public void testUpdate_valid() throws Exception {
		Long validUserId = 4L;
		String validUserName = "TestName";
		String validLogin = "TestLogin";
		String validPassword = "TestPassword";
		String validRole = "TestRole";
		User user = new User(validUserId, validUserName, validLogin, validPassword, validRole);
		userDAO.update(user);
		User updatedUser = userDAO.select(validUserId);
		assertEquals(user, updatedUser);
	}

	@Test(expected = TransactionSystemException.class)
	public void testUpdate_invalidSize() throws Exception {
		Long validUserId = 4L;
		String invalidUserName = StringUtils.repeat("INVALID", "_", 10);
		String validLogin = "TestLogin";
		String validPassword = "TestPassword";
		String validRole = "TestRole";
		User user = new User(validUserId, invalidUserName, validLogin, validPassword, validRole);
		userDAO.update(user);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testUpdate_invalidId() throws Exception {
		Long invalidUserId = 666L;
		String validUserName = "TestName";
		String validLogin = "TestLogin";
		String validPassword = "TestPassword";
		String validRole = "TestRole";
		User user = new User(invalidUserId, validUserName, validLogin, validPassword, validRole);
		userDAO.update(user);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testDelete_valid() throws Exception {
		Long validUserId = 4L;
		userDAO.delete(validUserId);
		userDAO.select(validUserId);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testDelete_invalid() throws Exception {
		Long invalidUserId = 666L;
		userDAO.delete(invalidUserId);
	}

	@Test
	public void testFind_valid() throws Exception {
		User user = userDAO.find("SetUpLogin3");
		assertNotNull(user);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testFind_invalid() throws Exception {
		String invalidLogin = "Invalid_Login";
		userDAO.find(invalidLogin);
	}
}
