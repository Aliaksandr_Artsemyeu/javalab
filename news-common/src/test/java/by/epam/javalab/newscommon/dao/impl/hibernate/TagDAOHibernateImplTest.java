package by.epam.javalab.newscommon.dao.impl.hibernate;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import by.epam.javalab.newscommon.dao.TagDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Tag;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/commonApplicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, 
						  DirtiesContextTestExecutionListener.class,
						  TransactionalTestExecutionListener.class, 
						  DbUnitTestExecutionListener.class })
@DatabaseSetup(value = { "classpath:/tag_test.xml" })
@ActiveProfiles(profiles = {"test", "hibernate"})
public class TagDAOHibernateImplTest {

	@Inject
	private TagDAO tagDAO;

	@Test
	public void testSelect_validId() throws Exception {
		Long validTagId = 5L;
		Tag tag = tagDAO.select(validTagId);
		assertNotNull(tag);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testSelect_invalidId() throws Exception {
		Long invalidTagId = 666L;
		tagDAO.select(invalidTagId);
	}

	@Test
	public void testInsert_valid() throws Exception {
		String validName = "Test Tag";
		Tag tag = new Tag(validName);
		Long returnedID = tagDAO.insert(tag);
		assertNotNull(returnedID);
		Tag insertedTag = tagDAO.select(returnedID);
		assertNotNull(insertedTag);
		tag.setId(returnedID);
		assertEquals(tag, insertedTag);
	}

	@Test(expected = DaoException.class)
	public void testInsert_invalid() throws Exception {
		String invalidName = StringUtils.repeat("INVALID", "_", 5);
		Tag tag = new Tag(invalidName);
		tagDAO.insert(tag);
	}

	@Test
	public void testUpdate_validId() throws Exception {
		Long validTagId = 5L;
		String validName = "Test Tag";
		Tag tag = new Tag(validTagId, validName);
		tagDAO.update(tag);
		Tag updatedTag = tagDAO.select(validTagId);
		assertEquals(tag, updatedTag);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testUpdate_invalidId() throws Exception {
		Long invalidTagId = 666L;
		String validName = "Test Tag";
		Tag tag = new Tag(invalidTagId, validName);
		tagDAO.update(tag);
	}

	@Test(expected = DaoException.class)
	public void testUpdate_invalidName() throws Exception {
		Long validTagId = 5L;
		String invalidName = StringUtils.repeat("INVALID", "_", 5);
		Tag tag = new Tag(validTagId, invalidName);
		tagDAO.update(tag);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testDelete_validId() throws Exception {
		Long validTagId = 5L;
		tagDAO.delete(validTagId);
		tagDAO.select(validTagId);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testDelete_invalidId() throws Exception {
		Long invalidTagId = 666L;
		tagDAO.delete(invalidTagId);
	}

	@Test
	public void testSelectEntries() throws Exception {
		List<Tag> tagList = tagDAO.selectEntries();
		assertEquals(8, tagList.size());
	}

	@Test
	public void testSelectTags() throws Exception {
		Long validNewsId = 5L;
		List<Tag> tagList = tagDAO.selectTags(validNewsId);
		assertEquals(2, tagList.size());
	}

	@Test
	public void testSelectTags_invalid() throws Exception {
		Long invalidNewsId = 666L;
		List<Tag> tagList = tagDAO.selectTags(invalidNewsId);
		assertEquals(0, tagList.size());
	}

	@Test
	public void testBindTagsToNews_valid() throws Exception {
		Long validNewsId = 5L;
		tagDAO.bindTagsToNews(validNewsId, Arrays.asList(5L, 6L));
		List<Tag> tags = tagDAO.selectTags(validNewsId);
		assertEquals(4, tags.size());
	}

	@Test(expected = DaoException.class)
	public void testBindTagsToNews_invalidId() throws Exception {
		Long invalidNewsId = 666L;
		tagDAO.bindTagsToNews(invalidNewsId, Arrays.asList(5L, 6L));
	}

	@Test(expected = DaoException.class)
	public void testBindTagsToNews_invalidTags() throws Exception {
		Long validNewsId = 5L;
		tagDAO.bindTagsToNews(validNewsId, Arrays.asList(666L, 777L));
	}

	@Test
	public void testUnbindTagsFromNews_valid() throws Exception {
		Long validNewsId = 5L;
		tagDAO.unbindTagsFromNews(validNewsId);
		List<Tag> tags = tagDAO.selectTags(validNewsId);
		assertEquals(0, tags.size());
	}

	@Test
	public void testUnbindTagsFromNewsSeveral_valid() throws Exception {
		List<Tag> tags = null;
		tagDAO.unbindTagsFromNews(3L, 4L, 1L);
		tags = tagDAO.selectTags(3L);
		assertEquals(0, tags.size());
		tags = tagDAO.selectTags(4L);
		assertEquals(0, tags.size());
		tags = tagDAO.selectTags(1L);
		assertEquals(0, tags.size());
	}

	@Test
	public void testUnbindTagsFromNewsSeveralInvalid() throws Exception {
		tagDAO.unbindTagsFromNews(2L, 666L);
		List<Tag> tags = tagDAO.selectTags(2L);
		assertEquals(0, tags.size());
	}

}
