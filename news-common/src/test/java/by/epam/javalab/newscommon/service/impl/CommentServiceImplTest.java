package by.epam.javalab.newscommon.service.impl;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import by.epam.javalab.newscommon.dao.CommentsDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Comment;
import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;
import by.epam.javalab.newscommon.service.impl.CommentsServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/commonApplicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
@ActiveProfiles(profiles = {"test", "jdbc"})
public class CommentServiceImplTest {

	@InjectMocks
	private CommentsServiceImpl commentsService;

	@Mock
	private CommentsDAO commentsDAO;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testInsert_valid() throws Exception {
		// given
		Long validCommentId = 6L;
		Comment validComment = new Comment();
		willReturn(validCommentId).given(commentsDAO).insert(validComment);

		// when
		Long returnedId = commentsService.insert(validComment);

		// then
		then(commentsDAO).should(times(1)).insert(validComment);
		assertEquals(validCommentId, returnedId);
	}

	@Test(expected = InvalidParameterException.class)
	public void testInsert_invalid() throws Exception {
		// given
		Comment invalidComment = new Comment();
		willThrow(new NoSuchRecordException()).given(commentsDAO).insert(invalidComment);

		// when
		commentsService.insert(invalidComment);

		// then
		then(commentsDAO).should(times(1)).insert(invalidComment);
	}

	@Test(expected = ServiceException.class)
	public void testInsert_exception() throws Exception {
		// given
		Comment validComment = new Comment();
		willThrow(new DaoException()).given(commentsDAO).insert(validComment);

		// when
		commentsService.insert(validComment);

		// then
		then(commentsDAO).should(times(1)).insert(validComment);
	}

	@Test(expected = ServiceException.class)
	public void testInsert_null() throws Exception {
		commentsService.insert(null);
	}

	@Test
	public void testSelect_valid() throws Exception {
		// given
		Long validCommentId = 6L;
		Comment validComment = new Comment();
		willReturn(validComment).given(commentsDAO).select(validCommentId);

		// when
		Comment returnedComment = commentsService.select(validCommentId);

		// then
		then(commentsDAO).should(times(1)).select(validCommentId);
		assertEquals(validComment, returnedComment);
	}

	@Test(expected = InvalidParameterException.class)
	public void testSelect_invalid() throws Exception {
		// given
		Long invalidCommentId = 666L;
		willThrow(new NoSuchRecordException()).given(commentsDAO).select(invalidCommentId);

		// when
		commentsService.select(invalidCommentId);

		// then
		then(commentsDAO).should(times(1)).select(invalidCommentId);
	}

	@Test(expected = ServiceException.class)
	public void testSelect_exception() throws Exception {
		// given
		Long validCommentId = 6L;
		willThrow(new DaoException()).given(commentsDAO).select(validCommentId);

		// when
		commentsService.select(validCommentId);

		// then
		then(commentsDAO).should(times(1)).select(validCommentId);
	}

	@Test(expected = ServiceException.class)
	public void testSelect_null() throws Exception {
		commentsService.select(null);
	}

	@Test
	public void testUpdate_valid() throws Exception {
		// given
		Comment validComment = new Comment();
		willDoNothing().given(commentsDAO).update(validComment);

		// when
		commentsService.update(validComment);

		// then
		then(commentsDAO).should(times(1)).update(validComment);
	}

	@Test(expected = InvalidParameterException.class)
	public void testUpdate_invalid() throws Exception {
		// given
		Comment invalidComment = new Comment();
		willThrow(new NoSuchRecordException()).given(commentsDAO).update(invalidComment);

		// when
		commentsService.update(invalidComment);

		// then
		then(commentsDAO).should(times(1)).update(invalidComment);
	}

	@Test(expected = ServiceException.class)
	public void testUpdate_exception() throws Exception {
		// given
		Comment validComment = new Comment();
		willThrow(new DaoException()).given(commentsDAO).update(validComment);

		// when
		commentsService.update(validComment);

		// then
		then(commentsDAO).should(times(1)).update(validComment);
	}

	@Test(expected = ServiceException.class)
	public void testUpdate_null() throws Exception {
		commentsService.update(null);
	}

	@Test
	public void testDelete_valid() throws Exception {
		// given
		Long validCommentId = 6L;
		willDoNothing().given(commentsDAO).delete(validCommentId);

		// when
		commentsService.delete(validCommentId);

		// then
		then(commentsDAO).should(times(1)).delete(validCommentId);
	}

	@Test(expected = InvalidParameterException.class)
	public void testDelete_invalid() throws Exception {
		// given
		Long invalidCommentId = 666L;
		willThrow(new NoSuchRecordException()).given(commentsDAO).delete(invalidCommentId);

		// when
		commentsService.delete(invalidCommentId);

		// then
		then(commentsDAO).should(times(1)).delete(invalidCommentId);
	}

	@Test(expected = ServiceException.class)
	public void testDelete_exception() throws Exception {
		// given
		Long validCommentId = 6L;
		willThrow(new DaoException()).given(commentsDAO).delete(validCommentId);

		// when
		commentsService.delete(validCommentId);

		// then
		then(commentsDAO).should(times(1)).delete(validCommentId);
	}

	@Test(expected = ServiceException.class)
	public void testDelete_null() throws Exception {
		commentsService.delete(null);
	}

	@Test
	public void testRemoveComments_valid() throws Exception {
		// given
		Long validNewsId = 6L;
		willDoNothing().given(commentsDAO).removeComments(validNewsId);

		// when
		commentsService.removeComments(validNewsId);

		// then
		then(commentsDAO).should(times(1)).removeComments(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testRemoveComments_exception() throws Exception {
		// given
		Long validNewsId = 6L;
		willThrow(new DaoException()).given(commentsDAO).removeComments(validNewsId);

		// when
		commentsService.removeComments(validNewsId);

		// then
		then(commentsDAO).should(times(1)).removeComments(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testRemoveComments_null() throws Exception {
		commentsService.removeComments(null, null);
	}

	@Test(expected = ServiceException.class)
	public void testRemoveComments_nestedNull() throws Exception {
		Long validNewsId = 6L;
		commentsService.removeComments(validNewsId, null);
	}

	@Test
	public void testSelectComments_valid() throws Exception {
		// given
		Long validNewsId = 6L;
		willReturn(new ArrayList<Comment>()).given(commentsDAO).selectComments(validNewsId);

		// when
		List<Comment> commentList = commentsService.selectComments(validNewsId);

		// then
		then(commentsDAO).should(times(1)).selectComments(validNewsId);
		assertNotNull(commentList);
	}

	@Test(expected = ServiceException.class)
	public void testSelectComments_exception() throws Exception {
		// given
		Long validNewsId = 6L;
		willThrow(new DaoException()).given(commentsDAO).selectComments(validNewsId);

		// when
		commentsService.selectComments(validNewsId);

		// then
		then(commentsDAO).should(times(1)).selectComments(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testSelectComments_null() throws Exception {
		commentsService.selectComments(null);
	}
}
