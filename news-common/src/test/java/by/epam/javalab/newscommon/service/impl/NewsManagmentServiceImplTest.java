package by.epam.javalab.newscommon.service.impl;

import static org.mockito.BDDMockito.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import by.epam.javalab.newscommon.entity.Author;
import by.epam.javalab.newscommon.entity.Comment;
import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.entity.Tag;
import by.epam.javalab.newscommon.service.AuthorService;
import by.epam.javalab.newscommon.service.CommentsService;
import by.epam.javalab.newscommon.service.NewsService;
import by.epam.javalab.newscommon.service.TagService;
import by.epam.javalab.newscommon.service.exception.ServiceException;
import by.epam.javalab.newscommon.service.impl.NewsManagmentServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/commonApplicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
@ActiveProfiles(profiles = {"test", "jdbc"})
public class NewsManagmentServiceImplTest {

	@InjectMocks
	private NewsManagmentServiceImpl newsManagmentServiceImpl;

	@Mock
	private NewsService newsService;
	@Mock
	private AuthorService authorService;
	@Mock
	private TagService tagService;
	@Mock
	private CommentsService commentsService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testInsert() throws Exception {
		Long validNewsId = 5L;
		Long validAuthorId = 3L;
		News validNews = new News(validNewsId, "test", "test", "test");
		Author validAuthor = new Author(validAuthorId, "test");
		List<Tag> tagList = Arrays.asList(new Tag(1L), new Tag(6L), new Tag(3L));
		List<Long> idList = Arrays.asList(1L, 6L, 3L);

		// given
		InOrder inOrder = inOrder(newsService, authorService, tagService);
		willReturn(validNewsId).given(newsService).insert(validNews);

		// when
		Long returnedId = newsManagmentServiceImpl.insert(validNews, validAuthor, tagList);

		// then
		then(newsService).should(inOrder).insert(validNews);
		then(authorService).should(inOrder).bindAuthorToNews(validNewsId, validAuthorId);
		then(tagService).should(inOrder).bindTagsToNews(validNewsId, idList);
		assertEquals(validNewsId, returnedId);
	}

	@Test(expected = ServiceException.class)
	public void testInsert_exception() throws Exception {
		Long validNewsId = 5L;
		Long validAuthorId = 3L;
		News validNews = new News(validNewsId, "test", "test", "test");
		Author validAuthor = new Author(validAuthorId, "test");
		List<Tag> tagList = Arrays.asList(new Tag(1L), new Tag(6L), new Tag(3L));
		List<Long> idList = Arrays.asList(1L, 6L, 3L);

		// given
		InOrder inOrder = inOrder(newsService, authorService, tagService);
		willReturn(validNewsId).given(newsService).insert(validNews);
		willThrow(new ServiceException()).given(authorService).bindAuthorToNews(validNewsId, validAuthorId);

		// when
		Long returnedId = newsManagmentServiceImpl.insert(validNews, validAuthor, tagList);

		// then
		then(newsService).should(inOrder, times(1)).insert(validNews);
		then(authorService).should(inOrder, times(1)).bindAuthorToNews(validNewsId, validAuthorId);
		then(tagService).should(inOrder, times(0)).bindTagsToNews(validNewsId, idList);
		assertEquals(validNewsId, returnedId);
	}

	@Test(expected = ServiceException.class)
	public void testInsert_null() throws Exception {
		Author validAuthor = new Author();
		List<Tag> tagList = Arrays.asList(new Tag(1L), new Tag(6L), new Tag(3L));
		newsManagmentServiceImpl.insert(null, validAuthor, tagList);
	}

	@Test
	public void testUpdate() throws Exception {
		Long validNewsId = 5L;
		Long validAuthorId = 3L;
		News validNews = new News(validNewsId, "test", "test", "test");
		Author validAuthor = new Author(validAuthorId, "test");
		List<Tag> tagList = Arrays.asList(new Tag(1L), new Tag(6L), new Tag(3L));
		List<Long> idList = Arrays.asList(1L, 6L, 3L);

		// given
		InOrder inOrder = inOrder(newsService, authorService, tagService);

		// when
		newsManagmentServiceImpl.update(validNews, validAuthor, tagList);

		// then
		then(newsService).should(inOrder, times(1)).update(validNews);
		then(authorService).should(inOrder, times(1)).unbindAuthorFromNews(validNewsId);
		then(authorService).should(inOrder, times(1)).bindAuthorToNews(validNewsId, validAuthorId);
		then(tagService).should(inOrder, times(1)).unbindTagsFromNews(validNewsId);
		then(tagService).should(inOrder, times(1)).bindTagsToNews(validNewsId, idList);
	}

	@Test(expected = ServiceException.class)
	public void testUpdate_exception() throws Exception {
		Long validNewsId = 5L;
		Long validAuthorId = 3L;
		News validNews = new News();
		Author validAuthor = new Author();
		List<Tag> tagList = Arrays.asList(new Tag(1L), new Tag(6L), new Tag(3L));
		List<Long> idList = Arrays.asList(1L, 6L, 3L);

		// given
		InOrder inOrder = inOrder(newsService, authorService, tagService);
		willThrow(new ServiceException()).given(newsService).update(validNews);

		// when
		newsManagmentServiceImpl.update(validNews, validAuthor, tagList);

		// then
		then(newsService).should(inOrder, times(1)).update(validNews);
		then(authorService).should(inOrder, never()).unbindAuthorFromNews(validNewsId);
		then(authorService).should(inOrder, never()).bindAuthorToNews(validNewsId, validAuthorId);
		then(tagService).should(inOrder, never()).unbindTagsFromNews(validNewsId);
		then(tagService).should(inOrder, never()).bindTagsToNews(validNewsId, idList);
	}

	@Test(expected = ServiceException.class)
	public void testUpdate_null() throws Exception {
		newsManagmentServiceImpl.update(null, null, null);
	}

	@Test
	public void testDelete() throws Exception {
		Long validNewsId = 5L;
		// given
		InOrder inOrder = inOrder(authorService, tagService, commentsService, newsService);

		// when
		newsManagmentServiceImpl.delete(validNewsId);

		// then
		then(authorService).should(inOrder, times(1)).unbindAuthorFromNews(validNewsId);
		then(tagService).should(inOrder, times(1)).unbindTagsFromNews(validNewsId);
		then(commentsService).should(inOrder, times(1)).removeComments(validNewsId);
		then(newsService).should(inOrder, times(1)).deleteNews(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testDelete_exception() throws Exception {
		Long validNewsId = 5L;
		// given
		InOrder inOrder = inOrder(authorService, tagService, commentsService, newsService);
		willThrow(new ServiceException()).given(commentsService).removeComments(validNewsId);

		// when
		newsManagmentServiceImpl.delete(validNewsId);

		// then
		then(authorService).should(inOrder, times(1)).unbindAuthorFromNews(validNewsId);
		then(tagService).should(inOrder, times(1)).unbindTagsFromNews(validNewsId);
		then(commentsService).should(inOrder, times(1)).removeComments(validNewsId);
		then(newsService).should(inOrder, never()).deleteNews(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testDelete_null() throws Exception {
		Long validNewsId = 5L;
		// given
		willThrow(new ServiceException()).given(authorService).unbindAuthorFromNews(validNewsId, null);

		// when
		newsManagmentServiceImpl.delete(validNewsId, null);
	}

	@Test
	public void testSelect() throws Exception {
		Long validNewsId = 5L;
		News validNews = new News(validNewsId, "test", "test", "test");
		Author validAuthor = new Author();
		List<Tag> tagList = Arrays.asList(new Tag(1L), new Tag(6L), new Tag(3L));
		List<Comment> commentList = new ArrayList<>();

		// given
		willReturn(validNews).given(newsService).select(validNewsId);
		willReturn(validAuthor).given(authorService).selectAuthor(validNewsId);
		willReturn(tagList).given(tagService).selectTags(validNewsId);
		willReturn(commentList).given(commentsService).selectComments(validNewsId);

		// when
		News news = newsManagmentServiceImpl.select(validNewsId);

		// then
		then(newsService).should().select(validNewsId);
		assertEquals(news, validNews);
		assertEquals(news.getAuthor(), validAuthor);
		assertEquals(news.getTags(), tagList);
		assertEquals(news.getComments(), commentList);
	}

	@Test(expected = ServiceException.class)
	public void testSelect_exception() throws Exception {
		Long validNewsId = 5L;

		// given
		willThrow(new ServiceException()).given(newsService).select(validNewsId);

		// when
		newsManagmentServiceImpl.select(validNewsId);
	}

	@Test
	public void testSelectList() throws Exception {
		Long validNewsId = 5L;
		Author validAuthor = new Author();
		List<Tag> tagList = Arrays.asList(new Tag(1L), new Tag(6L), new Tag(3L));
		List<Long> idList = Arrays.asList(1L, 6L, 3L);
		List<Comment> commentList = new ArrayList<>();
		List<News> newsList = Arrays.asList(new News(), new News(), new News());

		// given
		willReturn(newsList).given(newsService).selectNews(idList);
		willReturn(validAuthor).given(authorService).selectAuthor(validNewsId);
		willReturn(tagList).given(tagService).selectTags(validNewsId);
		willReturn(commentList).given(commentsService).selectComments(validNewsId);

		// when
		List<News> dtoNewsList = newsManagmentServiceImpl.selectList(idList);

		// then
		then(newsService).should().selectNews(idList);
		assertEquals(idList.size(), dtoNewsList.size());
	}

	@Test(expected = ServiceException.class)
	public void testSelectList_exception() throws Exception {
		List<Long> idList = Arrays.asList(1L, 6L, 3L);

		// given
		willThrow(new ServiceException()).given(newsService).selectNews(idList);

		// when
		newsManagmentServiceImpl.selectList(idList);
	}

}
