package by.epam.javalab.newscommon.dao.impl;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import by.epam.javalab.newscommon.dao.AuthorDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Author;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/commonApplicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, 
						  DirtiesContextTestExecutionListener.class,
						  TransactionalTestExecutionListener.class, 
						  DbUnitTestExecutionListener.class })
@DatabaseSetup(value = { "classpath:/author_test.xml" })
@ActiveProfiles(profiles = {"test", "jdbc"})
public class AuthorDAOImplTest {

	@Inject
	private AuthorDAO authorDAO;

	@Test
	public void testSelect_existingId() throws Exception {
		// given
		Long validId = 6L;

		// when
		Author author = authorDAO.select(validId);

		// then
		assertNotNull(author);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testSelect_nonexistingId() throws Exception {
		Long invalidId = 666L;
		authorDAO.select(invalidId);
	}

	@Test
	public void testInsert_validAuthor() throws Exception {
		Author author = new Author("ValidName");
		Long returnedId = authorDAO.insert(author);
		assertNotNull(returnedId);
		Author authorSelected = authorDAO.select(returnedId);
		assertNotNull(authorSelected);
		author.setId(returnedId);
		assertEquals(author, authorSelected);
	}

	@Test(expected = DaoException.class)
	public void testInsert_invalidAuthor() throws Exception {
		Author author = new Author(StringUtils.repeat("INVALID", "_", 5));
		authorDAO.insert(author);
	}

	@Test(expected = DaoException.class)
	public void testInsert_emptyAuthor() throws Exception {
		Author author = new Author();
		authorDAO.insert(author);
	}

	@Test
	public void testUpdate_validAuthor() throws Exception {
		Long validId = 6L;
		String validName = "ValidName";
		Author author = new Author(validId, validName);
		authorDAO.update(author);
		Author updatedAuthor = authorDAO.select(validId);
		assertEquals(author, updatedAuthor);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testUpdate_invalidAuthorId() throws Exception {
		Long invalidId = 666L;
		String validName = "ValidName";
		Author author = new Author(invalidId, validName);
		authorDAO.update(author);
	}

	@Test(expected = DaoException.class)
	public void testUpdate_invalidAuthorName() throws Exception {
		Long validId = 6L;
		String invalidName = StringUtils.repeat("INVALID", "_", 5);
		Author author = new Author(validId, invalidName);
		authorDAO.update(author);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testUpdate_invalidAuthorIdAndName() throws Exception {
		Long invalidId = 666L;
		String invalidName = StringUtils.repeat("INVALID", "_", 5);
		Author author = new Author(invalidId, invalidName);
		authorDAO.update(author);
	}

	@Test(expected = DaoException.class)
	public void testUpdate_emptyAuthor() throws Exception {
		Long validId = 6L;
		Author author = new Author(validId, null);
		authorDAO.update(author);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testDelete_valid() throws Exception {
		Long validId = 6L;
		authorDAO.delete(validId);
		authorDAO.select(validId);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testDelete_invalid() throws Exception {
		Long invalidId = 666L;
		authorDAO.delete(invalidId);
	}

	@Test
	public void testSelectAuthor_validId() throws Exception {
		Long validNewsId = 5L;
		Author author = authorDAO.selectAuthor(validNewsId);
		assertNotNull(author);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testSelectAuthor_invalidId() throws Exception {
		Long invalidId = 666L;
		authorDAO.selectAuthor(invalidId);
	}

	@Test
	public void testSelectEntries() throws Exception {
		List<Author> authorList = authorDAO.selectEntries();
		assertEquals(6, authorList.size());
	}
	
	@Test
	public void testSelectValidEntries() throws Exception {
		List<Author> authorList = authorDAO.selectUnexpiredEntries();
		assertEquals(3, authorList.size());
		assertTrue(authorList.containsAll(Arrays.asList(new Author(1L, "Jhon Lock"), new Author(4L, "Billy Bones"),
				new Author(5L, "Luke SkyWalker"))));
	}

	@Test
	public void testBindAuthorToNews_valid() throws Exception {
		Long validNewsId = 4L;
		Long validAuthorId = 6L;
		String bindedAuthorName = "Bilbo Baggins";
		authorDAO.bindAuthorToNews(validNewsId, validAuthorId);
		Author author = authorDAO.selectAuthor(validNewsId);
		assertEquals(bindedAuthorName, author.getName());
	}

	@Test(expected = NoSuchRecordException.class)
	public void testBindAuthorToNews_invalidNews() throws Exception {
		Long invalidNewsId = 666L;
		Long validAuthorId = 6L;
		authorDAO.bindAuthorToNews(invalidNewsId, validAuthorId);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testBindAuthorToNews_invalidAuthor() throws Exception {
		Long validNewsId = 3L;
		Long invalidAuthorId = 666L;
		authorDAO.bindAuthorToNews(validNewsId, invalidAuthorId);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testBindAuthorToNews_invalidNewsAndAuthor() throws Exception {
		Long invalidNewsId = 666L;
		Long invalidAuthorId = 666L;
		authorDAO.bindAuthorToNews(invalidNewsId, invalidAuthorId);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testUnbindAuthorFromNews() throws Exception {
		Long validNewsId = 3L;
		authorDAO.unbindAuthorFromNews(validNewsId);
		authorDAO.selectAuthor(validNewsId);
	}

}
