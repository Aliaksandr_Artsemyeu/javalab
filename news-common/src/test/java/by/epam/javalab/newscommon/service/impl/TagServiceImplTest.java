package by.epam.javalab.newscommon.service.impl;

import static org.mockito.Mockito.*;
import static org.mockito.BDDMockito.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import by.epam.javalab.newscommon.dao.TagDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Tag;
import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;
import by.epam.javalab.newscommon.service.impl.TagServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/commonApplicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
@ActiveProfiles(profiles = {"test", "jdbc"})
public class TagServiceImplTest {

	@InjectMocks
	private TagServiceImpl tagService;

	@Mock
	private TagDAO tagDAO;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testInsert_valid() throws Exception {
		// given
		Long validTagId = 6L;
		Tag validTag = new Tag();
		willReturn(validTagId).given(tagDAO).insert(validTag);

		// when
		Long returnedId = tagService.insert(validTag);

		// then
		then(tagDAO).should(times(1)).insert(validTag);
		assertEquals(validTagId, returnedId);
	}

	@Test(expected = ServiceException.class)
	public void testInsert_exception() throws Exception {
		// given
		Tag validTag = new Tag();
		willThrow(new DaoException()).given(tagDAO).insert(validTag);

		// when
		tagService.insert(validTag);

		// then
		then(tagDAO).should(times(1)).insert(validTag);
	}

	@Test(expected = ServiceException.class)
	public void testInsert_null() throws Exception {
		tagService.insert(null);
	}

	@Test
	public void testSelect_valid() throws Exception {
		// given
		Long validTagId = 6L;
		Tag validTag = new Tag();
		willReturn(validTag).given(tagDAO).select(validTagId);

		// when
		Tag returnedTag = tagService.select(validTagId);

		// then
		then(tagDAO).should(times(1)).select(validTagId);
		assertEquals(validTag, returnedTag);
	}

	@Test(expected = InvalidParameterException.class)
	public void testSelect_invalid() throws Exception {
		// given
		Long invalidTagId = 666L;
		willThrow(new NoSuchRecordException()).given(tagDAO).select(invalidTagId);

		// when
		tagService.select(invalidTagId);

		// then
		then(tagDAO).should(times(1)).select(invalidTagId);
	}

	@Test(expected = ServiceException.class)
	public void testSelect_exception() throws Exception {
		// given
		Long validTagId = 6L;
		willThrow(new DaoException()).given(tagDAO).select(validTagId);

		// when
		tagService.select(validTagId);

		// then
		then(tagDAO).should(times(1)).select(validTagId);
	}

	@Test(expected = ServiceException.class)
	public void testSelect_null() throws Exception {
		tagService.select(null);
	}

	@Test
	public void testUpdate_valid() throws Exception {
		// given
		Tag validTag = new Tag();
		willDoNothing().given(tagDAO).update(validTag);

		// when
		tagService.update(validTag);

		// then
		then(tagDAO).should(times(1)).update(validTag);
	}

	@Test(expected = InvalidParameterException.class)
	public void testUpdate_invalid() throws Exception {
		// given
		Tag invalidTag = new Tag();
		willThrow(new NoSuchRecordException()).given(tagDAO).update(invalidTag);

		// when
		tagService.update(invalidTag);

		// then
		then(tagDAO).should(times(1)).update(invalidTag);
	}

	@Test(expected = ServiceException.class)
	public void testUpdate_exception() throws Exception {
		// given
		Tag validTag = new Tag();
		willThrow(new DaoException()).given(tagDAO).update(validTag);

		// when
		tagService.update(validTag);

		// then
		then(tagDAO).should(times(1)).update(validTag);
	}

	@Test(expected = ServiceException.class)
	public void testUpdate_null() throws Exception {
		tagService.update(null);
	}

	@Test
	public void testDelete_valid() throws Exception {
		// given
		Long validTagId = 6L;
		willDoNothing().given(tagDAO).delete(validTagId);

		// when
		tagService.delete(validTagId);

		// then
		then(tagDAO).should(times(1)).delete(validTagId);
	}

	@Test(expected = InvalidParameterException.class)
	public void testDelete_invalid() throws Exception {
		// given
		Long invalidTagId = 666L;
		willThrow(new NoSuchRecordException()).given(tagDAO).delete(invalidTagId);

		// when
		tagService.delete(invalidTagId);

		// then
		then(tagDAO).should(times(1)).delete(invalidTagId);
	}

	@Test(expected = ServiceException.class)
	public void testDelete_exception() throws Exception {
		// given
		Long validTagId = 6L;
		willThrow(new DaoException()).given(tagDAO).delete(validTagId);

		// when
		tagService.delete(validTagId);

		// then
		then(tagDAO).should(times(1)).delete(validTagId);
	}

	@Test(expected = ServiceException.class)
	public void testDelete_null() throws Exception {
		tagService.delete(null);
	}

	@Test
	public void testSelectEntries_valid() throws Exception {
		// given
		willReturn(new ArrayList<Tag>()).given(tagDAO).selectEntries();

		// when
		List<Tag> returnedList = tagService.selectEntries();

		// then
		then(tagDAO).should(times(1)).selectEntries();
		assertNotNull(returnedList);
	}

	@Test(expected = ServiceException.class)
	public void testSelectEntries_exception() throws Exception {
		// given
		willThrow(new DaoException()).given(tagDAO).selectEntries();

		// when
		tagService.selectEntries();

		// then
		then(tagDAO).should(times(1)).selectEntries();
	}

	@Test
	public void testSelectTags_valid() throws Exception {
		// given
		Long validNewsId = 6L;
		willReturn(new ArrayList<Tag>()).given(tagDAO).selectTags(validNewsId);

		// when
		List<Tag> returnedList = tagService.selectTags(validNewsId);

		// then
		then(tagDAO).should(times(1)).selectTags(validNewsId);
		assertNotNull(returnedList);
	}

	@Test(expected = ServiceException.class)
	public void testSelectTags_exception() throws Exception {
		// given
		Long validNewsId = 6L;
		willThrow(new DaoException()).given(tagDAO).selectTags(validNewsId);

		// when
		tagService.selectTags(validNewsId);

		// then
		then(tagDAO).should(times(1)).selectTags(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testSelectTags_null() throws Exception {
		tagService.selectTags(null);
	}

	@Test
	public void testBindTagsToNews_valid() throws Exception {
		// given
		Long validNewsId = 6L;
		List<Long> tagIdList = Arrays.asList(4L, 3L);
		willDoNothing().given(tagDAO).bindTagsToNews(validNewsId, tagIdList);

		// when
		tagService.bindTagsToNews(validNewsId, tagIdList);

		// then
		then(tagDAO).should(times(1)).bindTagsToNews(validNewsId, tagIdList);
	}

	@Test(expected = ServiceException.class)
	public void testBindTagsToNews_exception() throws Exception {
		// given
		Long validNewsId = 6L;
		List<Long> tagIdList = Arrays.asList(4L, 3L);
		willThrow(new DaoException()).given(tagDAO).bindTagsToNews(validNewsId, tagIdList);

		// when
		tagService.bindTagsToNews(validNewsId, tagIdList);

		// then
		then(tagDAO).should(times(1)).bindTagsToNews(validNewsId, tagIdList);
	}

	@Test(expected = ServiceException.class)
	public void testBindTagsToNews_bothNull() throws Exception {
		tagService.bindTagsToNews(null, null);
	}

	@Test(expected = ServiceException.class)
	public void testBindTagsToNews_idNull() throws Exception {
		List<Long> tagIdList = new ArrayList<>();
		tagService.bindTagsToNews(null, tagIdList);
	}

	@Test(expected = ServiceException.class)
	public void testBindTagsToNews_tagNull() throws Exception {
		Long validNewsId = 6L;
		tagService.bindTagsToNews(validNewsId, null);
	}

	@Test(expected = ServiceException.class)
	public void testBindTagsToNews_nestedNull() throws Exception {
		Long validNewsId = 6L;
		tagService.bindTagsToNews(validNewsId, Arrays.asList(4L, 5L, null, 7L));
	}

	@Test
	public void testUnbindTagsFromNews_valid() throws Exception {
		// given
		Long validNewsId = 6L;
		willDoNothing().given(tagDAO).unbindTagsFromNews(validNewsId);

		// when
		tagService.unbindTagsFromNews(validNewsId);

		// then
		then(tagDAO).should(times(1)).unbindTagsFromNews(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testUnbindTagsFromNews_exception() throws Exception {
		// given
		Long validNewsId = 6L;
		willThrow(new DaoException()).given(tagDAO).unbindTagsFromNews(validNewsId);

		// when
		tagService.unbindTagsFromNews(validNewsId);

		// then
		then(tagDAO).should(times(1)).unbindTagsFromNews(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testUnbindTagsFromNews_null() throws Exception {
		tagService.unbindTagsFromNews(null, null);
	}

	@Test(expected = ServiceException.class)
	public void testUnbindTagsFromNews_nestedNull() throws Exception {
		tagService.unbindTagsFromNews(5L, 2L, null, 4L);
	}

}
