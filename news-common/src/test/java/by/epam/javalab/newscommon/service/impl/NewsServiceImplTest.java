package by.epam.javalab.newscommon.service.impl;

import static org.mockito.Mockito.*;
import static org.mockito.BDDMockito.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import by.epam.javalab.newscommon.dao.NewsDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;
import by.epam.javalab.newscommon.service.impl.NewsServiceImpl;
import by.epam.javalab.newscommon.service.util.SearchCriteria;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/commonApplicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
@ActiveProfiles(profiles = {"test", "jdbc"})
public class NewsServiceImplTest {

	@InjectMocks
	private NewsServiceImpl newsService;

	@Mock
	private NewsDAO newsDAO;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testInsert_valid() throws Exception {
		// given
		Long validNewsId = 5L;
		News validNews = new News();
		willReturn(validNewsId).given(newsDAO).insert(validNews);

		// when
		Long returnedId = newsService.insert(validNews);

		// then
		then(newsDAO).should(times(1)).insert(validNews);
		assertEquals(validNewsId, returnedId);
	}

	@Test(expected = ServiceException.class)
	public void testInsert_exception() throws Exception {
		// given
		News validNews = new News();
		willThrow(new DaoException()).given(newsDAO).insert(validNews);

		// when
		newsService.insert(validNews);

		// then
		then(newsDAO).should(times(1)).insert(validNews);
	}

	@Test(expected = ServiceException.class)
	public void testInsert_null() throws Exception {
		newsService.insert(null);
	}

	@Test
	public void testSelect_valid() throws Exception {
		// given
		Long validNewsId = 5L;
		News validNews = new News();
		willReturn(validNews).given(newsDAO).select(validNewsId);

		// when
		News returnedNews = newsService.select(validNewsId);

		// then
		then(newsDAO).should(times(1)).select(validNewsId);
		assertEquals(validNews, returnedNews);
	}

	@Test(expected = InvalidParameterException.class)
	public void testSelect_invalid() throws Exception {
		// given
		Long invalidNewsId = 666L;
		willThrow(new NoSuchRecordException()).given(newsDAO).select(invalidNewsId);

		// when
		newsService.select(invalidNewsId);

		// then
		then(newsDAO).should(times(1)).select(invalidNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testSelect_exception() throws Exception {
		// given
		Long validNewsId = 5L;
		willThrow(new DaoException()).given(newsDAO).select(validNewsId);

		// when
		newsService.select(validNewsId);

		// then
		then(newsDAO).should(times(1)).select(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testSelect_null() throws Exception {
		newsService.select(null);
	}

	@Test
	public void testUpdate_valid() throws Exception {
		// given
		News validNews = new News();
		willDoNothing().given(newsDAO).update(validNews);

		// when
		newsService.update(validNews);

		// then
		then(newsDAO).should(times(1)).update(validNews);
	}

	@Test(expected = InvalidParameterException.class)
	public void testUpdate_invalid() throws Exception {
		// given
		News invalidNews = new News();
		willThrow(new NoSuchRecordException()).given(newsDAO).update(invalidNews);

		// when
		newsService.update(invalidNews);

		// then
		then(newsDAO).should(times(1)).update(invalidNews);
	}

	@Test(expected = ServiceException.class)
	public void testUpdate_exception() throws Exception {
		// given
		News validNews = new News();
		willThrow(new DaoException()).given(newsDAO).update(validNews);

		// when
		newsService.update(validNews);

		// then
		then(newsDAO).should(times(1)).update(validNews);
	}

	@Test(expected = ServiceException.class)
	public void testUpdate_null() throws Exception {
		newsService.update(null);
	}

	@Test
	public void testDelete_valid() throws Exception {
		// given
		Long validNewsId = 5L;
		willDoNothing().given(newsDAO).delete(validNewsId);

		// when
		newsService.delete(validNewsId);

		// then
		then(newsDAO).should(times(1)).delete(validNewsId);
	}

	@Test(expected = InvalidParameterException.class)
	public void testDelete_invalid() throws Exception {
		// given
		Long invalidNewsId = 666L;
		willThrow(new NoSuchRecordException()).given(newsDAO).delete(invalidNewsId);

		// when
		newsService.delete(invalidNewsId);

		// then
		then(newsDAO).should(times(1)).delete(invalidNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testDelete_exception() throws Exception {
		// given
		Long validNewsId = 5L;
		willThrow(new DaoException()).given(newsDAO).delete(validNewsId);

		// when
		newsService.delete(validNewsId);

		// then
		then(newsDAO).should(times(1)).delete(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testDelete_null() throws Exception {
		newsService.delete(null);
	}

	@Test
	public void testSelectNews_valid() throws Exception {
		// given
		List<Long> validIdList = Arrays.asList(4L, 5L);
		willReturn(new ArrayList<News>()).given(newsDAO).selectNews(validIdList);

		// when
		List<News> newsList = newsService.selectNews(validIdList);

		// then
		then(newsDAO).should(times(1)).selectNews(validIdList);
		assertNotNull(newsList);
	}

	@Test(expected = ServiceException.class)
	public void testSelectNews_exception() throws Exception {
		// given
		List<Long> validIdList = Arrays.asList(4L, 5L);
		willThrow(new DaoException()).given(newsDAO).selectNews(validIdList);

		// when
		newsService.selectNews(validIdList);

		// then
		then(newsDAO).should(times(1)).selectNews(validIdList);
	}

	@Test(expected = ServiceException.class)
	public void testSelectNews_null() throws Exception {
		newsService.selectNews(null);
	}

	@Test(expected = ServiceException.class)
	public void testSelectNews_nestedNull() throws Exception {
		newsService.selectNews(Arrays.asList(5L, 4L, 3L, null, 1L, null));
	}
	
	@Test(expected = ServiceException.class)
	public void testSelectNews_empty() throws Exception {
		newsService.selectNews(new ArrayList<Long>());
	}

	@Test
	public void testSelectIdList_valid() throws Exception {
		// given
		SearchCriteria searchCriteria = new SearchCriteria();
		willReturn(new ArrayList<Long>()).given(newsDAO).selectIdList(searchCriteria);

		// when
		List<Long> idList = newsService.selectIdList(searchCriteria);

		// then
		then(newsDAO).should(times(1)).selectIdList(searchCriteria);
		assertNotNull(idList);
	}

	@Test(expected = ServiceException.class)
	public void testSelectIdList_exception() throws Exception {
		// given
		SearchCriteria searchCriteria = new SearchCriteria();
		willThrow(new DaoException()).given(newsDAO).selectIdList(searchCriteria);

		// when
		newsService.selectIdList(searchCriteria);

		// then
		then(newsDAO).should(times(1)).selectIdList(searchCriteria);
	}

	@Test(expected = ServiceException.class)
	public void testSelectIdList_null() throws Exception {
		newsService.selectIdList(null);
	}

	@Test
	public void testDeleteNews_valid() throws Exception {
		// given
		Long validNewsId = 5L;
		willDoNothing().given(newsDAO).deleteNews(validNewsId);

		// when
		newsService.deleteNews(validNewsId);

		// then
		then(newsDAO).should(times(1)).deleteNews(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testDeleteNews_exception() throws Exception {
		// given
		Long validNewsId = 5L;
		willThrow(new DaoException()).given(newsDAO).deleteNews(validNewsId);

		// when
		newsService.deleteNews(validNewsId);

		// then
		then(newsDAO).should(times(1)).deleteNews(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testDeleteNews_null() throws Exception {
		newsService.deleteNews(null, null);
	}

	@Test(expected = ServiceException.class)
	public void testDeleteNews_nestedNull() throws Exception {
		newsService.deleteNews(5L, 4L, null, 2L);
	}

}
