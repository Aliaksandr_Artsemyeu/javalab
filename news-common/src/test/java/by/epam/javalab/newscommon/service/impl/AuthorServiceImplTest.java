package by.epam.javalab.newscommon.service.impl;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import by.epam.javalab.newscommon.dao.AuthorDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Author;
import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;
import by.epam.javalab.newscommon.service.impl.AuthorServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/commonApplicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
@ActiveProfiles(profiles = {"test", "jdbc"})
public class AuthorServiceImplTest {

	@InjectMocks
	private AuthorServiceImpl authorSerivce;

	@Mock
	private AuthorDAO authorDAO;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testInsert_valid() throws Exception {
		// given
		Long validAuthorId = 6L;
		Author author = new Author();
		willReturn(validAuthorId).given(authorDAO).insert(author);

		// when
		Long returnedId = authorSerivce.insert(author);

		// then
		then(authorDAO).should(times(1)).insert(author);
		assertEquals(validAuthorId, returnedId);
	}

	@Test(expected = ServiceException.class)
	public void testInsert_exception() throws Exception {
		// given
		Author author = new Author();
		willThrow(new DaoException()).given(authorDAO).insert(author);

		// when
		authorSerivce.insert(author);

		// then
		then(authorDAO).should(times(1)).insert(author);
	}

	@Test(expected = ServiceException.class)
	public void testInsert_null() throws Exception {
		authorSerivce.insert(null);
	}

	@Test
	public void testSelect_valid() throws Exception {
		// given
		Author author = new Author();
		Long validAuthorId = 6L;
		willReturn(author).given(authorDAO).select(validAuthorId);

		// when
		Author returnedAuthor = authorSerivce.select(validAuthorId);

		// then
		then(authorDAO).should(times(1)).select(validAuthorId);
		assertEquals(author, returnedAuthor);
	}

	@Test(expected = InvalidParameterException.class)
	public void testSelect_invalid() throws Exception {
		// given
		Long invalidAuthorId = 666L;
		willThrow(new NoSuchRecordException()).given(authorDAO).select(invalidAuthorId);

		// when
		authorSerivce.select(invalidAuthorId);

		// then
		then(authorDAO).should(times(1)).select(invalidAuthorId);
	}

	@Test(expected = ServiceException.class)
	public void testSelect_exception() throws Exception {
		// given
		Long validAuthorId = 6L;
		willThrow(new DaoException()).given(authorDAO).select(validAuthorId);

		// when
		authorSerivce.select(validAuthorId);

		// then
		then(authorDAO).should(times(1)).select(validAuthorId);
	}

	@Test(expected = ServiceException.class)
	public void testSelect_null() throws Exception {
		authorSerivce.select(null);
	}

	@Test
	public void testUpdate_valid() throws Exception {
		// given
		Author author = new Author();
		willDoNothing().given(authorDAO).update(author);

		// when
		authorSerivce.update(author);

		// then
		then(authorDAO).should(times(1)).update(author);
	}

	@Test(expected = InvalidParameterException.class)
	public void testUpdate_invalid() throws Exception {
		// given
		Author invalidAuthor = new Author();
		willThrow(new NoSuchRecordException()).given(authorDAO).update(invalidAuthor);

		// when
		authorSerivce.update(invalidAuthor);

		// then
		then(authorDAO).should(times(1)).update(invalidAuthor);
	}

	@Test(expected = ServiceException.class)
	public void testUpdate_exception() throws Exception {
		// given
		Author author = new Author();
		willThrow(new DaoException()).given(authorDAO).update(author);

		// when
		authorSerivce.update(author);

		// then
		then(authorDAO).should(times(1)).update(author);
	}

	@Test(expected = ServiceException.class)
	public void testUpdate_null() throws Exception {
		authorSerivce.update(null);
	}

	@Test
	public void testDelete_valid() throws Exception {
		// given
		Long validAuthorId = 6L;
		willDoNothing().given(authorDAO).delete(validAuthorId);

		// when
		authorSerivce.delete(validAuthorId);

		// then
		then(authorDAO).should(times(1)).delete(validAuthorId);
	}

	@Test(expected = InvalidParameterException.class)
	public void testDelete_invalid() throws Exception {
		// given
		Long invalidAuthorId = 666L;
		willThrow(new NoSuchRecordException()).given(authorDAO).delete(invalidAuthorId);

		// when
		authorSerivce.delete(invalidAuthorId);

		// then
		then(authorDAO).should(times(1)).delete(invalidAuthorId);
	}

	@Test(expected = ServiceException.class)
	public void testDelete_exception() throws Exception {
		// given
		Long validAuthorId = 6L;
		willThrow(new DaoException()).given(authorDAO).delete(validAuthorId);

		// when
		authorSerivce.delete(validAuthorId);

		// then
		then(authorDAO).should(times(1)).delete(validAuthorId);
	}

	@Test(expected = ServiceException.class)
	public void testDelete_null() throws Exception {
		authorSerivce.delete(null);
	}

	@Test(expected = ServiceException.class)
	public void testSelectEntries_exception() throws Exception {
		// given
		willThrow(new DaoException()).given(authorDAO).selectEntries();

		// when
		authorSerivce.selectEntries();

		// then
		then(authorDAO).should(times(1)).selectEntries();
	}

	@Test
	public void testSelectEntries_valid() throws Exception {
		// given
		willReturn(new ArrayList<Author>()).given(authorDAO).selectEntries();

		// when
		List<Author> authorList = authorSerivce.selectEntries();

		// then
		then(authorDAO).should(times(1)).selectEntries();
		assertNotNull(authorList);
	}
	
	@Test(expected = ServiceException.class)
	public void testSelectValidEntries_exception() throws Exception {
		// given
		willThrow(new DaoException()).given(authorDAO).selectUnexpiredEntries();

		// when
		authorSerivce.selectUnexpiredEntries();

		// then
		then(authorDAO).should(times(1)).selectUnexpiredEntries();
	}

	@Test
	public void testSelectValidEntries_valid() throws Exception {
		// given
		willReturn(new ArrayList<Author>()).given(authorDAO).selectUnexpiredEntries();

		// when
		List<Author> authorList = authorSerivce.selectUnexpiredEntries();

		// then
		then(authorDAO).should(times(1)).selectUnexpiredEntries();
		assertNotNull(authorList);
	}

	@Test
	public void testSelectAuthor_valid() throws Exception {
		// given
		Author author = new Author();
		Long validAuthorId = 6L;
		willReturn(author).given(authorDAO).selectAuthor(validAuthorId);

		// when
		Author returnedAuthor = authorSerivce.selectAuthor(validAuthorId);

		// then
		then(authorDAO).should(times(1)).selectAuthor(validAuthorId);
		assertEquals(author, returnedAuthor);
	}

	@Test(expected = InvalidParameterException.class)
	public void testSelectAuthor_invalid() throws Exception {
		// given
		Long invalidAuthorId = 666L;
		willThrow(new NoSuchRecordException()).given(authorDAO).selectAuthor(invalidAuthorId);

		// when
		authorSerivce.selectAuthor(invalidAuthorId);

		// then
		then(authorDAO).should(times(1)).selectAuthor(invalidAuthorId);
	}

	@Test(expected = ServiceException.class)
	public void testSelectAuthor_exception() throws Exception {
		// given
		Long validAuthorId = 6L;
		willThrow(new DaoException()).given(authorDAO).selectAuthor(validAuthorId);

		// when
		authorSerivce.selectAuthor(validAuthorId);

		// then
		then(authorDAO).should(times(1)).selectAuthor(validAuthorId);
	}

	@Test(expected = ServiceException.class)
	public void testSelectAuthor_null() throws Exception {
		authorSerivce.selectAuthor(null);
	}

	@Test
	public void testBindAuthorToNews_valid() throws Exception {
		// given
		Long validAuthorId = 6L;
		Long validNewsId = 4L;
		willDoNothing().given(authorDAO).bindAuthorToNews(validNewsId, validAuthorId);

		// when
		authorSerivce.bindAuthorToNews(validNewsId, validAuthorId);

		// then
		then(authorDAO).should(times(1)).bindAuthorToNews(validNewsId, validAuthorId);
	}

	@Test(expected = InvalidParameterException.class)
	public void testBindAuthorToNews_invalid() throws Exception {
		// given
		Long invalidAuthorId = 666L;
		Long invalidNewsId = 666L;
		willThrow(new NoSuchRecordException()).given(authorDAO).bindAuthorToNews(invalidNewsId, invalidAuthorId);

		// when
		authorSerivce.bindAuthorToNews(invalidNewsId, invalidAuthorId);

		// then
		then(authorDAO).should(times(1)).bindAuthorToNews(invalidNewsId, invalidAuthorId);
	}

	@Test(expected = ServiceException.class)
	public void testBindAuthorToNews_exception() throws Exception {
		// given
		Long validAuthorId = 6L;
		Long validNewsId = 4L;
		willThrow(new DaoException()).given(authorDAO).bindAuthorToNews(validNewsId, validAuthorId);

		// when
		authorSerivce.bindAuthorToNews(validNewsId, validAuthorId);

		// then
		then(authorDAO).should(times(1)).bindAuthorToNews(validNewsId, validAuthorId);
	}

	@Test(expected = ServiceException.class)
	public void testBindAuthorToNews_nullNews() throws Exception {
		Long validAuthorId = 6L;
		authorSerivce.bindAuthorToNews(null, validAuthorId);
	}

	@Test(expected = ServiceException.class)
	public void testBindAuthorToNews_nullAuthor() throws Exception {
		Long validNewsId = 4L;
		authorSerivce.bindAuthorToNews(validNewsId, null);
	}

	@Test
	public void testUnbindAuthorFromNews_valid() throws Exception {
		// given
		Long validNewsId = 4L;
		willDoNothing().given(authorDAO).unbindAuthorFromNews(validNewsId);

		// when
		authorSerivce.unbindAuthorFromNews(validNewsId);

		// then
		then(authorDAO).should(times(1)).unbindAuthorFromNews(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testUnbindAuthorFromNews_exception() throws Exception {
		// given
		Long validNewsId = 4L;
		willThrow(new DaoException()).given(authorDAO).unbindAuthorFromNews(validNewsId);

		// when
		authorSerivce.unbindAuthorFromNews(validNewsId);

		// then
		then(authorDAO).should(times(1)).unbindAuthorFromNews(validNewsId);
	}

	@Test(expected = ServiceException.class)
	public void testUnbindAuthorFromNews_null() throws Exception {
		authorSerivce.unbindAuthorFromNews(null, null);
	}

	@Test(expected = ServiceException.class)
	public void testUnbindAuthorFromNews_nestedNull() throws Exception {
		Long validNewsId = 4L;
		authorSerivce.bindAuthorToNews(validNewsId, null);
	}

}
