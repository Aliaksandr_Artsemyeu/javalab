package by.epam.javalab.newscommon.dao.impl.hibernate;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import by.epam.javalab.newscommon.dao.NewsDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Author;
import by.epam.javalab.newscommon.entity.News;
import by.epam.javalab.newscommon.service.util.SearchCriteria;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/commonApplicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, 
						  DirtiesContextTestExecutionListener.class,
						  TransactionalTestExecutionListener.class, 
						  DbUnitTestExecutionListener.class })
@DatabaseSetup(value = { "classpath:/news_test.xml" })
@ActiveProfiles(profiles = {"test", "hibernate"})
public class NewsDAOHibernateImplTest {

	@Inject
	private NewsDAO newsDAO;

	@Test
	public void testSelect_validId() throws Exception {
		Long validNewsId = 5L;
		News news = newsDAO.select(validNewsId);
		assertNotNull(news);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testSelect_invalidId() throws Exception {
		Long invalidNewsId = 666L;
		newsDAO.select(invalidNewsId);
	}

	@Test
	public void testInsert_validNews() throws Exception {
		String validTitle = "Test";
		String validShortText = "ShortTextTest";
		String validFullText = "FullTextTest";
		Author author = new Author(2L, "Jack The Sparrow");
		News testNews = new News(validTitle, validShortText, validFullText);
		testNews.setAuthor(author);
		Long returnedID = newsDAO.insert(testNews);
		assertNotNull(returnedID);
		News insertedNews = newsDAO.select(returnedID);
		assertNotNull(insertedNews);
		testNews.setId(returnedID);
		testNews.setTags(insertedNews.getTags());
		testNews.setComments(insertedNews.getComments());
		assertEquals(insertedNews, testNews);
	}

	@Test(expected = DaoException.class)
	public void testInsert_invalidNewsTitle() throws Exception {
		String invalidTitle = StringUtils.repeat("INVALID", "_", 5);
		String validShortText = "ShortTextTest";
		String validFullText = "FullTextTest";
		News testNews = new News(invalidTitle, validShortText, validFullText);
		newsDAO.insert(testNews);
	}

	@Test(expected = DaoException.class)
	public void testInsert_invalidNewsShort() throws Exception {
		String validTitle = "Test";
		String invalidShortText = StringUtils.repeat("INVALID", "_", 20);
		String validFullText = "FullTextTest";
		News testNews = new News(validTitle, invalidShortText, validFullText);
		newsDAO.insert(testNews);
	}

	@Test(expected = DaoException.class)
	public void testInsert_invalidNewsFull() throws Exception {
		String validTitle = "Test";
		String validShortText = "ShortTextTest";
		String invalidFullText = StringUtils.repeat("INVALID", "_", 300);
		News testNews = new News(validTitle, validShortText, invalidFullText);
		newsDAO.insert(testNews);
	}

	@Test(expected = DaoException.class)
	public void testInsert_invalidNewsEmpty() throws Exception {
		News testNews = new News();
		newsDAO.insert(testNews);
	}

	@Test
	public void testUpdate_valid() throws Exception {
		Long validNewsId = 5L;
		String validTitle = "Test";
		String validShortText = "ShortTextTest";
		String validFullText = "FullTextTest";
		News testNews = newsDAO.select(validNewsId);
		testNews.setTitle(validTitle);
		testNews.setShortText(validShortText);
		testNews.setFullText(validFullText);
		newsDAO.update(testNews);
		News updatedNews = newsDAO.select(validNewsId);
		testNews.setTags(updatedNews.getTags());
		testNews.setComments(updatedNews.getComments());
		assertEquals(testNews, updatedNews);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testUpdate_invalidId() throws Exception {
		Long invalidNewsId = 666L;
		String validTitle = "Test";
		String validShortText = "ShortTextTest";
		String validFullText = "FullTextTest";
		News testNews = new News(invalidNewsId, validTitle, validShortText, validFullText);
		newsDAO.update(testNews);
	}

	@Test(expected = DaoException.class)
	public void testUpdate_invalidTitle() throws Exception {
		Long validNewsId = 5L;
		String invalidTitle = StringUtils.repeat("INVALID", "_", 5);
		String validShortText = "ShortTextTest";
		String validFullText = "FullTextTest";
		News testNews = new News(validNewsId, invalidTitle, validShortText, validFullText);
		newsDAO.update(testNews);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testDelete_valid() throws Exception {
		Long validNewsId = 5L;
		newsDAO.delete(validNewsId);
		newsDAO.select(validNewsId);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testDelete_invalidId() throws Exception {
		Long invalidNewsId = 666L;
		newsDAO.delete(invalidNewsId);
	}

	@Test
	public void testSelectNews_valid() throws Exception {
		List<Long> idList = Arrays.asList(3L, 2L, 5L);
		List<News> newsList = newsDAO.selectNews(idList);
		assertEquals(newsList.size(), 3);
		assertEquals(3L, newsList.get(0).getId().longValue());
		assertEquals(2L, newsList.get(1).getId().longValue());
		assertEquals(5L, newsList.get(2).getId().longValue());
	}

	@Test
	public void testSelectNews_invalidId() throws Exception {
		List<Long> idList = Arrays.asList(3L, 666L, 5L);
		List<News> newsList = newsDAO.selectNews(idList);
		assertEquals(2, newsList.size());
	}

	@Test
	public void testSelectIdList_valid() throws Exception {
		SearchCriteria searchCriteria = new SearchCriteria(4L, Arrays.asList(6L, 4L));
		List<Long> idList = newsDAO.selectIdList(searchCriteria);
		assertEquals(idList.size(), 3);
		assertTrue(idList.contains(4L));
		assertTrue(idList.contains(2L));
		assertTrue(idList.contains(1L));
	}

	@Test
	public void testSelectIdList_withoutParam() throws Exception {
		SearchCriteria searchCriteria = new SearchCriteria();
		List<Long> idList = newsDAO.selectIdList(searchCriteria);
		assertEquals(7, idList.size());
	}

	@Test
	public void testSelectIdList_invalidParam() throws Exception {
		SearchCriteria searchCriteria = new SearchCriteria(666L, Arrays.asList(666L, 4L));
		List<Long> idList = newsDAO.selectIdList(searchCriteria);
		assertEquals(idList.size(), 1);
		assertTrue(idList.contains(1L));
	}

	@Test
	public void testSelectIdList_invalidAuthorParam() throws Exception {
		SearchCriteria searchCriteria = new SearchCriteria(666L, null);
		List<Long> idList = newsDAO.selectIdList(searchCriteria);
		assertEquals(idList.size(), 0);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testDeleteNews_valid() throws Exception {
		Long validNewsId = 5L;
		newsDAO.deleteNews(validNewsId);
		newsDAO.select(validNewsId);
	}

	@Test
	public void testDeleteSeveralNews_valid() throws Exception {
		newsDAO.deleteNews(7L, 5L, 6L);
		List<Long> idList = newsDAO.selectIdList(new SearchCriteria());
		assertEquals(idList.size(), 4);
		assertTrue(idList.contains(1L));
		assertTrue(idList.contains(2L));
		assertTrue(idList.contains(3L));
		assertTrue(idList.contains(4L));
	}
}
