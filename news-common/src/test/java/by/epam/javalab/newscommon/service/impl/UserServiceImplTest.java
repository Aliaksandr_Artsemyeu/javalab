package by.epam.javalab.newscommon.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.mockito.Mockito.*;
import static org.mockito.BDDMockito.*;
import static org.junit.Assert.*;

import by.epam.javalab.newscommon.dao.UserDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.User;
import by.epam.javalab.newscommon.service.exception.InvalidParameterException;
import by.epam.javalab.newscommon.service.exception.ServiceException;
import by.epam.javalab.newscommon.service.impl.UserServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/commonApplicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
@ActiveProfiles(profiles = {"test", "jdbc"})
public class UserServiceImplTest {

	@InjectMocks
	private UserServiceImpl userService;

	@Mock
	private UserDAO userDAO;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testInsert_valid() throws Exception {
		// given
		Long validUserId = 6L;
		User validUser = new User();
		willReturn(validUserId).given(userDAO).insert(validUser);

		// when
		Long returnedId = userService.insert(validUser);

		// then
		then(userDAO).should(times(1)).insert(validUser);
		assertEquals(validUserId, returnedId);
	}

	@Test(expected = ServiceException.class)
	public void testInsert_exception() throws Exception {
		// given
		User validUser = new User();
		willThrow(new DaoException()).given(userDAO).insert(validUser);

		// when
		userService.insert(validUser);

		// then
		then(userDAO).should(times(1)).insert(validUser);
	}

	@Test(expected = ServiceException.class)
	public void testInsert_null() throws Exception {
		userService.insert(null);
	}

	@Test
	public void testSelect_valid() throws Exception {
		// given
		Long validUserId = 6L;
		User validUser = new User();
		willReturn(validUser).given(userDAO).select(validUserId);

		// when
		User returnedUser = userService.select(validUserId);

		// then
		then(userDAO).should(times(1)).select(validUserId);
		assertEquals(validUser, returnedUser);
	}

	@Test(expected = InvalidParameterException.class)
	public void testSelect_invalid() throws Exception {
		// given
		Long invalidUserId = 666L;
		willThrow(new NoSuchRecordException()).given(userDAO).select(invalidUserId);

		// when
		userService.select(invalidUserId);

		// then
		then(userDAO).should(times(1)).select(invalidUserId);
	}

	@Test(expected = ServiceException.class)
	public void testSelect_exception() throws Exception {
		// given
		Long validUserId = 6L;
		willThrow(new DaoException()).given(userDAO).select(validUserId);

		// when
		userService.select(validUserId);

		// then
		then(userDAO).should(times(1)).select(validUserId);
	}

	@Test(expected = ServiceException.class)
	public void testSelect_null() throws Exception {
		userService.select(null);
	}

	@Test
	public void testUpdate_valid() throws Exception {
		// given
		User validUser = new User();
		willDoNothing().given(userDAO).update(validUser);

		// when
		userService.update(validUser);

		// then
		then(userDAO).should(times(1)).update(validUser);
	}

	@Test(expected = InvalidParameterException.class)
	public void testUpdate_invalid() throws Exception {
		// given
		User invalidUser = new User();
		willThrow(new NoSuchRecordException()).given(userDAO).update(invalidUser);

		// when
		userService.update(invalidUser);

		// then
		then(userDAO).should(times(1)).update(invalidUser);
	}

	@Test(expected = ServiceException.class)
	public void testUpdate_exception() throws Exception {
		// given
		User validUser = new User();
		willThrow(new DaoException()).given(userDAO).update(validUser);

		// when
		userService.update(validUser);

		// then
		then(userDAO).should(times(1)).update(validUser);
	}

	@Test(expected = ServiceException.class)
	public void testUpdate_null() throws Exception {
		userService.update(null);
	}

	@Test
	public void testDelete_valid() throws Exception {
		// given
		Long validUserId = 6L;
		willDoNothing().given(userDAO).delete(validUserId);

		// when
		userService.delete(validUserId);

		// then
		then(userDAO).should(times(1)).delete(validUserId);
	}

	@Test(expected = InvalidParameterException.class)
	public void testDelete_invalid() throws Exception {
		// given
		Long invalidUserId = 666L;
		willThrow(new NoSuchRecordException()).given(userDAO).delete(invalidUserId);

		// when
		userService.delete(invalidUserId);

		// then
		then(userDAO).should(times(1)).delete(invalidUserId);
	}

	@Test(expected = ServiceException.class)
	public void testDelete_exception() throws Exception {
		// given
		Long validUserId = 6L;
		willThrow(new DaoException()).given(userDAO).delete(validUserId);

		// when
		userService.delete(validUserId);

		// then
		then(userDAO).should(times(1)).delete(validUserId);
	}

	@Test(expected = ServiceException.class)
	public void testDelete_null() throws Exception {
		userService.delete(null);
	}

	@Test
	public void testLoadUserByUsername_valid() throws Exception {
		// given
		User validUser = new User();
		String login = "Valid_Login";
		willReturn(validUser).given(userDAO).find(login);

		// when
		UserDetails userDetails = userService.loadUserByUsername(login);

		// then
		then(userDAO).should(times(1)).find(login);
		assertEquals(validUser, userDetails);
	}

	@Test(expected = UsernameNotFoundException.class)
	public void testLoadUserByUsername_invalid() throws Exception {
		// given
		String invalidLogin = "Invalid_Login";
		willThrow(new NoSuchRecordException()).given(userDAO).find(invalidLogin);

		// when
		userService.loadUserByUsername(invalidLogin);

		// then
		then(userDAO).should(times(1)).find(invalidLogin);
	}

	@Test(expected = UsernameNotFoundException.class)
	public void testLoadUserByUsername_exception() throws Exception {
		// given
		String login = "Valid_Login";
		willThrow(new DaoException()).given(userDAO).find(login);

		// when
		userService.loadUserByUsername(login);

		// then
		then(userDAO).should(times(1)).find(login);
	}

	@Test(expected = UsernameNotFoundException.class)
	public void testLoadUserByUsername_null() throws Exception {
		userService.loadUserByUsername(null);
	}

}
