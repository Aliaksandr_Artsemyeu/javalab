package by.epam.javalab.newscommon.dao.impl.jpa;

import static org.junit.Assert.*;

import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.TransactionSystemException;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import by.epam.javalab.newscommon.dao.CommentsDAO;
import by.epam.javalab.newscommon.dao.exception.DaoException;
import by.epam.javalab.newscommon.dao.exception.NoSuchRecordException;
import by.epam.javalab.newscommon.entity.Comment;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/commonApplicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, 
						  DirtiesContextTestExecutionListener.class,
						  TransactionalTestExecutionListener.class, 
						  DbUnitTestExecutionListener.class })
@DatabaseSetup(value = { "classpath:/comments_test.xml" })
@ActiveProfiles(profiles = {"test", "jpa"})
public class CommentDAOJpaImplTest {

	@Inject
	private CommentsDAO commentsDAO;

	@Test
	public void testInsert_valid() throws Exception {
		Long validNewsId = 5L;
		String validText = "TestCommentText";
		Comment comment = new Comment(validNewsId, validText);
		Long returnedId = commentsDAO.insert(comment);
		assertNotNull(returnedId);
		Comment insertedComment = commentsDAO.select(returnedId);
		assertNotNull(insertedComment);
		comment.setCommentId(returnedId);
		assertEquals(comment, insertedComment);
	}

	@Test(expected = TransactionSystemException.class)
	public void testInsert_invalidId() throws Exception {
		Long invalidNewsId = 666L;
		String validText = "TestCommentText";
		Comment comment = new Comment(invalidNewsId, validText);
		commentsDAO.insert(comment);
	}

	@Test(expected = DaoException.class)
	public void testInsert_invalidText() throws Exception {
		Long validNewsId = 5L;
		String invalidText = StringUtils.repeat("ERROR", "_", 20);
		Comment comment = new Comment(validNewsId, invalidText);
		commentsDAO.insert(comment);
	}

	@Test
	public void testSelect_validId() throws Exception {
		Long validCommentId = 2L;
		Comment comment = commentsDAO.select(validCommentId);
		assertNotNull(comment);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testSelect_invalidId() throws DaoException {
		Long invalidCommentId = 666L;
		commentsDAO.select(invalidCommentId);
	}

	@Test
	public void testUpdate_valid() throws Exception {
		Long validCommentId = 2L;
		Long validNewsId = 5L;
		String validText = "TestCommentText";
		Comment comment = new Comment(validCommentId, validNewsId, validText);
		commentsDAO.update(comment);
		Comment updatedComment = commentsDAO.select(validCommentId);
		assertEquals(comment, updatedComment);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testUpdate_invalidId() throws Exception {
		Long invalidCommentId = 666L;
		Long validNewsId = 5L;
		String validText = "TestCommentText";
		Comment comment = new Comment(invalidCommentId, validNewsId, validText);
		commentsDAO.update(comment);
	}

	@Test(expected = TransactionSystemException.class)
	public void testUpdate_invalidNewsId() throws Exception {
		Long validCommentId = 2L;
		Long invalidNewsId = 666L;
		String validText = "TestCommentText";
		Comment comment = new Comment(validCommentId, invalidNewsId, validText);
		commentsDAO.update(comment);
	}

	@Test(expected = TransactionSystemException.class)
	public void testUpdate_invalidText() throws Exception {
		Long validCommentId = 2L;
		Long validNewsId = 5L;
		String invalidText = StringUtils.repeat("ERROR", "_", 20);
		Comment comment = new Comment(validCommentId, validNewsId, invalidText);
		commentsDAO.update(comment);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testDelete_validId() throws Exception {
		Long validCommentId = 2L;
		commentsDAO.delete(validCommentId);
		commentsDAO.select(validCommentId);
	}

	@Test(expected = NoSuchRecordException.class)
	public void testDelete_invalidId() throws Exception {
		Long invalidCommentId = 666L;
		commentsDAO.delete(invalidCommentId);
	}

	@Test
	public void testRemoveComments_valid() throws Exception {
		Long validNewsId = 5L;
		commentsDAO.removeComments(validNewsId);
		List<Comment> commentList = commentsDAO.selectComments(validNewsId);
		assertEquals(0, commentList.size());
	}

	@Test
	public void testSelectComments_valid() throws Exception {
		Long validNewsId = 5L;
		List<Comment> commentsList = commentsDAO.selectComments(validNewsId);
		assertEquals(3, commentsList.size());
	}

	@Test(expected = NoSuchRecordException.class)
	public void testSelectComments_invalid() throws Exception {
		Long invalidNewsId = 666L;
		List<Comment> commentsList = commentsDAO.selectComments(invalidNewsId);
		assertEquals(0, commentsList.size());
	}

}
